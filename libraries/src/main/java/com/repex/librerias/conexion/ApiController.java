package com.repex.librerias.conexion;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class ApiController {
	
	// CONSTANTES
	public static final String DEFAULT_TAG = "DEFAULT";
	
	// SINGLETON
	private static ApiController singleton;
	
	private ApiController() {
		
		apis = new HashMap<String, String>();
	}
	
	public static ApiController getSingleton() {
		
		if(singleton == null) {
		
			singleton = new ApiController();
		}
		
		return singleton;
	}
	
	// APIS, HASHMAP POR TAG
	private Map<String, String> apis;
	private String defecto = "";
	
	// API POR DEFECTO
	public String getDefault() {
		
		return defecto;
	}
	
	public void setDefault(String api) throws MalformedURLException {
		
		new URL(api);
		
		this.defecto = api;		
		this.apis.put(DEFAULT_TAG, defecto);
	}
	
	// APIS
	public String get(String tag) {
		
		return apis.getOrDefault(tag, "");
	}
	
	public void add(String tag, String api) {
		
		this.apis.put(tag, api);
	}
}
package com.repex.librerias.conexion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

import com.repex.librerias.date.Fecha;

public class Conection {
	
	// CONSTANTES	
	// VARIABLES
	private String url;
	private String funcion;

	private Charset codificacion;
	
	private List<Parameter> data;

	public Conection(String url, String funcion, Charset codificacion, List<Parameter> parametros) throws MalformedURLException {

		new URL(url);
		
		this.url = url;
        this.funcion = funcion;

        this.codificacion = codificacion;
        
		this.data = parametros;
	}
	
	public Conection(String url, String funcion, Charset codificacion, Parcelable parcelable) throws MalformedURLException {

		this.url = url;
        this.funcion = funcion;

        this.codificacion = codificacion;

		this.data = parcelable.parcel();
	}
	
	public Conection(String url, String funcion, Charset codificacion) throws MalformedURLException {

		this(url, funcion, codificacion, new LinkedList<Parameter>());
	}

	public Conection(String url, String funcion, Parcelable parcelable) throws MalformedURLException {

		this(url, funcion, StandardCharsets.UTF_8, parcelable);
	}
	
	public Conection(String url, String funcion, List<Parameter> parametros) throws MalformedURLException {

		this(url, funcion, StandardCharsets.UTF_8, parametros);
	}
	
	public Conection(String url, String funcion) throws MalformedURLException {

		this(url, funcion, StandardCharsets.UTF_8);
	}
	
	public Conection(String url) throws MalformedURLException {

		this(url, "");
	}

	// CLASES
	public static class Response {

		private int codigo;
		private String respuesta;

		public Response(int codigo, String respuesta) {

			this.codigo = codigo;
			this.respuesta = respuesta;
		}

		public int getCodigo() {

			return codigo;
		}

		public String getRespuesta() {

			return respuesta;
		}
	}

	public static class Parameter {
	
		private String key;
		private String value;
		
		public Parameter(String key, String value) {

			this.key = key;
			this.value = value;
		}
		
		public Parameter(String key, int value) {

			this.key = key;
			this.value = Integer.toString(value);
		}
		
		public Parameter(String key, double value) {

			this.key = key;
			this.value = Double.toString(value);
		}
		
		public Parameter(String key, boolean value) {

			this.key = key;
			this.value = Boolean.toString(value);
		}
		
		public Parameter(String key, Fecha value, Fecha.Formato formato) {

			this.key = key;
			this.value = value.parse(formato);
		}
		
		public Parameter(String key, Fecha value) {

			this.key = key;
			this.value = value.parse(Fecha.FORMATO_COMPLETO);
		}
		
		private String getKey() {
			
			return key;
		}
		
		private String getValue() {
			
			return value;
		}
	}

	public void addData(List<Parameter> parametros) {

		this.data.addAll(parametros);
	}

	public void addData(Parameter parametro) {

		this.data.add(parametro);
	}

	// COMPROBAR SI EXISTE UNA URL
	public boolean comprobar() {

		try {

			URL url = new URL(this.url);
			url.openStream();
			
			// DEVOLVER RESPUESTA
			return true;

		} catch (Exception e) {}

		return false;
	}

	// GET
	public Response methodGet() {

		BufferedReader rd = null;

		try {

			URL url = new URL(this.url);

			if (!funcion.equals("")) {

				url = new URL(this.url + "?" + createData());
			}

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), codificacion));

			String line = "";
			String response = "";

			while ((line = rd.readLine()) != null)
				response += line;

			// DEVOLVER RESPUESTA
			return new Response(conn.getResponseCode(), response);

		} catch (Exception e) {

			e.printStackTrace();

        } finally {

			if (rd != null) {

				try {

					rd.close();

				} catch (IOException ex) {

					ex.printStackTrace();
				}
			}
		}

		return null;
	}

	// POST
	public Response methodPost() {

		BufferedReader rd = null;

		try {

			URL url = new URL(this.url);

			byte[] data = createData().getBytes("UTF-8");

			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Length", String.valueOf(data.length));
			conn.setDoOutput(true);
			conn.getOutputStream().write(data);

			rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), codificacion));

			String line = "";
			String response = "";

			while ((line = rd.readLine()) != null)
				response += line;

			// DEVOLVER RESPUESTA
			return new Response(conn.getResponseCode(), response);

		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			if (rd != null) {

				try {

					rd.close();

				} catch (IOException ex) {

					ex.printStackTrace();
				}
			}
		}

		return null;
	}

	// FUNCIONES
	private String createData() throws UnsupportedEncodingException {

		StringBuilder data = new StringBuilder();

		if(!this.funcion.equals("")) {

			data.append(URLEncoder.encode("funcion", codificacion.name()));
			data.append("=");
			data.append(URLEncoder.encode(this.funcion, codificacion.name()));
		}

		for(Parameter parametro : this.data) {

			if(data.length() > 0) {

				data.append("&");
			}

			data.append(URLEncoder.encode(parametro.getKey(), codificacion.name()));
			data.append("=");
			data.append(URLEncoder.encode(parametro.getValue(), codificacion.name()));
		}

		return data.toString();
	}
	
	// CLASES AUXILIARES
	public interface Parcelable {
		
		public List<Parameter> parcel();
	}
}
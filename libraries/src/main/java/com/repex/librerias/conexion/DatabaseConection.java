package com.repex.librerias.conexion;

import java.net.MalformedURLException;
import java.util.LinkedList;
import java.util.List;

public class DatabaseConection {
	
	// SINGLETON
	private static DatabaseConection singleton;
	
	private DatabaseConection() {
		
		permanentParameters = new LinkedList<Conection.Parameter>();
	}

	public static DatabaseConection getSingleton() {

		if(singleton == null)
			singleton = new DatabaseConection();

		return singleton;
	}
	
	private List<Conection.Parameter> permanentParameters; // PARAMETROS PERMANENTES EN TODAS LAS CONEXIONES
	
	public void addPermanent(Conection.Parameter parameter) {
	
		this.permanentParameters.add(parameter);
	}
	
	public void clearPermanent() {
		
		this.permanentParameters.clear();
	}
	
	// FUNCIONES
	public boolean comprobar(String url) throws MalformedURLException {
	
		return new Conection(url).comprobar();
	}
	
	public String get(String tag, String funcion, List<Conection.Parameter> parametros) throws NotApiException, UnreachableApiException, MalformedURLException {
		
		String api = ApiController.getSingleton().get(tag);
		if(api.equals("")) {

			throw new NotApiException("The API with TAG "+tag+" is undefined");
		}
		
		if(!comprobar(api)) {

			throw new UnreachableApiException("Api: "+api+" is unreachable");
		}
		
		Conection c = new Conection(api, funcion, combine(parametros, permanentParameters));
		
		try {

			Conection.Response respuesta = c.methodGet();
			if(respuesta != null)
				return respuesta.getRespuesta();
			
		} catch (Exception e) {

            e.printStackTrace();
        }
		
		return "";
	}
	
	public String get(String funcion, List<Conection.Parameter> parametros) throws NotApiException, UnreachableApiException, MalformedURLException {
		
		return get(ApiController.DEFAULT_TAG, funcion, combine(parametros, permanentParameters));
	}
	
	public String get(String tag, String funcion) throws NotApiException, UnreachableApiException, MalformedURLException {

		return get(tag, funcion, new LinkedList<Conection.Parameter>());
	}
	
	public String get(String funcion) throws NotApiException, UnreachableApiException, MalformedURLException {

		return get(ApiController.DEFAULT_TAG, funcion, new LinkedList<Conection.Parameter>());
	}

	public String post(String tag, String funcion, List<Conection.Parameter> parametros) throws NotApiException, UnreachableApiException, MalformedURLException {

		String api = ApiController.getSingleton().get(tag);
		if(api.equals("")) {

			throw new NotApiException("The API with TAG "+tag+" is undefined");
		}
		
		if(!comprobar(api)) {

			throw new UnreachableApiException("Api: "+api+" is unreachable");
		}
		
		Conection c = new Conection(api, funcion, combine(parametros, permanentParameters));

		try {

			Conection.Response respuesta = c.methodPost();
			if(respuesta != null)
				return respuesta.getRespuesta();

		} catch (Exception e) {

			e.printStackTrace();
		}

		return "";
	}

	public String post(String funcion, List<Conection.Parameter> parametros) throws NotApiException, UnreachableApiException, MalformedURLException {
		
		return post(ApiController.DEFAULT_TAG, funcion, combine(parametros, permanentParameters));
	}
	
	public String post(String tag, String funcion) throws NotApiException, UnreachableApiException, MalformedURLException {

		return post(tag, funcion, new LinkedList<Conection.Parameter>());
	}
	
	public String post(String funcion) throws NotApiException, UnreachableApiException, MalformedURLException {

		return post(ApiController.DEFAULT_TAG, funcion, new LinkedList<Conection.Parameter>());
	}
	
	private List<Conection.Parameter> combine(List<Conection.Parameter> first, List<Conection.Parameter> second) {
	
		List<Conection.Parameter> result = new LinkedList<Conection.Parameter>();
		result.addAll(first);
		result.addAll(second);
		
		return result;
	}
	
	public class NotApiException extends Exception {
		
		private static final long serialVersionUID = 1L;

		public NotApiException() {
			
			super();
		}

		public NotApiException(String message, Throwable cause) {
			
			super(message, cause);
		}

		public NotApiException(String message) {
			
			super(message);
		}

		public NotApiException(Throwable cause) {
			
			super(cause);
		}
	}

	public class UnreachableApiException extends Exception {
		
		private static final long serialVersionUID = 1L;

		public UnreachableApiException() {
			
			super();
		}

		public UnreachableApiException(String message, Throwable cause) {
			
			super(message, cause);
		}

		public UnreachableApiException(String message) {
			
			super(message);
		}

		public UnreachableApiException(Throwable cause) {
			
			super(cause);
		}
	}
}
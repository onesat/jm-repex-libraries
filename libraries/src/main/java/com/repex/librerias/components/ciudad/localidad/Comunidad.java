package com.repex.librerias.components.ciudad.localidad;

public enum Comunidad {
	
	ANDALUCIA(1, "Andalucia"),
	ARAGON(2, "Aragon"),
	ASTURIAS(3, "Asturias"),
	CANTABRIA(4, "Cantabria"),
	CASTILLA_MANCHA(5, "Castilla la Mancha"),
	CASTILLA_LEON(6, "Castilla Leon"),
	CATALUNA(7, "Cataluña"),
	CEUTA(8, "Ceuta"),
	EXTREMADURA(9, "Extremadura"),
	GALICIA(10, "Galicia"),
	ISLAS_BALEARES(11, "Islas Baleares"),
	ISLAS_CANARIAS(12, "Islas Canarias"),
	LA_RIOJA(13, "La Rioja"),
	MADRID(14, "Madrid"),
	MELILLA(15, "Melilla"),
	MURCIA(16, "Murcia"),
	NAVARRA(17, "Navarra"),
	PAIS_VASCO(18, "Pais Vasco"),
	VALENCIA(19, "Valencia");
	
	public int id;
	
	public String nombre;
	public String carpeta;
	
	private Comunidad(int id, String nombre) {
		
		this.id = id;
		
		this.nombre = nombre;
		this.carpeta = nombre.toLowerCase().replaceAll(" ", "_").replaceAll("ñ", "n");
	}
}
package com.repex.librerias.components.paneles.carga;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JPanel;

import com.repex.librerias.components.labels.MyJLabel;
import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.utilidades.IconUtils;

public class PanelCargandoCentral extends BorderPanel {
	
	private static final long serialVersionUID = 1L;

	private JPanel cargando;
	
	public PanelCargandoCentral() {

		super(0);
		
		// CONFIGURACION
		setLayout(new BorderLayout(0, 0));
		
		// CREAR
		inicializar();
	}
	
	private void inicializar() {

		// CARGANDO
		if(cargando == null) {
			
			cargando = new BorderPanel(0, Color.WHITE, 5, false, true);
			cargando.setOpaque(false);
			cargando.setLayout(new FlowLayout(FlowLayout.CENTER));

			MyJLabel label = new MyJLabel(IconUtils.escalar(getClass().getClassLoader().getResource("com/repex/librerias/gif/loading.gif")));
			cargando.add(label);
		}
		
		add(cargando, BorderLayout.CENTER);
	}
	
	public void cancelar() {

		// QUITAR ANTERIOR
		removeAll();
		
		revalidate();
		repaint();
	}
	
	public void fin(JPanel contenido) {
		
		// QUITAR ANTERIOR
		removeAll();
		
		revalidate();
		repaint();
		
		// PONER NUEVO
		add(contenido, BorderLayout.CENTER);
		
		revalidate();
		repaint();
	}
	
	public void inicio() {

		// QUITAR ANTERIOR
		removeAll();
		
		revalidate();
		repaint();
		
		inicializar();
		
		revalidate();
		repaint();
	}
}

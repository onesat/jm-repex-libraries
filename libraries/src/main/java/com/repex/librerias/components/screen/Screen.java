package com.repex.librerias.components.screen;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public abstract class Screen extends JFrame {

	private static final long serialVersionUID = 1L;

	private String titulo;
	
	public Screen(String titulo) {

		this.titulo = titulo;		
		setTitle(this.titulo);
		
		// CONFIGURACION
		configuracion();

		// SALIR
		addWindowListener(new WindowAdapter() {
			
			@Override     
			public void windowClosing(WindowEvent we) {

				salir();
			}
		});
	}
	
	protected abstract void configuracion();
	
	// SALIR. IMPLEMENTAR EN LOS HIJOS PARA CAMBIAR CONFIGURACION
	protected void salir() {
		
		System.exit(0);
	}
}
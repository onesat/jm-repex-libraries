package com.repex.librerias.components.utilidades;

import java.io.File;

import java.net.URL;

import javax.swing.Icon;

import org.apache.commons.io.FileUtils;

import com.repex.librerias.utilidades.IconUtils;

public class UrlControler {

	private static UrlControler singleton;
	
	private UrlControler() {}
	
	public static UrlControler getSingleton() {
		
		if(singleton == null) {
			singleton = new UrlControler();
		}
		
		return singleton;
	}
	
	public synchronized Icon cargar(String url, int tamano) throws Exception {

		URL u = new URL(url);

		// NOMBRE
		String nombre = url.substring(url.indexOf(".")+1, url.lastIndexOf("."));
		nombre = nombre.replaceAll("/", "-");
		nombre = nombre.replaceAll("\\.", "-");

		String tempDir = System.getProperty("java.io.tmpdir");
		String path = tempDir + nombre + ".png";

		File file = new File(path);
		file.deleteOnExit();

		if(!file.exists()) {
			System.out.println("Retrieving file " + u.toString());
			FileUtils.copyURLToFile(u, file);
		}

		return IconUtils.escalar(file.toURI().toURL(), tamano);
	}
	
	private String defecto;
	
	public String getDefecto() {
		return defecto;
	}
	
	public void setDefecto(String defecto) {
		this.defecto = defecto;
	}
}
package com.repex.librerias.components.animacion.component;

import java.awt.Color;
import java.awt.Component;

public class ColorTransition extends ComponentAnimation {
	
	private Color origen;
	private Color destino;
	
	public Color getOrigen() {
		
		return origen;
	}
	
	public Color getDestino() {
		
		return destino;
	}
	
	public ColorTransition(Component component, Color origen, Color destino, double second) {
		
		super(component, second);
		
		this.origen = origen;
		this.destino = destino;
		
		this.addOnAnimationListener(new OnAnimationListener() {

			public void onStart() {

				getComponent().setBackground(getOrigen());
			}

			public void onEnd() {}
		});
	}
	
	public ColorTransition(Component component, Color origen, Color destino) {
		
		this(component, origen, destino, 1f);
	}
	
	protected void animacion(float factor) {

		getComponent().setBackground(interpolateColor(factor));
	}
	
	private Color interpolateColor(float factor) {
		
		factor = Math.min(1, factor);
		float inverse_factor = 1 - factor;
		
		float red = (origen.getRed() * inverse_factor) + (destino.getRed() * factor);
		float green = (origen.getGreen() * inverse_factor) + (destino.getGreen() * factor);
		float blue = (origen.getBlue() * inverse_factor) + (destino.getBlue() * factor);
		
		return new Color(red/255, green/255, blue/255);
	}
}
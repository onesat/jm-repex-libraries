package com.repex.librerias.components.calendario;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.repex.librerias.components.over.OverPanel;
import com.repex.librerias.components.over.OverPanelDia;
import com.repex.librerias.components.paneles.BorderPanelOverSelected;
import com.repex.librerias.date.Fecha;

public class PanelDia extends BorderPanelOverSelected {

	private static final long serialVersionUID = 1L;
	
	// VARIABLES
	private int size;
	
	// VARIABLES INTERNAS
	private Fecha fecha;
	
	private float overTime = 1.5f;
	
	private JPanel panelContenido;
	
	private List<OverPanel.Informacion> contenidoOver;	
	private List<OnDaySelected> acciones;

	private OverPanel over;
	
	public PanelDia(Fecha fecha, int size, int borderSize, Color borderColor) {
		
		super(borderSize, borderColor);

		this.fecha = fecha;

		this.size = size;
	
		this.contenidoOver = new LinkedList<OverPanel.Informacion>();		
		this.acciones = new LinkedList<OnDaySelected>();
		
		inicializar();
	}
	
	public PanelDia(Fecha fecha, int size, int borderSize) {
		
		this(fecha, size, borderSize, BORDER_COLOR_DEFAULT);
	}

	public PanelDia(Fecha fecha, int size) {
		
		this(fecha, size, BORDER_SIZE_DEFAULT, BORDER_COLOR_DEFAULT);
	}
	
	public Fecha getFecha() {
		
		return fecha;
	}
	
	public void setOverTime(float overTime) {
		
		this.overTime = overTime;
	}
	
	private void inicializar() {

		// TAMANO
		Dimension d = new Dimension(size+getBorderSize(), size+getBorderSize());
		setPreferredSize(d);
		setMaximumSize(d);
		setMinimumSize(d);
		
		// CONFIGURACION
		setLayout(new FlowLayout(FlowLayout.CENTER));
		
		// DETECTOR
		addMouseListener(new MouseAdapter() {

			@Override
			public void mouseEntered(MouseEvent e) {

				if(!contenidoOver.isEmpty()) {

					if(over == null) {
											
						over = new OverPanelDia(fecha, contenidoOver);
					}

					over.lanzar(overTime);
				}
			}
			
			@Override
			public void mouseExited(MouseEvent e) {

				if(over != null) {
					
					over.detener();
				}
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				for(OnDaySelected accion : acciones) {
					
					accion.selected(PanelDia.this, fecha);
				}
			}
		});
		
		// DIA DEL MES
		setLayout(new BorderLayout(0, 5));
		
		JPanel panelDia = new JPanel();
		panelDia.setOpaque(false);
		panelDia.setLayout(new FlowLayout(FlowLayout.CENTER));
		add(panelDia, BorderLayout.CENTER);
		
		JLabel label = new JLabel(Integer.toString(fecha.getDia()));
		panelDia.add(label);

		panelContenido = new JPanel();
		panelContenido.setOpaque(false);
		panelContenido.setLayout(new BoxLayout(panelContenido, BoxLayout.Y_AXIS));
		add(panelContenido, BorderLayout.SOUTH);
	}
	
	void addContenido(OverPanel.Informacion contenido) {

		if(contenido == null)
			return;
		
		try {

			panelContenido.add(contenido.construir(getFecha()));
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		panelContenido.revalidate();
		panelContenido.repaint();
	}
	
	void addContenido(List<OverPanel.Informacion> contenido) {

		if(contenido == null)
			return;
		
		for(OverPanel.Informacion i : contenido) {

			addContenido(i);
		}
	}

	public void clearContenido() {

		panelContenido.removeAll();
		
		panelContenido.revalidate();
		panelContenido.repaint();
	}
	
	void addContenidoOver(OverPanel.Informacion contenido) {
		
		if(contenido == null)
			return;
		
		this.contenidoOver.add(contenido);
	}
	
	void addContenidoOver(List<OverPanel.Informacion> contenido) {

		if(contenido == null)
			return;
		
		this.contenidoOver.addAll(contenido);
	}
	
	public void clearOver() {
		
		if(over != null) {
	
			over.detener();
		}

		this.contenidoOver.clear();
	}
	
	void addAccion(OnDaySelected accion) {
		
		this.acciones.add(accion);
	}
	
	void addAcciones(List<OnDaySelected> acciones) {
		
		this.acciones.addAll(acciones);
	}
	
	// CLASES AUXILIARES	
	public interface OnDaySelected {
		
		public void selected(PanelDia panel, Fecha fecha);
	}
}
package com.repex.librerias.components.paneles;

import java.awt.Color;
import java.awt.Component;

import javax.swing.Box;

import org.jfree.ui.tabbedui.VerticalLayout;

import com.repex.librerias.components.paneles.BorderPanel;

public class VerticalPanelPadding extends BorderPanel {

	private static final long serialVersionUID = 1L;
	
	private int padding = 1; // Valor por defecto

	public VerticalPanelPadding() {
		super(0);
		inicilizar();
	}

	public VerticalPanelPadding(int padding) {
		super(0);
		this.padding = padding;
		inicilizar();
	}
	
	public VerticalPanelPadding(int borderSize, Color borderColor, int emptyBorderSize, boolean outterEmptyBorder, boolean innerEmptyBorder, int padding) {
		super(borderSize, borderColor, emptyBorderSize, outterEmptyBorder, innerEmptyBorder);
		this.padding = padding;
		inicilizar();
	}

	public VerticalPanelPadding(int borderSize, Color borderColor, int outterBorderSize, int innerBorderSize, boolean outterEmptyBorder, boolean innerEmptyBorder, int padding) {
		super(borderSize, borderColor, outterBorderSize, innerBorderSize, outterEmptyBorder, innerEmptyBorder);
		this.padding = padding;
		inicilizar();
	}


	public VerticalPanelPadding(int borderSize, Color borderColor, int padding) {
		super(borderSize, borderColor);
		this.padding = padding;
		inicilizar();
	}

	private void inicilizar() {
		setLayout(new VerticalLayout());
	}
	
	@Override
	public Component add(Component comp) {
		
		if(padding > 0 && getComponentCount() > 0) {
			super.add(Box.createVerticalStrut(padding));
		}
		
		return super.add(comp);
	}
	
	@Override
	public Component add(Component comp, int index) {
		
		// TODO:
		if(padding > 0 && index == 0 && getComponentCount() > 0) {
			super.add(Box.createVerticalStrut(padding), 0);
		}
		
		if(padding > 0 && index > 0 && getComponentCount() > 0) {
			
			index = (index * 2) - 1;
			index = Math.min(index, getComponentCount());
			
			super.add(Box.createVerticalStrut(padding), index++);
		}
		
		return super.add(comp, index);
	}
}
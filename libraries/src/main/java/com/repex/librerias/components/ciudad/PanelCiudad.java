package com.repex.librerias.components.ciudad;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.OverlayLayout;

import com.repex.librerias.components.ciudad.localidad.Localidad;
import com.repex.librerias.components.over.OverLocalidad;
import com.repex.librerias.components.over.OverPanel;
import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.utilidades.IconUtils;

public class PanelCiudad extends BorderPanel {
	
	private static final long serialVersionUID = 1L;

	// CONSTANTES
	public static int SIZE_DEFAULT = 140;
	
	// VARIABLES
	private int size;

	private List<OnLocationSelected> listeners;
	
	private List<Seleccion> seleccion;
	private Seleccion localidadOver;
	
	private OverLocalidad over;
	
	private boolean editable = true;
	
	// VARIABLES INTERNAS
	private List<Coordenadas> coordenadas;
	
	private boolean multiSeleccion = false;

	private float overTime = 0.5f;
		
	public PanelCiudad(int size, int borderSize, Color borderColor, List<OnLocationSelected> listeners) {
		
		super(borderSize, borderColor);

		this.size = Math.max(size, SIZE_DEFAULT);
		
		this.listeners = listeners;
		
		this.seleccion = new LinkedList<Seleccion>();		
		this.contenidoOver = new HashMap<Localidad, List<OverPanel.Informacion>>();
		
		crearCoordenadas();
		inicializar();
	}

	public PanelCiudad(int size, int borderSize, Color borderColor) {
	
		this(size, borderSize, borderColor, new LinkedList<OnLocationSelected>());
	}
	
	public PanelCiudad(int size, int borderSize) {
		
		this(size, borderSize, BORDER_COLOR_DEFAULT);
	}

	public PanelCiudad(int size) {
		
		this(size, BORDER_SIZE_DEFAULT, BORDER_COLOR_DEFAULT);
	}
	
	public int getTamano() {
		
		return this.size;
	}
	
	public void setTamano(int tamano) {

		this.size = Math.max(tamano, SIZE_DEFAULT);
		inicializar();
		
		revalidate();
		repaint();
	}
	
	public void setEditable(boolean editable) {
		
		this.editable = editable;
	}
	
	public boolean isEditable() {
		
		return editable;
	}
	
	public void setMultiSeleccion(boolean multiSeleccion) {
		
		this.multiSeleccion = multiSeleccion;
	}
	
	public boolean isMultiSeleccion() {
		
		return multiSeleccion;
	}
	
	public void setOverTime(float overTime) {
		
		this.overTime = overTime;
	}
	
	// CONTENIDO OVER DE PANELES
	private Map<Localidad, List<OverPanel.Informacion>> contenidoOver;	
	
	public void addOver(Localidad localidad, List<OverPanel.Informacion> contenido) {
		
		List<OverPanel.Informacion> acciones = contenidoOver.get(localidad);		
		if(acciones != null) {
			
			acciones.addAll(contenido);
			
		} else {
			
			contenidoOver.put(localidad, contenido);
		}
	}

	public void addOver(Localidad localidad, OverPanel.Informacion contenido) {
		
		List<OverPanel.Informacion> c = new LinkedList<OverPanel.Informacion>();
		c.add(contenido);
		
		this.addOver(localidad, c);
	}
	
	public void overrideOver(Localidad localidad, List<OverPanel.Informacion> contenido) {

		List<OverPanel.Informacion> acciones = contenidoOver.get(localidad);
		if(acciones != null) {
			
			acciones.clear();
			acciones.addAll(contenido);
			
		} else {
			
			contenidoOver.put(localidad, contenido);
		}
	}
	
	// ACCION	
	public void addOnLocationListener(OnLocationSelected listener) {
		
		this.listeners.add(listener);
	}
	
	public void clearListener() {
	
		this.listeners.clear();
	}
	
	// SELECCION
	public List<Seleccion> getSeleccionada() {
		
		return seleccion;
	}

	public boolean addSeleccion(Localidad localidad) {
		
		if(over != null) {
			
			over.detener();
		}
		
		if(localidad != null) {
			
			Seleccion seleccion = null;
			for(Seleccion s : this.seleccion) {
			
				if(s.getLocalidad().equals(localidad)) {
					
					seleccion = s;
				}
			}
			
			if(seleccion != null) {
				
				quitar(seleccion);
				
			} else {
				
				if(!multiSeleccion && !this.seleccion.isEmpty()) {
				
					quitar(this.seleccion.get(0));
				}
				
				this.seleccion.add(new Seleccion(localidad, fijar(localidad.getIcono())));
				return true;
			}
		}
		
		return false;
	}

	public boolean addSeleccion(Localidad localidad, boolean listener) {
		
		if(addSeleccion(localidad)) {

			if(listener) {
				
				for(OnLocationSelected l : PanelCiudad.this.listeners) {

					l.selected(seleccion);
				}
			}
			
			return true;
		}
		
		return false;
	}
	
	public void clear() {
		
		List<Seleccion> copia = new LinkedList<Seleccion> (seleccion);
		if(!copia.isEmpty()) {
			
			for(Seleccion seleccion : copia) {

				quitar(seleccion);
			}

			for(OnLocationSelected listener : PanelCiudad.this.listeners) {

				listener.selected(seleccion);
			}
		}		
	}
	
	private void crearCoordenadas() {
		
		// COORDENADAS
		coordenadas = new LinkedList<Coordenadas>();
		coordenadas.add(new Coordenadas(Localidad.ALICANTE, new Coordenada(105, 97), new Coordenada(82, 65)));
		coordenadas.add(new Coordenadas(Localidad.VALENCIA, new Coordenada(103, 91), new Coordenada(70, 57)));
		coordenadas.add(new Coordenadas(Localidad.CASTELLON, new Coordenada(103, 96), new Coordenada(57, 46)));

		coordenadas.add(new Coordenadas(Localidad.ZARAGOZA, new Coordenada(101, 84), new Coordenada(45, 29)));
		coordenadas.add(new Coordenadas(Localidad.HUESCA, new Coordenada(103, 91), new Coordenada(38, 25)));
		coordenadas.add(new Coordenadas(Localidad.TERUEL, new Coordenada(100, 88), new Coordenada(55, 42)));

		coordenadas.add(new Coordenadas(Localidad.TARRAGONA, new Coordenada(112, 102), new Coordenada(45, 36)));
		coordenadas.add(new Coordenadas(Localidad.GIRONA, new Coordenada(124, 113), new Coordenada(32, 23)));
		coordenadas.add(new Coordenadas(Localidad.BARCELONA, new Coordenada(119, 110), new Coordenada(37, 25)));
		coordenadas.add(new Coordenadas(Localidad.LLEIDA, new Coordenada(113, 103), new Coordenada(38, 23)));

		coordenadas.add(new Coordenadas(Localidad.HUELVA, new Coordenada(52, 44), new Coordenada(95, 82)));
		coordenadas.add(new Coordenadas(Localidad.CADIZ, new Coordenada(63, 55), new Coordenada(102, 92)));				
		coordenadas.add(new Coordenadas(Localidad.MALAGA, new Coordenada(75, 62), new Coordenada(98, 90)));
		coordenadas.add(new Coordenadas(Localidad.CORDOBA, new Coordenada(71, 59), new Coordenada(88, 74)));
		coordenadas.add(new Coordenadas(Localidad.GRANADA, new Coordenada(86, 73), new Coordenada(93, 80)));
		coordenadas.add(new Coordenadas(Localidad.JAEN, new Coordenada(83, 70), new Coordenada(85, 75)));
		coordenadas.add(new Coordenadas(Localidad.ALMERIA, new Coordenada(92, 82), new Coordenada(91, 81)));
		coordenadas.add(new Coordenadas(Localidad.SEVILLA, new Coordenada(66, 53), new Coordenada(94, 81)));

		coordenadas.add(new Coordenadas(Localidad.CORUNA, new Coordenada(35, 26), new Coordenada(32, 22)));
		coordenadas.add(new Coordenadas(Localidad.LUGO, new Coordenada(43, 36), new Coordenada(36, 23)));
		coordenadas.add(new Coordenadas(Localidad.PONTEVEDRA, new Coordenada(33, 26), new Coordenada(42, 33)));
		coordenadas.add(new Coordenadas(Localidad.OURENSE, new Coordenada(43, 33), new Coordenada(42, 36)));

		coordenadas.add(new Coordenadas(Localidad.SANTA_CRUZ_DE_TENERIFE, new Coordenada(26, 9), new Coordenada(117, 103)));
		coordenadas.add(new Coordenadas(Localidad.LAS_PALMAS, new Coordenada(44, 27), new Coordenada(117, 103)));

		coordenadas.add(new Coordenadas(Localidad.CACERES, new Coordenada(60, 44), new Coordenada(69, 59)));
		coordenadas.add(new Coordenadas(Localidad.BADAJOZ, new Coordenada(62, 45), new Coordenada(81, 67)));

		coordenadas.add(new Coordenadas(Localidad.NAVARRA, new Coordenada(88, 80), new Coordenada(35, 22)));
		
		coordenadas.add(new Coordenadas(Localidad.LA_RIOJA, new Coordenada(83, 74), new Coordenada(36, 30)));

		coordenadas.add(new Coordenadas(Localidad.ASTURIAS, new Coordenada(59, 43), new Coordenada(29, 23)));
		
		coordenadas.add(new Coordenadas(Localidad.CANTABRIA, new Coordenada(69, 60), new Coordenada(28, 22)));
		
		coordenadas.add(new Coordenadas(Localidad.BALEARES, new Coordenada(133, 115), new Coordenada(65, 45)));

		coordenadas.add(new Coordenadas(Localidad.ALAVA, new Coordenada(79, 73), new Coordenada(29, 25)));
		coordenadas.add(new Coordenadas(Localidad.GUIPUZCOA, new Coordenada(81, 76), new Coordenada(25, 22)));
		coordenadas.add(new Coordenadas(Localidad.VIZCAYA, new Coordenada(75, 71), new Coordenada(25, 22)));
		
		coordenadas.add(new Coordenadas(Localidad.ALBACETE, new Coordenada(97, 81), new Coordenada(78, 63)));
		coordenadas.add(new Coordenadas(Localidad.TOLEDO, new Coordenada(78, 60), new Coordenada(66, 58)));
		coordenadas.add(new Coordenadas(Localidad.CIUDAD_REAL, new Coordenada(80, 64), new Coordenada(75, 64)));
		coordenadas.add(new Coordenadas(Localidad.GUADALAJARA, new Coordenada(88, 73), new Coordenada(55, 45)));
		coordenadas.add(new Coordenadas(Localidad.CUENCA, new Coordenada(90, 77), new Coordenada(64, 51)));
		
		coordenadas.add(new Coordenadas(Localidad.LEON, new Coordenada(58, 44), new Coordenada(37, 26)));
		coordenadas.add(new Coordenadas(Localidad.PALENCIA, new Coordenada(66, 59), new Coordenada(40, 28)));
		coordenadas.add(new Coordenadas(Localidad.BURGOS, new Coordenada(73, 64), new Coordenada(42, 25)));
		coordenadas.add(new Coordenadas(Localidad.SORIA, new Coordenada(82, 72), new Coordenada(45, 36)));		
		coordenadas.add(new Coordenadas(Localidad.ZAMORA, new Coordenada(58, 45), new Coordenada(47, 38)));
		coordenadas.add(new Coordenadas(Localidad.VALLADOLID, new Coordenada(67, 57), new Coordenada(47, 37)));
		coordenadas.add(new Coordenadas(Localidad.SEGOVIA, new Coordenada(71, 63), new Coordenada(52, 43)));
		coordenadas.add(new Coordenadas(Localidad.SALAMANCA, new Coordenada(57, 46), new Coordenada(57, 48)));
		coordenadas.add(new Coordenadas(Localidad.AVILA, new Coordenada(65, 57), new Coordenada(59, 48)));

		coordenadas.add(new Coordenadas(Localidad.MADRID, new Coordenada(76, 66), new Coordenada(59, 47)));
		
		coordenadas.add(new Coordenadas(Localidad.MURCIA, new Coordenada(98, 88), new Coordenada(81, 73)));

		coordenadas.add(new Coordenadas(Localidad.CEUTA, new Coordenada(69, 63), new Coordenada(111, 105)));

		coordenadas.add(new Coordenadas(Localidad.MELILLA, new Coordenada(86, 80), new Coordenada(114, 111)));
	}
	
	private void inicializar() {
		
		// LIMPIAR
		removeAll();
		
		for(MouseListener listener : getMouseListeners()) {
			removeMouseListener(listener);
		}

		for(MouseMotionListener listener : getMouseMotionListeners()) {
			removeMouseMotionListener(listener);
		}
		
		// BASICO
		setBackground(Color.WHITE);
	    setLayout(new OverlayLayout(this));
	    
	    // FONDO
		JLabel lblMapa = new JLabel();
		lblMapa.setIcon(IconUtils.escalar(getClass().getClassLoader().getResource(Localidad.CARPETA_RECURSOS + "mapa.png"), size));
		add(lblMapa);
		
		MouseAdapter adapter = new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {

				if(isEditable()) {
					
					Coordenadas actual = null;				
					for(Coordenadas c : coordenadas) {

						float escala = (float)size / SIZE_DEFAULT;

						if(c.getX().comparar(new Coordenada(e.getX(), e.getX()), escala) && c.getY().comparar(new Coordenada(e.getY(), e.getY()), escala)) {

							actual = c;
						}
					}

					if(actual != null) {

						addSeleccion(actual.getLocalidad());

						// EJECUTAR ACCION
						for(OnLocationSelected listener : PanelCiudad.this.listeners) {

							listener.selected(seleccion);
						}

					} else if(!multiSeleccion && !seleccion.isEmpty()) {

						quitar(seleccion.get(0));
					}
				}
			}
			
			@Override
			public void mouseMoved(MouseEvent e) {

				if(isEditable()) {
					
					Coordenadas actual = null;				
					for(Coordenadas c : coordenadas) {

						float escala = (float)size / SIZE_DEFAULT;

						if(c.getX().comparar(new Coordenada(e.getX(), e.getX()), escala) && c.getY().comparar(new Coordenada(e.getY(), e.getY()), escala)) {

							actual = c;
						}
					}

					if(actual != null && (localidadOver == null || !localidadOver.getLocalidad().equals(actual.getLocalidad()))) {

						// QUITAR OVER
						if(over != null) {

							over.detener();
						}

						// MOSTRAR OVER LOCALIDAD
						List<OverPanel.Informacion> contenido = contenidoOver.get(actual.getLocalidad());

						over = new OverLocalidad(actual.getLocalidad(), contenido);
						over.lanzar(overTime);

						if(localidadOver != null) {

							quitar(localidadOver);
						}

						localidadOver = new Seleccion(actual.getLocalidad(), fijar(actual.getLocalidad().getIcono()));

					} else if(actual == null && localidadOver != null) {

						// QUITAR OVER
						if(over != null) {

							over.detener();
						}

						// QUITAR OVER LOCALIDAD
						quitar(localidadOver);
						localidadOver = null;
					}
				}
			}
			
			@Override
			public void mouseExited(MouseEvent e) {

				if(isEditable()) {
					
					// QUITAR LOCALIDAD OVER
					if(localidadOver != null) {

						quitar(localidadOver);
						localidadOver = null;
					}

					// QUITAR OVER
					if(over != null) {

						over.detener();
					}
				}
			}
		};

		lblMapa.addMouseListener(adapter);
		lblMapa.addMouseMotionListener(adapter);
	}
	
	// FUNCIONES
	private JLabel fijar(Icon icono) {
		
		// CAMBIAR TAMANO
		Icon nuevo = IconUtils.escalar(icono, size);
		
		// ANADIR		
		JLabel otra = new JLabel();
		otra.setIcon(nuevo);
		add(otra, 0);
		
		revalidate();
		repaint();
		
		return otra;
	}
	
	private void quitar(Seleccion seleccion) {
		
		this.seleccion.remove(seleccion);
		remove(seleccion.getLabel());

		revalidate();
		repaint();
	}
	
	// CLASES AUXILIARES PARA COORDENADAS
	public interface OnLocationSelected {
		
		public void selected(List<Seleccion> seleccion);
	}
	
	public static class Seleccion {
	
		private Localidad localidad;
		private JLabel label;
		
		public Seleccion(Localidad localidad, JLabel label) {
			
			this.localidad = localidad;
			this.label = label;
		}
		
		public Localidad getLocalidad() {
			
			return localidad;
		}
		
		public JLabel getLabel() {
			
			return label;
		}
		
		@Override
		public boolean equals(Object obj) {
			
			if (this == obj)
				return true;
			
			if (obj == null)
				return false;
			
			if (getClass() != obj.getClass())
				return false;
			
			Seleccion other = (Seleccion) obj;
			return getLocalidad().equals(other.getLocalidad()) && getLabel().equals(other.getLabel());
		}
	}
	
	private class Coordenadas {
	
		private Localidad localidad;
		
		private Coordenada x;
		private Coordenada y;
		
		public Coordenadas(Localidad localidad, Coordenada x, Coordenada y) {
			
			this.localidad = localidad;
			
			this.x = x;
			this.y = y;
		}
		
		public Localidad getLocalidad() {
			
			return localidad;
		}
		
		public Coordenada getX() {
			return x;
		}
		
		public Coordenada getY() {
			
			return y;
		}
	}
	
	private class Coordenada {
		
		private float maximo;
		private float minimo;
		
		public Coordenada(float maximo, float minimo) {
			
			this.maximo = maximo;
			this.minimo = minimo;
		}
		
		public float getMaximo() {
			
			return maximo;
		}
		
		public float getMinimo() {
			
			return minimo;
		}

		public float getMaximo(float escala) {
			
			return maximo*escala;
		}
		
		public float getMinimo(float escala) {
			
			return minimo*escala;
		}
		
		public boolean comparar(Coordenada otra, float escala) {
			
			return otra.getMinimo() >= getMinimo(escala) && otra.getMaximo() <= getMaximo(escala);
		}
	}
}
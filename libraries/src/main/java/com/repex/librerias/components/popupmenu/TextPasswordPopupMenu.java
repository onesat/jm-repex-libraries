package com.repex.librerias.components.popupmenu;

import java.util.LinkedList;
import java.util.List;

import javax.swing.Icon;

import com.repex.librerias.components.textfield.HintPasswordField;
import com.repex.librerias.utilidades.Constantes;
import com.repex.librerias.utilidades.IconUtils;

public class TextPasswordPopupMenu extends CopyPastePopupMenu {

	private static final long serialVersionUID = 1L;

	public TextPasswordPopupMenu(HintPasswordField parent) {
		
		super(parent);
	}

	@Override
	public void crear() {

		final HintPasswordField parent = (HintPasswordField) getPadre();
		
		// OCULTAR SI YA ESTA VISIBLE
		List<Opcion> opciones = new LinkedList<Opcion>();
		
		if(parent.getEchoChar() == '*') {
			
			// MOSTRAR
			Icon icono = IconUtils.escalar(getClass().getClassLoader().getResource(Constantes.CARPETA_RECURSOS + "textfield/ver_contrasena.png"), 20);
			opciones.add(new MyPopupMenu.Opcion(icono, "Ver contraseña", new Runnable() {
				
				public void run() {
					
					parent.setEchoChar((char) 0);
					parent.setShow(true);
				}
			}));
			
		} else {

			// OCULTAR
			opciones.add(new MyPopupMenu.Opcion("Ocultar contraseña", new Runnable() {
				
				public void run() {
					
					parent.setEchoChar('*');
					parent.setShow(false);
				}
			}));
		}
		
		this.overrideOpciones(opciones);
		super.crear();
	}
}
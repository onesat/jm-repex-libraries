package com.repex.librerias.components.animacion.window;

import java.awt.Window;

public class FadeIn extends WindowAnimation {
		
	public FadeIn(Window window, double second) {
		
		super(window, second);
		
		this.addOnAnimationListener(new OnAnimationListener() {

			public void onStart() {

				getWindow().setOpacity(0);
			}

			public void onEnd() {}
		});
	}

	public FadeIn(Window window) {
		
		this(window, 0.5);
	}

	@Override
	protected void animacion(float factor) {

		getWindow().setOpacity(Math.min(factor, 1));
	}
}
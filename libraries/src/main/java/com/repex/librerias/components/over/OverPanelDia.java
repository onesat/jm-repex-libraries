package com.repex.librerias.components.over;

import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.repex.librerias.date.Fecha;

public class OverPanelDia extends OverPanel {
	
	private static final long serialVersionUID = 1L;

	private Fecha fecha;
	
	private List<OverPanel.Informacion> contenido;

	// CONSTANTES
	private static final int MARGIN = 2;
		
	public OverPanelDia(Fecha fecha, List<OverPanel.Informacion> contenido) {

		super(0, 0, new Margins(MARGIN, MARGIN, MARGIN, MARGIN));
		
		this.fecha = fecha;
		this.contenido = contenido;
	}
	
	@Override
	protected void crear() {
		
		super.crear();
		pack();
	}
	
	@Override
	protected JPanel construir() {

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setOpaque(false);

		JPanel p = new JPanel();
		p.setOpaque(false);
		p.add(new JLabel(fecha.parse(Fecha.FORMATO_SIN_HORA)));
		panel.add(p);
		
		for(OverPanel.Informacion i : contenido) {
			
			try {

				panel.add(i.construir(fecha));
				
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		}
		
		return panel;
	}
}
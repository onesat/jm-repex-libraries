package com.repex.librerias.components.paneles.lista;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.components.utilidades.Constantes;
import com.repex.librerias.components.utilidades.OnDestroyPanel;
import com.repex.librerias.components.utilidades.OnLaterConstructor;
import com.repex.librerias.components.utilidades.OnResizePanel;

public class PanelLista extends BorderPanel {
	
	private static final long serialVersionUID = 1L;
	
	private List<JComponent> datos;
	
	private int spacing;
	private int limite;
	
	private Orientacion orientacion;
	
	private boolean hasScroll = true;
	
	public PanelLista(int spacing, int limite, Orientacion orientacion, boolean hasScroll) {
		
		super(0);
		
		this.datos = new LinkedList<JComponent>();
		
		this.spacing = spacing;
		this.limite = limite;
		
		this.orientacion = orientacion;
		
		this.hasScroll = hasScroll;
		
		inicializar();
	}

	public PanelLista(int spacing, int limite, Orientacion orientacion) {
		this(spacing, limite, orientacion, true);
	}

	public PanelLista(int spacing, Orientacion orientacion, boolean hasScroll) {
		this(spacing, 0, orientacion, hasScroll);
	}
	
	public PanelLista(int spacing, Orientacion orientacion) {
		this(spacing, 0, orientacion);
	}
	
	public PanelLista(int spacing, int limite) {		
		this(spacing, limite, Orientacion.VERTICAL);
	}

	public PanelLista(int spacing) {		
		this(spacing, 0);
	}

	public PanelLista(int spacing, boolean hasScroll) {		
		this(spacing, 0, Orientacion.VERTICAL, hasScroll);
	}
	
	public PanelLista() {
		
		this(0);
	}
	
	public void setSpacing(int spacing) {
		
		this.spacing = spacing;
	}
	
	private JPanel panelLista;
	private JScrollPane scroll;
	
	private void inicializar() {
		
		setOpaque(false);
		setLayout(new BorderLayout(0, 0));

		// RESULTADOS		
		panelLista = new JPanel();
		panelLista.setOpaque(false);

		if(hasScroll) {
			
			scroll = new JScrollPane(panelLista);
			scroll.getViewport().setOpaque(false);
			scroll.setOpaque(false);
			scroll.setBorder(null);
			add(scroll, BorderLayout.CENTER);
			
		} else {

			add(panelLista, BorderLayout.CENTER);
		}
		
		if(this.orientacion == Orientacion.VERTICAL) {
			panelLista.setLayout(new BoxLayout(panelLista, BoxLayout.PAGE_AXIS));
			if(scroll != null) {
				scroll.getVerticalScrollBar().setUnitIncrement(Constantes.VELOCIDAD_SCROLL);
			}
		} else {
			panelLista.setLayout(new BoxLayout(panelLista, BoxLayout.LINE_AXIS));
			if(scroll != null) {
				scroll.getHorizontalScrollBar().setUnitIncrement(Constantes.VELOCIDAD_SCROLL);
			}
		}
	}

	public List<JComponent> getDatos() {
		return datos;
	}
	
	public int getChilds() {
		return datos.size();
	}
	
	public int getCount() {		
		return panelLista.getComponentCount();
	}
	
	public JComponent getChild(int indice) {
		
		if(indice < 0 || indice >= datos.size())
			return null;
		
		return datos.get(indice);
	}
	
	public void moveTo(int position) {
		
		int height = 0;
		if(position - 1 >= 0) {

			Component component = panelLista.getComponent(position - 1);
			height = component.getHeight();
		}
		
		scroll.getVerticalScrollBar().setValue(height * position);
	}
	
	public void reset() {

		for(Component component : panelLista.getComponents()) {		
			if(component instanceof OnDestroyPanel) {				
				((OnDestroyPanel) component).onDestroy();
			}
		}
		
		panelLista.removeAll();
		datos.clear();
		
		revalidate();
		repaint();
	}

	public void addDato(JComponent panel, int posicion) {

		// AJUSTAR
		int original = Math.min(posicion, getChilds());
		
		int newPosicion = getChild(posicion - 1) == null ? original : getComponentIndex(getChild(posicion - 1)) + 1;
		
		// ESPACIO
		if(posicion > 0 && spacing > 0 && getChilds() > 0) {

			panelLista.add(Box.createRigidArea(new Dimension(orientacion == Orientacion.HORIZONTAL ? spacing : 0,
					orientacion == Orientacion.VERTICAL ? spacing : 0)), newPosicion++);
		}

		panelLista.add(panel, newPosicion);
		datos.add(original, panel);
		
		if(posicion == 0 && spacing > 0 && getChilds() > 1) {

			panelLista.add(Box.createRigidArea(new Dimension(orientacion == Orientacion.HORIZONTAL ? spacing : 0,
					orientacion == Orientacion.VERTICAL ? spacing : 0)), newPosicion+1);
		}
		
		if(panel instanceof OnResizePanel) {				
			((OnResizePanel) panel).resize();
		}
		
		if(panel instanceof OnLaterConstructor) {
			((OnLaterConstructor) panel).onConstructor();
		}
		
		update();
	}
	
	public void addDato(JComponent panel) {
		this.addDato(panel, getChilds());
	}
	
	public void removeDato(JPanel panel) {
		
		int index = getComponentIndex(panel);
		if(index >= 0) {

			if(spacing > 0 && getChilds() > 1) {
				panelLista.remove(index > 0 ? index - 1 : index + 1);
			}
			
			panelLista.remove(panel);
			datos.remove(panel);
			
			if(panel instanceof OnDestroyPanel) {				
				((OnDestroyPanel) panel).onDestroy();
			}
		}
		
		update();
	}
	
	private void update() {

		if(limite > 0) {
			
			if((spacing > 0 && panelLista.getComponentCount() > limite * 2) || spacing == 0 && panelLista.getComponentCount() > limite) {
				
				panelLista.revalidate();
				panelLista.repaint();
				
			} else {

				revalidate();
				repaint();
			}
			
		} else {

			revalidate();
			repaint();
		}
	}
	
	private int getComponentIndex(JComponent panel) {
		
		for (int i = 0; i < panelLista.getComponentCount(); i++) {
			if (panelLista.getComponent(i).equals(panel))
				return i;
		}

		return -1;
	}
	public enum Orientacion {
		
		HORIZONTAL,
		VERTICAL
	}
}
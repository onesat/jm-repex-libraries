package com.repex.librerias.components.animacion.window;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.IllegalComponentStateException;
import java.awt.Window;

import com.repex.librerias.components.animacion.MyAnimation;

public abstract class WindowAnimation extends MyAnimation {
	
	private Window window;
	
	public WindowAnimation(Window window, double second) throws IllegalComponentStateException {
		
		super(second);
		
		if(window instanceof Frame && !((Frame) window).isUndecorated()) {
			
			throw new IllegalComponentStateException("Frame need to be undecorated");
		}

		if(window instanceof Dialog && !((Dialog) window).isUndecorated()) {
			
			throw new IllegalComponentStateException("Dialog need to be undecorated");
		}
		
		this.window = window;
	}
	
	public WindowAnimation(Window window) {
		
		this(window, 0.5);
	}
	
	public Window getWindow() {
		
		return window;
	}
}
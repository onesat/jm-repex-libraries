package com.repex.librerias.components.botones;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import com.repex.librerias.components.labels.LabelPanel;

public class BotonLabel extends LabelPanel {
	
	private static final long serialVersionUID = 1L;
	
	// CONSTANTES
	private Color background = Color.decode("#43546e");
	private Color over = Color.decode("#50555d");
	
	// VARIABLES
	private String texto;

	public BotonLabel(String texto) {
		
		this(texto, Color.decode("#43546e"), Color.decode("#50555d"));
	}
	
	public BotonLabel(String texto, Color background, Color over) {
		
		super();
		
		this.texto = texto;
		
		this.background = background;
		this.over = over;
		
		inicializar();
	}
	
	private void inicializar() {
		
		setInnerBorderSize(10);
		setInnerEmptyBorder(true);
		setOutterEmptyBorder(true);
		
		setShape(new LabelPanel.RoundedShape(new Dimension(5, 5)));
		
		setLayout(new BorderLayout(0, 0));
		
		setBackgroundColor(background);
		setOverColor(over);
		
		// LABEL
		JLabel lblAceptar = new JLabel(texto);
		lblAceptar.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblAceptar.setForeground(Color.WHITE);
		
		lblAceptar.setHorizontalAlignment(SwingConstants.CENTER);
		
		add(lblAceptar, BorderLayout.CENTER);
	}
}
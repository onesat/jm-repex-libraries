package com.repex.librerias.components.calendario;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JLabel;

import com.repex.librerias.components.paneles.BorderPanel;

public class PanelCabecera extends BorderPanel {

	private static final long serialVersionUID = 1L;
	
	// VARIABLES
	private int size;

	private final Color COLOR_NORMAL = Color.WHITE;
	
	// VARIABLES INTERNAS
	private String titulo;
	
	public PanelCabecera(String titulo, int size, int borderSize, Color borderColor) {
		
		super(borderSize, borderColor);

		this.titulo = titulo;
		this.size = size;
		
		inicializar();
	}
	
	public PanelCabecera(String titulo, int size, int borderSize) {
		
		this(titulo, size, borderSize, BORDER_COLOR_DEFAULT);
	}

	public PanelCabecera(String titulo, int size) {
		
		this(titulo, size, BORDER_SIZE_DEFAULT, BORDER_COLOR_DEFAULT);
	}
	
	private void inicializar() {

		// TAMANO
		Dimension d = new Dimension(size+getBorderSize(), 30);
		setPreferredSize(d);
		setMaximumSize(d);
		setMinimumSize(d);
		
		// CONFIGURACION
		setBackground(COLOR_NORMAL);
		setLayout(new FlowLayout(FlowLayout.CENTER));
				
		setHasBorder(false);
		
		// TITULO
		JLabel label = new JLabel(titulo);
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		add(label);
	}
}
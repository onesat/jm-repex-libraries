package com.repex.librerias.components.paneles;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class BorderPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	// CONSTANTES
	public static int BORDER_SIZE_DEFAULT = 2;
	public static Color BORDER_COLOR_DEFAULT = Color.BLACK;

	// VARIABLES
	private int borderSize;
	private Color borderColor;
	
	private boolean hasBorder = true;
	
	private boolean bottomBorder = false;
	
	// EMPTY BORDER
	private int outterBorderSize;
	private int innerBorderSize;
	private boolean outterEmptyBorder;
	private boolean innerEmptyBorder;
	
	public BorderPanel(int borderSize, Color borderColor, int outterBorderSize, int innerBorderSize, boolean outterEmptyBorder, boolean innerEmptyBorder) {
				
		if(borderSize == 0) {
			
			hasBorder = false;
		}
		
		this.borderSize = borderSize;
		this.borderColor = borderColor;
		
		this.outterBorderSize = outterBorderSize;
		this.innerBorderSize = innerBorderSize;
		this.outterEmptyBorder = outterEmptyBorder;
		this.innerEmptyBorder = innerEmptyBorder;
		
		inicializar();
	}

	public BorderPanel(int borderSize, Color borderColor, int emptyBorderSize, boolean outterEmptyBorder, boolean innerEmptyBorder) {
	
		this(borderSize, borderColor, emptyBorderSize, emptyBorderSize, outterEmptyBorder, innerEmptyBorder);
	}
	
	public BorderPanel(int borderSize, Color borderColor) {
		
		this(borderSize, borderColor, 0, false, false);
	}
	
	public BorderPanel(int borderSize) {
		
		this(borderSize, BORDER_COLOR_DEFAULT);
	}

	
	public BorderPanel() {
		
		this(BORDER_SIZE_DEFAULT, BORDER_COLOR_DEFAULT);
	}
	
	// GET/SET
	public int getBorderSize() {
		
		if(!hasBorder)
			return 0;
		
		return borderSize;
	}

	public void setBorderSize(int borderSize) {
		
		this.borderSize = borderSize;
		setBorder(createBorder());
	}

	public Color getBorderColor() {
		
		return borderColor;
	}

	public void setBorderColor(Color borderColor) {
		
		this.borderColor = borderColor;
		setBorder(createBorder());
	}

	public void setHasBorder(boolean hasBorder) {
		
		this.hasBorder = hasBorder;
		setBorder(createBorder());
	}
	
	public boolean hasBorder() {
		
		return hasBorder;
	}
	
	public boolean isBottomBorder() {
		
		return bottomBorder;
	}
	
	public void setBottomBorder(boolean bottomBorder) {
		
		this.bottomBorder = bottomBorder;
		setBorder(createBorder());
	}
	
	public int getOutterBorderSize() {

		if(!outterEmptyBorder)
			return 0;
		
		return outterBorderSize;
	}
	
	public int getInnerBorderSize() {

		if(!innerEmptyBorder)
			return 0;
		
		return innerBorderSize;
	}
	
	public void setOutterBorderSize(int outterBorderSize) {
		
		this.outterBorderSize = outterBorderSize;
		setBorder(createBorder());
	}
	
	public void setInnerBorderSize(int innerBorderSize) {
		
		this.innerBorderSize = innerBorderSize;
		setBorder(createBorder());
	}

	public void setEmptyBorderSize(int emptyBorderSize) {

		this.outterBorderSize = emptyBorderSize;
		this.innerBorderSize = emptyBorderSize;
		setBorder(createBorder());		
	}

	public boolean isOutterEmptyBorder() {
		
		return outterEmptyBorder;
	}

	public void setOutterEmptyBorder(boolean outterEmptyBorder) {
		
		this.outterEmptyBorder = outterEmptyBorder;
		setBorder(createBorder());
	}

	public boolean isInnerEmptyBorder() {
		
		return innerEmptyBorder;
	}

	public void setInnerEmptyBorder(boolean innerEmptyBorder) {
		
		this.innerEmptyBorder = innerEmptyBorder;
		setBorder(createBorder());
	}

	// INICIALIZAR
	private void inicializar() {
		
		setBorder(createBorder());
	}

	protected Border createBorder() {

		Border border = null;
		
		if(hasBorder()) {
			
			if(isBottomBorder()) {

				border = BorderFactory.createMatteBorder(0, 0, borderSize, 0, borderColor);
				
			} else {
			
				border = BorderFactory.createMatteBorder(borderSize, borderSize, borderSize, borderSize, borderColor);
			}
		}
		
		if(outterEmptyBorder) {
			
			border = BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(outterBorderSize, outterBorderSize, outterBorderSize, outterBorderSize), border);
		}

		if(innerEmptyBorder) {
			
			border = BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(innerBorderSize, innerBorderSize, innerBorderSize, innerBorderSize));
		}
		
		return border;
	}
	
	@Override
	protected void paintComponent(Graphics g) {

		super.paintComponent(g);
		
		if(isOpaque() && isOutterEmptyBorder()) {			
			
			Graphics2D graphics = (Graphics2D) g;
			
			graphics.setColor(getBackground());
			graphics.fillRect(outterBorderSize, outterBorderSize, getWidth() - (outterBorderSize*2), getHeight() - (outterBorderSize*2));
			
			Color color = Color.WHITE;
			Component parent = getParent();
			
			do {
				
				if(parent.isOpaque()) {
				
					color = parent.getBackground();
					parent = null;
					
				} else {
					
					parent = parent.getParent();
				}
				
			} while(parent != null);
			
			graphics.setColor(color);
			graphics.setStroke(new BasicStroke(outterBorderSize*2));
			graphics.drawRect(0, 0, getWidth(), getHeight());
		}
	}
}
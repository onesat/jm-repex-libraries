package com.repex.librerias.components.animacion;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Timer;

public abstract class MyAnimation {

	public final static int DELAY = 10;

	// CALCULADAS
	private Timer timer;
	private double incremento;
	
	// VARIABLES
	private double second;
	
	private List<OnAnimationListener> listeners;
	
	public MyAnimation(double second) {
		
		this.second = second;
		
		this.listeners = new LinkedList<OnAnimationListener>();
		
		calcular();
	}

	private void calcular() {

		double ticks = (getSecond()*1000)/DELAY; 
		incremento = 1/ticks;	
	}
	
	public double getSecond() {
		
		return second;
	}
	
	public void setSecond(double second) {
		
		this.second = second;
		calcular();
	}
	
	public void start() {

		for(OnAnimationListener listener : listeners) {
			
			listener.onStart();
		}
		
		// INICIAR TIMER
		timer = new Timer(DELAY, null);
		timer.setRepeats(true);
		timer.addActionListener(new ActionListener() {
			
			private float factor = 0;
			
			public void actionPerformed(ActionEvent e) {
				
				factor += incremento;
				animacion(factor);
				
				if (factor >= 1) {

					for(OnAnimationListener listener : listeners) {
						
						listener.onEnd();
					}
					
					timer.stop();		
				}
			}
		});

		timer.start();
	}
	
	protected abstract void animacion(float factor);
	
	// LISTENER	
	public void addOnAnimationListener(OnAnimationListener listener) {
		
		this.listeners.add(listener);
	}
	
	public interface OnAnimationListener {
		
		public void onStart();
		public void onEnd();
	}
}
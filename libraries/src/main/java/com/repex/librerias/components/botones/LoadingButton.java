package com.repex.librerias.components.botones;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.repex.librerias.utilidades.Constantes;
import com.repex.librerias.utilidades.IconUtils;

public class LoadingButton extends JPanel {

	private static final long serialVersionUID = 1L;
		
	private final static String CARPETA_RECURSOS = Constantes.CARPETA_RECURSOS + "botones/";
	
	// CONSTANTES
	private static final Color COLOR_NORMAL = Color.decode("#DDDDEE");
	private static final Color COLOR_OVER = Color.decode("#BBBBCC");

	private static final Color COLOR_BORDER = Color.DARK_GRAY;
	
	private static final int PADDING = 5;
		
	// VARIABLES
	private Icon icono;
	private String texto;
	
	private boolean error; // INDICA SI ES ERROR
	private boolean showErrorAsImage;

	private boolean loading; // INDICA SI ESTA CARGANDO
	private boolean showLoadingAsImage;
	
	private String mensajeError;
	private int tiempoError; // TIEMPO QUE SE MUESTRA EL ERROR
		
	public LoadingButton(Icon icono, String texto, final Color backgroundColor, final Color overColor, String mensajeError, int tiempoError) {
		
		this.icono = icono;
		this.texto = texto;
		
		this.mensajeError = mensajeError;
		this.tiempoError = tiempoError;
		
		// CONFIGURACION
		setLayout(new BorderLayout(0, 5));
		setBackground(backgroundColor);
		
		addMouseListener(new MouseAdapter() {

			@Override
			public void mouseEntered(MouseEvent e) {
				
				setBackground(overColor);
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				
				setBackground(backgroundColor);
			}
		});
		
		setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(COLOR_BORDER, 2), BorderFactory.createEmptyBorder(PADDING, PADDING, PADDING, PADDING)));
		
		// CARGAR INTERFAZ
		inicializar();
	}

	public LoadingButton(Icon icono, String texto, Color backgroundColor, Color overColor) {
		
		this(icono, texto, backgroundColor, overColor, "", 5);
	}
	
	public LoadingButton(Icon icono, String texto) {
		
		this(icono, texto, COLOR_NORMAL, COLOR_OVER);
	}

	public LoadingButton(Icon icono) {
		
		this(icono, "", COLOR_NORMAL, COLOR_OVER);
	}

	public LoadingButton(String texto) {
		
		this(null, texto, COLOR_NORMAL, COLOR_OVER);
	}
	
	public String getTexto() {
		
		return texto;
	}
	
	public void setTexto(String texto) {
		
		this.texto = texto;
		inicializar();
	}
	
	public boolean isError() {
		
		return error;
	}
	
	public boolean isLoading() {
		
		return loading;
	}
	
	public void showErrorAsImage(boolean showErrorAsImage) {
		
		this.showErrorAsImage = showErrorAsImage;
	}

	public void showLoadingAsImage(boolean showLoadingAsImage) {
		
		this.showLoadingAsImage = showLoadingAsImage;
	}
	
	public void setMensajeError(String mensajeError) {
		
		this.mensajeError = mensajeError;
	}
	
	public void setTiempoError(int tiempoError) {
		
		this.tiempoError = tiempoError;
	}
	
	private void inicializar() {

		removeAll();
		
		revalidate();
		repaint();

		// CONFIGURACION
		int diferencia = texto.length() - 10;
		diferencia = Math.max(diferencia, 0);
		
		int iconPadding = 0;
		if(icono != null && !texto.equals("")) {
			
			iconPadding += 20;
		}
		
		Dimension d = new Dimension(120 + (diferencia*10), 55 + PADDING + iconPadding);
		setMaximumSize(d);
		setMinimumSize(d);
		setPreferredSize(d);
		
		// ANADIR
		JLabel labelTexto = new JLabel(texto);
		labelTexto.setHorizontalAlignment(SwingConstants.CENTER);
		labelTexto.setForeground(Color.BLACK);
		labelTexto.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		if(icono != null) {
			
			JLabel labelIcono = new JLabel();
			labelIcono.setHorizontalAlignment(SwingConstants.CENTER);
			add(labelIcono, BorderLayout.CENTER);
			
			if(!texto.equals("")) {
				
				add(labelTexto, BorderLayout.SOUTH);
				labelIcono.setIcon(IconUtils.escalar(icono, 35));
				
			} else {

				labelIcono.setIcon(IconUtils.escalar(icono, 40));
			}
			
		} else {

			add(labelTexto, BorderLayout.CENTER);
		}

		revalidate();
		repaint();
	}
	
	// EJECUTAR
	private OnCorrectListener correctListener;
	private OnErrorListener errorListener;

	public void setCorrectListener(OnCorrectListener correctListener) {
		
		this.correctListener = correctListener;
	}
	
	public void setErrorListener(OnErrorListener errorListener) {
		
		this.errorListener = errorListener;
	}

	public void ejecutar(final Ejecucion ejecucion) {

		if(!error && !loading) {

			cargando();
			
			new Thread(new Runnable() {
				
				public void run() {
					
					if(ejecucion.ejecutar()) {

						inicializar();
						if(correctListener != null) {
							
							correctListener.correct();
						}
						
					} else {

						error();
						if(errorListener != null) {
							
							errorListener.error();
						}					
					}
					
					loading = false;
				}

			}).start();
		}
	}
	
	private void cargando() {

		loading = true;
		
		removeAll();
		
		revalidate();
		repaint();

		// ANADIR
		final JLabel label = new JLabel();
		label.setHorizontalAlignment(SwingConstants.CENTER);
		add(label, BorderLayout.CENTER);

		if(showLoadingAsImage) {
			
			label.setIcon(IconUtils.escalar(getClass().getClassLoader().getResource(CARPETA_RECURSOS + "cargando/cargando_circular_50.gif")));
			
		} else {

			label.setText("CARGANDO");
			label.setFont(new Font("Tahoma", Font.BOLD, 14));
			
			new Thread(new Runnable() {

				public void run() {
					
					String[] textos = new String[] {"CARGANDO", "CARGANDO.", "CARGANDO..", "CARGANDO..."};
					int indice = 0;
					
					while(isLoading()) {
					
						label.setText(textos[indice]);
						
						try {

							Thread.sleep(200);
							
						} catch (Exception e) {}
						
						indice = (indice+1) % textos.length;
					}
				}
				
			}).start();
		}
		
		revalidate();
		repaint();
	}
	
	private void error() {

		this.error = true;
		this.loading = false;
		
		removeAll();
		
		revalidate();
		repaint();

		// ANADIR
		JLabel label = new JLabel();
		label.setHorizontalAlignment(SwingConstants.CENTER);
		add(label, BorderLayout.CENTER);

		if(!mensajeError.equals("")) {
			
			label.setToolTipText(mensajeError);
		}
		
		if(showErrorAsImage) {
			
			label.setIcon(IconUtils.escalar(getClass().getClassLoader().getResource(CARPETA_RECURSOS + "error.png"), 50));
			
		} else {

			label.setText("ERROR");
			label.setForeground(Color.RED);
			label.setFont(new Font("Tahoma", Font.BOLD, 16));
		}

		revalidate();
		repaint();
		
		new Thread(new Runnable() {
			
			public void run() {
				
				try {
					Thread.sleep(tiempoError*1000);
				} catch (InterruptedException e) {}
				
				inicializar();
				error = false;
			}
			
		}).start();
	}
	
	// CLASES AUXILIARES
	public interface Ejecucion {
		
		public boolean ejecutar();
	}
	
	public interface OnErrorListener {
	
		public void error();
	}
	
	public interface OnCorrectListener {
		
		public void correct();
	}
}
package com.repex.librerias.components.calendario;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.date.Fecha;

public class PanelSemana extends BorderPanel {

	private static final long serialVersionUID = 1L;
	
	// VARIABLES
	private int size;
	
	// VARIABLES INTERNAS
	private Fecha fecha;
	
	public PanelSemana(Fecha fecha, int size) {
		
		super(0);

		this.fecha = fecha;
		this.size = size;
		
		inicializar();
	}
	
	public Fecha getFecha() {
		
		return fecha;
	}
	
	private void inicializar() {

		// TAMANO
		Dimension d = new Dimension(size+getBorderSize(), size+getBorderSize());
		setPreferredSize(d);
		setMaximumSize(d);
		setMinimumSize(d);
				
		// DIA DEL MES
		setLayout(new GridLayout());
		setBackground(Color.decode("#f2f2f2"));
		
		JLabel label = new JLabel(Integer.toString(fecha.getSemana()));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setVerticalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.BOLD, 13));
		add(label);
	}
}
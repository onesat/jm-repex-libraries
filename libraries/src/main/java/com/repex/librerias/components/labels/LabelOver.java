package com.repex.librerias.components.labels;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.border.Border;

import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.components.utilidades.Constantes;
import com.repex.librerias.utilidades.IconUtils;

public class LabelOver extends BorderPanel {

	private static final long serialVersionUID = 1L;

	// CONSTANTES
	private final static String EXTENSION_OVER = "_over";
	
	// VARIABLES
	private int tamano;
	
	private String original;
	
	private Icon icono;
	private Icon over;
	
	public LabelOver(String icono, int tamano) {

		super(BorderPanel.BORDER_SIZE_DEFAULT);
		
		this.original = icono;

		if(tamano != 0) {
			
			this.tamano = Math.max(tamano, Constantes.MINIMUN_SIZE_DEFAULT);
		}
		
		updateIconos();
		
		inicializar();
	}

	public LabelOver(String icono) {

		this(icono, 0);
	}

	public LabelOver(Icon icono, int tamano) {

		if(tamano != 0) {
			
			this.tamano = Math.max(tamano, Constantes.MINIMUN_SIZE_DEFAULT);
			this.icono = IconUtils.escalar(icono, this.tamano);
			
		} else {

			this.icono = icono;
		}
		
		inicializar();
	}
	
	public LabelOver(Icon icono) {

		this(icono, 0);
	}
	
	public int getTamano() {
		
		return tamano;
	}
	
	public void setTamano(int tamano) {

		this.tamano = Math.max(tamano, Constantes.MINIMUN_SIZE_DEFAULT);
		updateIconos();
		
		label.setIcon(this.icono);

		revalidate();
		repaint();
	}
	
	public void setIcono(String icono) {
		
		this.original = icono;
		updateIconos();
		
		label.setIcon(this.icono);
		
		revalidate();
		repaint();
	}
	
	public void setOverable(boolean isOver) {
		
		if(isOver) {
			
			// ANADIR LISTENER
			addMouseListener(new MouseAdapter() {

				@Override
				public void mouseEntered(MouseEvent e) {

					if(over != null) {
						
						label.setIcon(over);
					}
				}

				@Override
				public void mouseExited(MouseEvent e) {

					label.setIcon(icono);
				}
			});
			
			for(final Runnable accion : acciones) {

				addMouseListener(new MouseAdapter() {
					
					@Override
					public void mouseClicked(MouseEvent e) {
							
						new Thread(accion).start();
					}
				});
			}
			
		} else {
			
			for(MouseListener listener : getMouseListeners()) {
				
				removeMouseListener(listener);
			}
		}
	}
	
	public void setOverIcon(boolean isOver) {
		
		if(isOver && this.over != null) {
			
			label.setIcon(over);
			
		} else {

			label.setIcon(icono);
		}
	}
	
	private List<Runnable> acciones;	
	public void addAccion(final Runnable accion) {

		if(accion != null) {
			
			acciones.add(accion);

			addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {


					new Thread(accion).start();
				}
			});
		}
	}
	
	private void updateIconos() {

		try {

			// ICONO
			this.icono = IconUtils.escalar(getClass().getClassLoader().getResource(original), tamano);
			this.setToolTipText(null);
			
		} catch (Exception e) {
			
			this.icono = IconUtils.escalar(getClass().getClassLoader().getResource(com.repex.librerias.utilidades.Constantes.CARPETA_RECURSOS + "error.png"), tamano);
			this.setToolTipText("Error al cargar imagen");
		}
		
		try {

			// OVER
			String imagen = original.substring(0, original.lastIndexOf("."));
			String extension = original.substring(original.lastIndexOf(".")+1);

			this.over = IconUtils.escalar(getClass().getClassLoader().getResource(imagen + EXTENSION_OVER + "." + extension), tamano);
			
		} catch (Exception e) {
			
			this.over = null;
		}
	}
	
	// INICIALIZAR
	private JLabel label;
	
	private void inicializar() {
		
		this.acciones = new LinkedList<Runnable>();
		
		// CONFIGURACION
		setOpaque(false);
		setLayout(new FlowLayout(FlowLayout.CENTER));

		cambiartamano();
		
		setHasBorder(false);
		
		// LABEL
		label = new JLabel(icono);
		add(label);

		// ACCION
		addMouseListener(new MouseAdapter() {

			@Override
			public void mouseEntered(MouseEvent e) {

				if(over != null) {
					
					label.setIcon(over);
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {

				label.setIcon(icono);
			}
		});
	}
	
	@Override
	protected Border createBorder() {
		
		cambiartamano();
		return super.createBorder();
	}
	
	private void cambiartamano() {

		int borderSize = getBorderSize()+getOutterBorderSize()+getInnerBorderSize();
		Dimension d = new Dimension(tamano+borderSize, tamano+borderSize);
		setMaximumSize(d);
	}
}
package com.repex.librerias.components.calendario;

import java.awt.Color;

import com.repex.librerias.date.Fecha;

public class PanelCalendarioBusqueda extends PanelCalendario {

	private static final long serialVersionUID = 1L;

	public static Color FIN_COLOR_DEFAULT = Color.decode("#904DBF");
	
	// VARIABLES
	private PanelDia inicio;
	private PanelDia fin;
	
	public Fecha getInicio() {
		
		if(inicio == null)
			return null;
		
		return inicio.getFecha();
	}
	
	public Fecha getFin() {

		if(fin == null)
			return null;
		
		return fin.getFecha();
	}
	
	public void setInicio(Fecha fecha) {

		if(inicio != null) {

			inicio.setSeleccionado(false);
		}
		
		// CAMBIAR AL MES DE LA FECHA
		cambiarMes(fecha);

		// BUSCAR PANEL
		for(PanelDia panel : getPaneles()) {

			if(panel.getFecha().comprobar(fecha)) {

				inicio = panel;				
				inicio.setSeleccionado(true);
			}
		}
	}
	
	public void setFin(Fecha fecha) {

		if(fin != null) {

			fin.setSeleccionado(false);
		}
			
		// CAMBIAR AL MES DE LA FECHA
		cambiarMes(fecha);

		// BUSCAR PANEL
		for(PanelDia panel : getPaneles()) {

			if(panel.getFecha().comprobar(fecha)) {

				fin = panel;				
				fin.setSeleccionado(true);
			}
		}
	}
	
	public PanelCalendarioBusqueda(int size, int borderSize, Color borderColor) {
		
		super(size, borderSize, borderColor);
		inicializar();
	}

	public PanelCalendarioBusqueda(int size, int borderSize) {
		
		super(size, borderSize);
		inicializar();
	}

	public PanelCalendarioBusqueda(int size) {
		
		super(size);
		inicializar();
	}
	
	private void inicializar() {

		addAccion(new PanelDia.OnDaySelected() {
			
			public void selected(PanelDia panel, Fecha fecha) {

				if(inicio == null) {
					
					inicio = panel;
					panel.setSelectionColor(PanelDia.SELECTION_COLOR_DEFAULT);
					panel.setSeleccionado(true);					

				} else if (fin == null && fecha.despues(inicio.getFecha())) {

					fin = panel;
					panel.setSelectionColor(FIN_COLOR_DEFAULT);
					panel.setSeleccionado(true);
					
				} else {

					inicio.setSeleccionado(false);
					
					inicio = panel;
					panel.setSeleccionado(true);

					if(fin != null) {
						
						fin.setSeleccionado(false);
						fin = null;
					}
				}
			}
		});

		addCambioMes(new PanelCalendario.OnMonthChanged() {
			
			public void change(Fecha firstDay) {

				for(PanelDia panel : getPaneles()) {

					if(inicio != null && panel.getFecha().comprobar(inicio.getFecha())) {

						inicio = panel;
						
						panel.setSelectionColor(PanelDia.SELECTION_COLOR_DEFAULT);
						panel.setSeleccionado(true);
					}

					if(fin != null && panel.getFecha().comprobar(fin.getFecha())) {

						fin = panel;
						
						panel.setSelectionColor(FIN_COLOR_DEFAULT);
						panel.setSeleccionado(true);
					}
				}
			}
		});
	}
	
	@Override
	public void reiniciar() {
		
		inicio = null;
		fin = null;
		
		super.reiniciar();
	}
}
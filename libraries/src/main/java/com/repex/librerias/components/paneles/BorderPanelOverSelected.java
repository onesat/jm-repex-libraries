package com.repex.librerias.components.paneles;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.SwingUtilities;

public class BorderPanelOverSelected extends BorderPanel {

	private static final long serialVersionUID = 1L;
	
	public static Color BACKGROUND_COLOR_DEFAULT = Color.WHITE;
	public static Color OVER_COLOR_DEFAULT = Color.decode("#B4ECFF");	
	public static Color SELECTION_COLOR_DEFAULT = Color.decode("#8495CF");
	
	// VARIABLES
	private Color backgroundColor;
	private Color overColor;
	private Color selectionColor;
	
	// VARIABLES INTERNAS	
	private boolean seleccionado = false;

	private boolean selectable = true;
	private boolean overable = true;

	public BorderPanelOverSelected(int borderSize, Color borderColor, int outterBorderSize, int innerBorderSize, boolean outterEmptyBorder, boolean innerEmptyBorder) {

		super(borderSize, borderColor, outterBorderSize, innerBorderSize, outterEmptyBorder, innerEmptyBorder);

		this.backgroundColor = BACKGROUND_COLOR_DEFAULT;
		this.overColor = OVER_COLOR_DEFAULT;
		this.selectionColor = SELECTION_COLOR_DEFAULT;
		
		inicializar();
	}
	
	public BorderPanelOverSelected(int borderSize, Color borderColor, int emptyBorderSize, boolean outterEmptyBorder, boolean innerEmptyBorder) {
		
		this(borderSize, borderColor, emptyBorderSize, emptyBorderSize, outterEmptyBorder, innerEmptyBorder);
	}

	public BorderPanelOverSelected(int borderSize, Color borderColor) {
		
		this(borderSize, borderColor, 0, false, false);
	}

	public BorderPanelOverSelected(int borderSize) {
		
		this(borderSize, BorderPanel.BORDER_COLOR_DEFAULT);
	}
	
	// CONSTRUCTORES
	public BorderPanelOverSelected(Color backgroundColor, Color overColor, Color selectionColor) {
		
		super(BorderPanel.BORDER_SIZE_DEFAULT);
		
		this.backgroundColor = backgroundColor;
		this.overColor = overColor;
		this.selectionColor = selectionColor;
		
		inicializar();
	}

	public BorderPanelOverSelected() {
		
		this(BACKGROUND_COLOR_DEFAULT, OVER_COLOR_DEFAULT, SELECTION_COLOR_DEFAULT);
	}	
	
	// SELECCIONADO
	public boolean isSeleccionado() {
		
		return seleccionado;
	}
	
	public void setSeleccionado(boolean seleccionado) {
		
		this.seleccionado = seleccionado;
		
		if(!seleccionado) {
			
			setBackground(backgroundColor);
			
		} else {

			setBackground(selectionColor);
		}
	}
	
	public boolean isSelectable() {
		
		return selectable;
	}
	
	public void setSelectable(boolean selectable) {
		
		boolean antiguo = this.selectable;
		this.selectable = selectable;
		
		if(antiguo != this.selectable) {
			
			if(!selectable && isSeleccionado()) {

				setSeleccionado(false);
			}

			if(!isSelectable()) {

				removeMouseListener(selectionListener);

			} else {

				addMouseListener(selectionListener);
			}
		}
	}
	
	public boolean isOverable() {
		
		return overable;
	}
	
	public void setOverable(boolean overable) {
		
		this.overable = overable;
		
		if(!isOverable()) {
			
			removeMouseListener(overListener);
			
		} else {
			
			addMouseListener(overListener);
		}
	}
	
	public void setSelectableOverable(boolean valor) {
		
		setSelectable(valor);
		setOverable(valor);
	}
	
	// MODIFICAR INTERFAZ
	public Color getBackgroundColor() {
		
		return backgroundColor;
	}
	
	public void setBackgroundColor(Color backgroundColor) {
		
		this.backgroundColor = backgroundColor;
		setBackground(backgroundColor);
	}
	
	public Color getOverColor() {
		
		return overColor;
	}
	
	public void setOverColor(Color overColor) {
		
		this.overColor = overColor;
	}
	
	public Color getSelectionColor() {
		
		return selectionColor;
	}
	
	public void setSelectionColor(Color selectionColor) {
		
		this.selectionColor = selectionColor;
	}
	
	private void inicializar() {
		
		this.onSelectedListeners = new LinkedList<OnSelectedListener>();
		
		this.overActionListener = new LinkedList<OverAction>();
		this.endOverActionListener = new LinkedList<EndOverAction>();
		
		// CONFIGURACION
		setBackground(backgroundColor);
		
		// OVER
		addMouseListener(overListener);
		
		// SELECCION
		addMouseListener(selectionListener);
	}
	
	// LISTENER POR DEFECTO
	private MouseListener overListener = new MouseAdapter() {
		
		@Override
		public void mouseEntered(MouseEvent e) {
			
			if(isOverable()) {
				
				setBackground(overColor);

				for(OverAction listener : overActionListener) {
					
					listener.over();
				}
			}
		}
		
		@Override
		public void mouseExited(MouseEvent e) {

			if(isOverable()) {
				
				if(seleccionado) {

					setBackground(selectionColor);

				} else {

					setBackground(backgroundColor);
				}

				for(EndOverAction listener : endOverActionListener) {
					
					listener.endOver();
				}
			}
		}
	};
	
	private MouseListener selectionListener = new MouseAdapter() {
		
		@Override
		public void mouseClicked(MouseEvent e) {

			if(SwingUtilities.isLeftMouseButton(e) && isSelectable()) {
				
				setSeleccionado(!seleccionado);
				
				for(OnSelectedListener listener : onSelectedListeners) {
					
					listener.onSelected(seleccionado);
				}
			}
		}
	};
	
	protected void launch() {
		for(OnSelectedListener listener : onSelectedListeners) {			
			listener.onSelected(seleccionado);
		}
	}
	
	// LISTENER SELECCION
	private List<OnSelectedListener> onSelectedListeners;
	
	public interface OnSelectedListener {
		
		public void onSelected(boolean selected);
	}
	
	public void addOnSelectedListener(OnSelectedListener listener) {
	
		this.onSelectedListeners.add(listener);
	}
	
	// OVER ACTION
	private List<OverAction> overActionListener;
	private List<EndOverAction> endOverActionListener;
	
	public void addOverActionListener(OverAction listener) {
		
		overActionListener.add(listener);
	}
	
	public void addEndOverActionListener(EndOverAction listener) {
		
		endOverActionListener.add(listener);
	}

	// LISTENER
	public interface OverAction {
		
		public void over();
	}

	public interface EndOverAction {
		
		public void endOver();
	}
}
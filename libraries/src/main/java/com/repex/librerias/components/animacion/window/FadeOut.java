package com.repex.librerias.components.animacion.window;

import java.awt.Window;

public class FadeOut extends WindowAnimation {
		
	public FadeOut(Window window, double second) {
		
		super(window, second);
		
		this.addOnAnimationListener(new OnAnimationListener() {

			public void onStart() {

				getWindow().setOpacity(1);
			}

			public void onEnd() {}
		});
	}
	
	public FadeOut(Window window) {
		
		this(window, 0.5);
	}
	
	@Override
	protected void animacion(float factor) {
		
		getWindow().setOpacity(Math.max(1-factor, 0));
	}
}

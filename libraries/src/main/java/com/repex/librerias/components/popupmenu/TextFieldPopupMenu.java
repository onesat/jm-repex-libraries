package com.repex.librerias.components.popupmenu;

import java.util.LinkedList;
import java.util.List;

import javax.swing.text.JTextComponent;

public class TextFieldPopupMenu extends CopyPastePopupMenu {

	private static final long serialVersionUID = 1L;
	
	public TextFieldPopupMenu(final JTextComponent parent) {
	
		super(parent);
		
		// OPCIONES
		List<Opcion> opciones = new LinkedList<Opcion>();
		
		// SELECCIONAR
		opciones.add(new MyPopupMenu.Opcion("Seleccionar todo", new Runnable() {
			
			public void run() {
				
				parent.select(0, parent.getText().length());
			}
		}));
		
		// SEPARATOR Y BORRAR
		opciones.add(new MyPopupMenu.OpcionSeparator());
		opciones.add(new MyPopupMenu.Opcion("Borrar todo", new Runnable() {
			
			public void run() {
				
				parent.setText("");
			}
		}));

		// BORRAR SELECCION
		opciones.add(new MyPopupMenu.Opcion("Borrar", new Runnable() {
			
			public void run() {
				
				borrar(parent);
			}
		}));
		
		this.addOpciones(opciones);
	}
	
	private void borrar(JTextComponent parent) {
		
		if (parent.getSelectedText() != null && parent.getSelectedText().length() > 0) {
			
			int start = parent.getSelectionStart();
			int end = parent.getSelectionEnd();
			
			parent.setText(parent.getText().substring(0, start) + parent.getText().substring(end, parent.getText().length()));
			parent.setCaretPosition(start);
		}
	}
}
package com.repex.librerias.components.textfield;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import com.repex.librerias.components.popupmenu.MyPopupMenu;
import com.repex.librerias.components.popupmenu.TextFieldPopupMenu;

public class HintTextField extends JTextField implements FocusListener {
	
	private static final long serialVersionUID = 1L;
	
	// CONSTANTES
	public final static String DEFAULT_HINT = "DEFAULT";
	
	public final static int MARGIN_DEFAULT = 10;
	
	public final static Color BORDER_COLOR_DEFAULT = Color.BLACK;
	public final static Color HINT_BORDER_COLOR_DEFAULT = Color.decode("#037a75");
	
	public final static int BORDER_SIZE_DEFAULT = 2;
	public final static int HINT_BORDER_SIZE_DEFAULT = 2;
	
	public final static Color HINT_COLOR_DEFAULT = Color.decode("#777777");
	public final static Color NORMAL_COLOR_DEFAULT = Color.BLACK;
	
	// VARIABLES
	private String hint;
	private boolean showingHint;

	private Color hintColor = Color.BLACK;
	private Color normalColor = Color.BLACK;
	
	private int borderSize = 2;
	private int hintBorderSize = 3;
	private int marginBorder = 10;
	
	private Color hintBorderColor = Color.BLUE;
	private Color borderColor = Color.BLACK;

	// VARIABLES INTERNAS
	private boolean hasBorder = true;
	
	private boolean hasPopupMenu = false;	
	private MyPopupMenu popupMenu;
	
	public HintTextField(String hint, int margin, int borderSize, int hintBorderSize, Color borderColor, Color hintBorderColor, Color hintColor, Color normalColor) {

		super((hint.equals("")) ? DEFAULT_HINT : hint);

		this.hint = hint;
		this.showingHint = true;

		if(hint.equals("")) {
			
			this.hint = DEFAULT_HINT;
		}
		
		this.hintColor = hintColor;
		this.normalColor = normalColor;		
		
		this.borderSize = borderSize;
		this.hintBorderSize = hintBorderSize;
		this.marginBorder = margin;
		
		this.hintBorderColor = hintBorderColor;
		this.borderColor = borderColor;

		popupMenu = new TextFieldPopupMenu(this);
		
		setForeground(hintColor);
		setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(borderSize, borderSize, borderSize, borderSize, this.borderColor), BorderFactory.createEmptyBorder(marginBorder, marginBorder, marginBorder, marginBorder)));

		super.addFocusListener(this);
		super.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				
				if(SwingUtilities.isRightMouseButton(e) && hasPopupMenu) {
					
					// MOSTAR MENU POPUP					
					popupMenu.crear();
				}
				
				super.mouseReleased(e);
			}
		});
	}

	public HintTextField(String hint, int margin, int borderSize, int hintBorderSize, Color borderColor, Color hintColor, Color normalColor) {

		this(hint, margin, borderSize, hintBorderSize, borderColor, HINT_BORDER_COLOR_DEFAULT, hintColor, normalColor);
	}

	public HintTextField(String hint, int margin, int borderSize, int hintBorderSize, Color hintColor, Color normalColor) {

		this(hint, margin, borderSize, hintBorderSize, BORDER_COLOR_DEFAULT, HintTextField.HINT_BORDER_COLOR_DEFAULT, hintColor, normalColor);
	}
	
	public HintTextField(String hint, int margin, Color borderColor, Color hintColor, Color normalColor) {

		this(hint, margin, BORDER_SIZE_DEFAULT, HINT_BORDER_SIZE_DEFAULT, borderColor, HINT_BORDER_COLOR_DEFAULT, hintColor, normalColor);
	}
	
	public HintTextField(String hint, int margin, Color hintColor, Color normalColor) {

		this(hint, margin, BORDER_COLOR_DEFAULT, hintColor, normalColor);
	}

	public HintTextField(String hint, Color hintColor, Color normalColor) {

		this(hint, MARGIN_DEFAULT, hintColor, normalColor);
	}

	public HintTextField(String hint, int margin, Color borderColor) {

		this(hint, margin, borderColor, HINT_COLOR_DEFAULT, NORMAL_COLOR_DEFAULT);
	}
	
	public HintTextField(String hint, int margin) {

		this(hint, margin, BORDER_COLOR_DEFAULT);
	}
	
	public HintTextField(String hint) {

		this(hint, MARGIN_DEFAULT);
	}

	public HintTextField() {

		this("");
	}
	
	// BASICO
	public String getHint() {
		
		return hint;
	}
	
	public void setHint(String hint) {
		
		this.hint = hint;
		if(!hasFocus() && isShowingHint()) {
			
			super.setText(hint);
		}
	}
	
	public boolean isShowingHint() {
		
		return showingHint;
	}
	
	// METODOS
	public Color getHintColor() {
	
		return hintColor;
	}
	
	public void setHintColor(Color hintColor) {
		
		this.hintColor = hintColor;
	}
	
	public Color getNormalColor() {
		
		return normalColor;
	}
	
	public void setNormalColor(Color normalColor) {
		
		this.normalColor = normalColor;
	}
	
	public void setBorderColor(Color borderColor) {
		
		this.borderColor = borderColor;
		createBorder(this.borderSize, this.borderColor);
	}

	// BORDER
	public boolean hasBorder() {
		
		return hasBorder;
	}
	
	public void setHasBorder(boolean hasBorder) {
		
		this.hasBorder = hasBorder;
		
		if(!hasBorder) {
			
			super.setBorder(null);
		}
	}
	
	@Override
	public void setBorder(Border border) {
		
		// TODO: ARREGLAR, YA QUE NO TIENE EN CUENTA EL BORDER.
		// ANADIR PARA BORDER CON SOLO UN LADO
		setHasBorder(border != null);
		
		if(hasFocus()) {
		
			createBorder(hintBorderSize, hintBorderColor);
			
		} else {

			createBorder(borderSize, borderColor);
		}
	}
	
	private void createBorder(int borderSize, Color color) {

		if(hasBorder) {
		
			super.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(borderSize, borderSize, borderSize, borderSize, color), BorderFactory.createEmptyBorder(marginBorder, marginBorder, marginBorder, marginBorder)));
		
		} else {
			
			super.setBorder(BorderFactory.createEmptyBorder(marginBorder, marginBorder, marginBorder, marginBorder));
		}
	}
	
	// POPUP MENU
	public boolean hasPopupMenu() {
		
		return hasPopupMenu;
	}
	
	public void setHasPopupMenu(boolean hasPopupMenu) {
		
		this.hasPopupMenu = hasPopupMenu;
	}
	
	public void setPopupMenu(MyPopupMenu popupMenu) {
		
		this.popupMenu = popupMenu;
	}
	
	@Override
	public void setText(String t) {
		
		if(t.equals("") && !hasFocus()) {

			t = hint;
		}
		
		this.showingHint = t.equals(hint);
		super.setText(t);
		
		if(isShowingHint()) {

			setForeground(hintColor);
			
		} else {

			setForeground(normalColor);
		}
	}
	
	public void focusGained(FocusEvent e) {

		if(this.getText().isEmpty()) {

			super.setText("");
			showingHint = false;

			setForeground(normalColor);
		}
		
		createBorder(this.hintBorderSize, this.hintBorderColor);
	}

	public void focusLost(FocusEvent e) {

		if(this.getText().isEmpty()) {

			super.setText(hint);
			showingHint = true;

			setForeground(hintColor);
		}

		createBorder(this.borderSize, this.borderColor);
	}

	@Override
	public String getText() {
		
		return showingHint ? "" : super.getText();
	}
	
	@Override
	public void setEditable(boolean b) {
		
		Color color = getBackground();
		super.setEditable(b);
		setBackground(color);
	}
}
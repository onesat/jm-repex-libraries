package com.repex.librerias.components.utilidades;

import java.awt.Color;

public class Colores {

	public static Color COLOR_ACTIVADO = Color.decode("#91c98d");
	public static Color COLOR_DESACTIVADO = Color.decode("#8c5d5d");

	public static Color COLOR_PANEL = Color.WHITE;
	public static Color COLOR_OVER_PANEL = Color.decode("#AAAABB");
	
	public static Color COLOR_SELECCIONADO = Color.decode("#AAFFAA");

	public static Color COLOR_NUEVO = Color.decode("#88CCAA");
	public static Color COLOR_NUEVO_OVER = Color.decode("#558888");
	
	public static String transform(Color color) {
		
		if(color == null)
			return null;
		
		return "#" + Integer.toHexString(color.getRGB()).substring(2).toUpperCase();
	}
}

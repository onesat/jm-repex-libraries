package com.repex.librerias.components.animacion.component;

import java.awt.Component;
import java.awt.IllegalComponentStateException;
import java.awt.Window;

import com.repex.librerias.components.animacion.MyAnimation;

public abstract class ComponentAnimation extends MyAnimation {
	
	private Component component;
	
	public ComponentAnimation(Component component, double second) throws IllegalComponentStateException {

		super(second);
		
		if(!component.isOpaque()) {
			
			throw new IllegalComponentStateException("Component not cant be opaque");
		}
		
		this.component = component;
	}
	
	public ComponentAnimation(Window window) {
		
		this(window, 0.5);
	}

	public Component getComponent() {
		
		return component;
	}
}
package com.repex.librerias.components.paneles.lista;

import java.awt.Dimension;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;

import com.repex.librerias.components.labels.LabelPanel;
import com.repex.librerias.components.utilidades.OnResizePanel;

public class PanelListaCarga extends PanelLista {
	
	private static final long serialVersionUID = 1L;

	public final static int LIMITE_CARGA = 15;
	
	private int carga;
	
	private List<JComponent> datos = new LinkedList<JComponent>();
	
	public PanelListaCarga(int carga) {
		
		super();
		
		this.carga = carga;
		this.limite = carga;
	}

	public PanelListaCarga() {
		this(LIMITE_CARGA);
	}
	
	public PanelListaCarga(int spacing, int limite, Orientacion orientacion, int carga) {
		
		super(spacing, limite, orientacion);
		
		this.carga = carga;
		this.limite = carga;
	}

	public PanelListaCarga(int spacing, int limite, int carga) {
		
		super(spacing, limite);
		
		this.carga = carga;
		this.limite = carga;
	}

	public PanelListaCarga(int spacing, Orientacion orientacion, int carga) {
		
		super(spacing, orientacion);
		
		this.carga = carga;
		this.limite = carga;
	}

	public PanelListaCarga(int spacing, int carga) {
		
		super(spacing);
		
		this.carga = carga;
		this.limite = carga;
	}
	
	@Override
	public void addDato(JComponent panel) {
		
		this.addDato(panel, false);
	}
	
	public void addDato(JComponent panel, boolean anadir) {

		this.datos.add(panel);
		
		if(anadir || indice < limite) {
			indice = indice + 1;
			super.addDato(panel);
		}

		if(datos.size() > indice && !cargaActiva) {						
			super.addDato(crearPanelCarga());
		}
	}

	@Override
	public void removeDato(JPanel panel) {
		
		super.removeDato(panel);
		
		int indice = this.datos.indexOf(panel);
		this.datos.remove(panel);
		
		if(indice < this.indice) {
			this.indice--;
		}
	}
	
	@Override
	public void reset() {

		this.indice = 0;
		this.limite = carga;
		
		datos.clear();
		super.reset();
	}
	
	private PanelCarga panelCarga;
	
	private boolean cargaActiva = false;
	
	private int indice = 0;
	private int limite = 0;
	
	private void carga() {
	
		if(panelCarga != null) {
			removeDato(panelCarga);
			cargaActiva = false;
		}

		limite = indice + carga;
		
		for(int i=indice; i<limite && i<datos.size(); i++) {
			super.addDato(datos.get(i));
		}
		
		indice += carga;
		indice = Math.min(indice, datos.size());
		
		if(indice < datos.size()) {						
			super.addDato(crearPanelCarga());
		}
	}
	
	private PanelCarga crearPanelCarga() {
		
		if(panelCarga == null) {
			
			panelCarga = new PanelCarga();
			
			panelCarga.addAccion(new Runnable() {
				
				public void run() {
					carga();
				}
			});
		}

		cargaActiva = true;
		
		return panelCarga;
	}
	
	private class PanelCarga extends LabelPanel implements OnResizePanel {
		
		private static final long serialVersionUID = 1L;
		
		private final static int TAMANO = 50;
		
		public PanelCarga() {
			super("com/repex/librerias/botones/cargar.png", TAMANO - 10);
		}

		public void resize() {
			Dimension d = new Dimension(2000, 50);
			setMaximumSize(d);
		}
	}
}
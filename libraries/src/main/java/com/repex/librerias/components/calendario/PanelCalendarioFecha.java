package com.repex.librerias.components.calendario;

import java.awt.Color;

import com.repex.librerias.date.Fecha;

public class PanelCalendarioFecha extends PanelCalendario {

	private static final long serialVersionUID = 1L;
	
	// VARIABLES
	private PanelDia inicio;	
	private boolean onlyNext = false;
	
	public Fecha getFecha() {

		if(inicio == null)
			return null;
		
		return inicio.getFecha();
	}
	
	public void setFecha(Fecha fecha) {

		if(inicio != null) {

			inicio.setSeleccionado(false);
		}
		
		Fecha hoy = new Fecha();
		
		if(!onlyNext || fecha.comprobar(hoy) || fecha.despues(hoy)) {
			
			// CAMBIAR AL MES DE LA FECHA
			cambiarMes(fecha);

			// BUSCAR PANEL
			for(PanelDia panel : getPaneles()) {

				if(panel.getFecha().comprobar(fecha)) {

					inicio = panel;				
					inicio.setSeleccionado(true);
				}
			}
		}
	}
	
	public boolean isOnlyNext() {
		
		return onlyNext;
	}
	
	public void setOnlyNext(boolean onlyNext) {
		
		this.onlyNext = onlyNext;
	}
	
	public PanelCalendarioFecha(int size, int borderSize, Color borderColor) {
		
		super(size, borderSize, borderColor);
		inicializar();
	}

	public PanelCalendarioFecha(int size, int borderSize) {
		
		super(size, borderSize);
		inicializar();
	}

	public PanelCalendarioFecha(int size) {
		
		super(size);
		inicializar();
	}
	
	private void inicializar() {

		addAccion(new PanelDia.OnDaySelected() {
			
			public void selected(PanelDia panel, Fecha fecha) {

				Fecha hoy = new Fecha();
				
				if(!onlyNext || fecha.comprobar(hoy) || fecha.despues(hoy)) {
					
					if(inicio != null) {

						inicio.setSeleccionado(false);
					}

					inicio = panel;
					inicio.setSeleccionado(true);
					
				} else {

					panel.setSeleccionado(false);
				}
			}
		});

		addCambioMes(new PanelCalendario.OnMonthChanged() {
			
			public void change(Fecha firstDay) {

				for(PanelDia panel : getPaneles()) {

					if(inicio != null && panel.getFecha().comprobar(inicio.getFecha())) {

						inicio = panel;
						
						panel.setSeleccionado(true);
					}
				}
			}
		});
	}
	
	@Override
	public void reiniciar() {
		
		inicio = null;		
		super.reiniciar();
	}
}
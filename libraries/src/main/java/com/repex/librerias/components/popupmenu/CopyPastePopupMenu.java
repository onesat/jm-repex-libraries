package com.repex.librerias.components.popupmenu;

import java.awt.datatransfer.DataFlavor;
import java.util.LinkedList;
import java.util.List;

import javax.swing.text.JTextComponent;

import com.repex.librerias.utilidades.ClipboardController;

public class CopyPastePopupMenu extends MyPopupMenu {
	
	private static final long serialVersionUID = 1L;

	public CopyPastePopupMenu(JTextComponent parent, List<Opcion> opciones) {

		super(parent, opciones);		
		opciones.addAll(0, crearOpciones(parent));
	}

	protected List<Opcion> crearOpciones(final JTextComponent parent) {
		
		List<Opcion> principales = new LinkedList<Opcion>();
		
		// CORTAR
		principales.add(new MyPopupMenu.Opcion("Cortar", new Runnable() {
			
			public void run() {

				int pos = parent.getSelectionStart();
				
				parent.cut();
				parent.select(0, 0);
				parent.setCaretPosition(pos);
			}
		}));
		
		// COPIAR
		principales.add(new MyPopupMenu.Opcion("Copiar", new Runnable() {

			public void run() {
				
				parent.copy();				
			}
		}));
		
		// PEGAR
		MyPopupMenu.Opcion pegar = new MyPopupMenu.Opcion("Pegar", new Runnable() {

			public void run() {

				try {
					
					int pos = Math.min(parent.getCaretPosition(), parent.getText().length());
					
					borrar(parent);
					
					String data = (String) ClipboardController.getSystemClipboard().getData(DataFlavor.stringFlavor);
					parent.setText(parent.getText().substring(0, pos)+data+parent.getText().substring(pos, parent.getText().length()));
					
					parent.setCaretPosition(pos+data.length());
					
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		});
		
		pegar.addCondicion(new Condicion() {

			public boolean comprobar() {
				
				try {

					return !((String) ClipboardController.getSystemClipboard().getData(DataFlavor.stringFlavor)).equals("");
					
				} catch (Exception e) {}
				
				return false;
			}
		});
		
		principales.add(pegar);
		
		principales.add(new MyPopupMenu.OpcionSeparator());
		
		return principales;
	}
	
	public CopyPastePopupMenu(JTextComponent parent) {

		this(parent, new LinkedList<Opcion>());
	}
	
	private void borrar(JTextComponent parent) {
		
		if (parent.getSelectedText() != null && parent.getSelectedText().length() > 0) {
			
			int start = parent.getSelectionStart();
			int end = parent.getSelectionEnd();
			
			parent.setText(parent.getText().substring(0, start) + parent.getText().substring(end, parent.getText().length()));
			parent.setCaretPosition(start);
		}
	}
}
package com.repex.librerias.components.labels;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import com.repex.librerias.components.popupmenu.LabelPopupMenu;
import com.repex.librerias.components.popupmenu.MyPopupMenu;

public class MyJLabel extends JLabel {
	
	private static final long serialVersionUID = 1L;

	private MyPopupMenu popupMenu;
	
	public MyJLabel() {
	
		super();
		inicializar();
	}
	
	public MyJLabel(String texto) {
	
		super(texto);
		inicializar();
	}
	
	public MyJLabel(Icon icon) {
		
		super(icon);
		inicializar();
	}
	
	public MyJLabel(String texto, int posicion) {
		
		super(texto, posicion);
		inicializar();
	}

	public MyJLabel(Icon icon, int posicion) {
		
		super(icon, posicion);
		inicializar();
	}
	
	public void setPopupMenu(MyPopupMenu popupMenu) {
		
		this.popupMenu = popupMenu;
	}
	
	private void inicializar() {
		
		setOpaque(false);
		
		popupMenu = new LabelPopupMenu(this);
		
		addMouseListener(new MouseAdapter() {
						
			@Override
			public void mouseReleased(MouseEvent e) {
				
				if(SwingUtilities.isRightMouseButton(e)) {
					
					// MOSTAR MENU POPUP					
					popupMenu.crear();
				}
				
				super.mouseReleased(e);
			}
		});
	}
}
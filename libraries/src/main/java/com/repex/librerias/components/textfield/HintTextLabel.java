package com.repex.librerias.components.textfield;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;

import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.text.JTextComponent;

import com.repex.librerias.components.popupmenu.MyPopupMenu;
import com.repex.librerias.components.textfield.HintTextField;

public class HintTextLabel extends HintTextField {

	private static final long serialVersionUID = 1L;

	private int textPosition = SwingConstants.CENTER;
	
	public HintTextLabel() {
		super();
		inicializar();
	}

	public HintTextLabel(String hint, Color hintColor, Color normalColor) {
		super(hint, hintColor, normalColor);
		inicializar();
	}

	public HintTextLabel(String hint, int margin, Color borderColor, Color hintColor, Color normalColor) {
		super(hint, margin, borderColor, hintColor, normalColor);
		inicializar();
	}

	public HintTextLabel(String hint, int margin, Color hintColor, Color normalColor) {
		super(hint, margin, hintColor, normalColor);
		inicializar();
	}

	public HintTextLabel(String hint, int margin, Color borderColor) {
		super(hint, margin, borderColor);
		inicializar();
	}

	public HintTextLabel(String hint, int margin, int borderSize, int hintBorderSize, Color borderColor, Color hintBorderColor, Color hintColor, Color normalColor) {
		super(hint, margin, borderSize, hintBorderSize, borderColor, hintBorderColor, hintColor, normalColor);
		inicializar();
	}

	public HintTextLabel(String hint, int margin, int borderSize, int hintBorderSize, Color borderColor, Color hintColor, Color normalColor) {
		super(hint, margin, borderSize, hintBorderSize, borderColor, hintColor, normalColor);
		inicializar();
	}

	public HintTextLabel(String hint, int margin, int borderSize, int hintBorderSize, Color hintColor, Color normalColor) {
		super(hint, margin, borderSize, hintBorderSize, hintColor, normalColor);
		inicializar();
	}

	public HintTextLabel(String hint, int textPosition, int margin) {

		super(hint, margin);		
		this.textPosition = textPosition;
		
		inicializar();
	}

	public HintTextLabel(String hint, int textPosition) {
		
		super(hint);		
		this.textPosition = textPosition;
		
		inicializar();
	}

	public HintTextLabel(String hint) {
		super(hint);
		inicializar();
	}

	private Border defaultBorder;
	
	private void inicializar() {
		
		setHorizontalAlignment(textPosition);
	
		defaultBorder = getBorder();
		
		setEditable(false);
		
		setPopupMenu(new TextLabelPopupMenu(this));
		setHasPopupMenu(true);
	}
	
	@Override
	public void setBorder(Border border) {
		super.setBorder(border);
		defaultBorder = getBorder();
	}
	
	@Override
	public void setEditable(boolean editable) {
		
		setOpaque(editable);

		super.setBorder(editable ? defaultBorder : null);
		super.setEditable(editable);
	}
	
	private class TextLabelPopupMenu extends MyPopupMenu {
		
		private static final long serialVersionUID = 1L;

		private TextLabelPopupMenu(final JTextComponent parent) {
			
			super(parent);
			
			// OPCIONES
			List<Opcion> opciones = new LinkedList<Opcion>();
			
			// SELECCIONAR
			opciones.add(new MyPopupMenu.Opcion("Seleccionar todo", new Runnable() {
				
				public void run() {
					
					parent.select(0, parent.getText().length());
				}
			}));
			
			// COPIAR
			opciones.add(new MyPopupMenu.Opcion("Copiar", new Runnable() {
				
				public void run() {
					
					parent.copy();
				}
			}));

			// BORRAR SELECCION
			
			this.addOpciones(opciones);
		}
	}	
}
package com.repex.librerias.components.dialog;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.OverlayLayout;
import javax.swing.text.JTextComponent;

import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.components.paneles.BorderPanelOverSelected;
import com.repex.librerias.components.paneles.carga.PanelCargandoCentral;
import com.repex.librerias.components.paneles.lista.PanelLista;
import com.repex.librerias.components.screen.Screen;
import com.repex.librerias.components.utilidades.Colores;

public abstract class LostFocusLoadingDialog extends LostFocusDialog {
	
	private static final long serialVersionUID = 1L;
	
	public LostFocusLoadingDialog(Screen parent, JTextComponent padre, Color background, int spacing, int emptyBorder) {
		
		super(parent, padre);
		
		setFocusableWindowState(false);

		if(padre != null) {
			padre.requestFocus();
		}
		
		contenedor.setBackground(background);
		
		contenedor.setEmptyBorderSize(emptyBorder);
		lista.setSpacing(spacing);
	}
	
	public LostFocusLoadingDialog(Screen parent, JTextComponent padre, Color background) {
		
		this(parent, padre, background, 5, 5);		
	}
	
	public LostFocusLoadingDialog(Screen parent, JTextComponent padre, int spacing, int emptyBorder) {
		
		this(parent, padre, Color.decode("#EEFFFF"), spacing, emptyBorder);		
	}
	
	public LostFocusLoadingDialog(Screen parent, JTextComponent padre) {
		
		this(parent, padre, Color.decode("#AAAAAA"));		
	}
	
	private BorderPanel contenedor;
	
	private int indice;
	
	private PanelLista lista;
	private PanelCargandoCentral cargando;
	
	protected PanelCargandoCentral getCargando() {
		return cargando;
	}
	
	protected void reset() {
		
		lista.reset();		
		indice = -1;
	}
	
	public void clear() {
		
		indice = -1;
		
		for(int i=0; i<lista.getCount(); i++) {
			
			Component component = lista.getChild(i);
			if(component instanceof BorderPanelOverSelected) {
				
				((BorderPanelOverSelected) component).setBackgroundColor(Colores.COLOR_PANEL);
			}
		}
	}
	
	protected void add(JPanel panel) {
		
		lista.addDato(panel);
	}
	
	// AJUSTA EL TAMANO AL CONTENIDO
	protected void ajustar() {

		if(lista.getCount() > 0) {
			
			int width = getPadre().getWidth();
			int heigth = Math.min(250, lista.getCount() * lista.getChild(0).getHeight());

			Dimension dimension = new Dimension(width, heigth);
			setMaximumSize(dimension);
			setMinimumSize(dimension);
			setPreferredSize(dimension);
		}
	}
	
	protected BorderPanelOverSelected get() {
				
		try {

			if(indice >= 0 && indice < lista.getCount()) {

				return (BorderPanelOverSelected) lista.getChild(indice);
			}

		} catch (Exception e) {}
		
		return null;
	}
	
	@Override
	public JPanel construir() {
		
		contenedor = new BorderPanel(2, Color.BLACK);
		contenedor.setBackground(Color.LIGHT_GRAY);
		contenedor.setLayout(new OverlayLayout(contenedor));
				
		contenedor.add(lista = new PanelLista());
		
		cargando = new PanelCargandoCentral();
		cargando.setOpaque(false);		
		contenedor.add(cargando);

		return contenedor;
	}

	public void next() {

		// QUITAR ACTUAL
		BorderPanelOverSelected panelAnterior = get();
		if(panelAnterior != null) {
			panelAnterior.setBackgroundColor(Colores.COLOR_PANEL);
		}
		
		// PASAR
		if(lista.getCount() > 0) {
			
			indice = (indice+1) % lista.getCount();

			BorderPanelOverSelected panel = get();
			if(panel != null) {
				panel.setBackgroundColor(Colores.COLOR_SELECCIONADO);
			}
		}
		
		lista.moveTo(indice);
	}
	
	public void previous() {

		// QUITAR ACTUAL
		BorderPanelOverSelected panelAnterior = get();
		if(panelAnterior != null) {
			panelAnterior.setBackgroundColor(Colores.COLOR_PANEL);
		}
		
		// PASAR
		if(lista.getCount() > 0) {
			
			indice = (indice-1) % lista.getCount();
			if(indice < 0) {
				indice = lista.getCount() - 1;
			}

			BorderPanelOverSelected panel = get();
			if(panel != null) {
				panel.setBackgroundColor(Colores.COLOR_SELECCIONADO);
			}
		}
		
		lista.moveTo(indice);
	}
	
	public abstract void seleccionar();
}
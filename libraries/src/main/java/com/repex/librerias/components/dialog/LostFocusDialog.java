package com.repex.librerias.components.dialog;

import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;

import com.repex.librerias.components.screen.Screen;

public abstract class LostFocusDialog extends JDialog {
	
	private static final long serialVersionUID = 1L;

	private JComponent padre;	
	private boolean closable;
	
	public JComponent getPadre() {		
		return padre;
	}
	
	public boolean isClosable() {
		return closable;
	}
	
	public void setClosable(boolean closable) {
		this.closable = closable;
	}
	
	public LostFocusDialog(Screen parent, JComponent padre) {
		
		super(parent);
		
		this.padre = padre;
		this.closable = true;
		
		setContentPane(construir());
		setUndecorated(true);
		
		pack();

		posicion();
		
		this.addWindowFocusListener(new WindowFocusListener() {

			public void windowLostFocus(WindowEvent e) {

				if(isClosable()) {
					
					dispose();
				}
			}

			public void windowGainedFocus(WindowEvent e) {
				// TODO: LISTENER ??
			}
		});
		
		if(padre != null) {
		
			padre.addFocusListener(new FocusListener() {
				
				public void focusLost(FocusEvent e) {

					if(isClosable()) {
						dispose();
					}
				}
				
				public void focusGained(FocusEvent e) {}
			});
		}
		
		if(parent != null) {
			
			parent.addComponentListener(new ComponentAdapter() {
				
				@Override
				public void componentMoved(ComponentEvent e) {
					
					dispose();
				}
				
				@Override
				public void componentResized(ComponentEvent e) {
					
					dispose();
				}
				
				@Override
				public void componentHidden(ComponentEvent e) {
					
					dispose();
				}
			});
		}
	}
	
	public void posicion() {
		
		Point punto;
		
		if(padre == null) {
			
			punto = MouseInfo.getPointerInfo().getLocation();
			setLocation((int)punto.getX(), (int)punto.getY() + 2);
			
		} else {
			
			punto = padre.getLocationOnScreen();
			setLocation((int)punto.getX(), (int)punto.getY() + padre.getHeight() + 2);
		}
	}
	
	public abstract JPanel construir();
}

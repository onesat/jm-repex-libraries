package com.repex.librerias.components.labels;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Icon;

import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.components.paneles.BorderPanelOverSelected;
import com.repex.librerias.components.utilidades.Constantes;
import com.repex.librerias.utilidades.IconUtils;

public class LabelPanel extends BorderPanelOverSelected {

	private static final long serialVersionUID = 1L;

	// CONSTANTES
		
	// VARIABLES
	private String original;
	private Icon icono;
	
	private int tamano;

	private int innerEmptyBorderSize;
	
	// TODO: CONTROLAR CUANDO ES NULL
	public LabelPanel(String original, int tamano, int innerEmptyBorderSize) {

		super(BorderPanel.BORDER_SIZE_DEFAULT);

		this.original = original;

		if(original != null && !original.equals("")) {
			
			if(tamano == 0) {
				this.icono = IconUtils.escalar(getClass().getClassLoader().getResource(original));
			} else {
				this.tamano = Math.max(tamano, Constantes.MINIMUN_SIZE_DEFAULT);
				this.icono = IconUtils.escalar(getClass().getClassLoader().getResource(original), tamano);
			}
		}
		
		this.innerEmptyBorderSize = innerEmptyBorderSize;
		
		inicializar();
	}

	public LabelPanel(String original, int tamano) {
		
		this(original, tamano, 0);
	}
	
	public LabelPanel(Icon icono, int innerEmptyBorderSize) {

		super(BorderPanel.BORDER_SIZE_DEFAULT);
				
		this.original = "";
		this.icono = icono;

		this.innerEmptyBorderSize = innerEmptyBorderSize;
		
		inicializar();
	}

	public LabelPanel(Icon icono) {
		
		this(icono, 0);
	}
	
	public LabelPanel(String icono) {

		this(icono, 0, 0);
	}

	public LabelPanel() {

		this("");
	}
	
	public int getTamano() {
		
		return tamano;
	}
	
	public void setTamano(int tamano) {

		this.tamano = Math.max(tamano, Constantes.MINIMUN_SIZE_DEFAULT);		
		if(label != null) {
			label.setTamano(tamano);
		}

		revalidate();
		repaint();
	}
	
	public void setIcono(String icono) {

		if(icono != null && !icono.equals("")) {
			
			this.original = icono;	
			if(tamano == 0) {

				this.icono = IconUtils.escalar(getClass().getClassLoader().getResource(original));

			} else {

				this.icono = IconUtils.escalar(getClass().getClassLoader().getResource(original), tamano);
			}

			setIcono(this.icono);
		}
	}
	
	public void setIcono(Icon icono) {
		
		if(icono != null) {
			
			this.original = "";
			this.icono = icono;

			if(label != null) {
				remove(label);
			}

			if(tamano == 0) {
				label = new LabelOver(icono);
			} else {
				label = new LabelOver(icono, tamano);
			}

			label.setOverable(false);
			add(label);

			revalidate();
			repaint();
		}
	}
	
	public void addAccion(final Runnable accion) {
		
		addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {

				if(accion != null) {
					
					new Thread(accion).start();
				}
			}
		});
	}
	
	// INICIALIZAR
	private LabelOver label;
	
	private void inicializar() {
		
		// CONFIGURACION
		setLayout(new BorderLayout(0, 0));

		setHasBorder(false);
		
		if(innerEmptyBorderSize > 0) {
			
			setInnerEmptyBorder(true);
			setEmptyBorderSize(5);
		}
		
		setSelectable(false);
		
		// LABEL
		if(icono != null) {
			
			label = new LabelOver(icono);
			label.setOverable(false);
			add(label);
		}
		
		// LISTENER
		addOverActionListener(new OverAction() {
			
			public void over() {
				
				if(label != null) {
					
					label.setOverIcon(true);
				}
			}
		});
		
		addEndOverActionListener(new EndOverAction() {
			
			public void endOver() {

				if(label != null) {
					
					label.setOverIcon(false);
				}
			}
		});
	}
	
	@Override
	public boolean hasBorder() {
		
		if(shape != null) {
			
			return false;
		}
		
		return super.hasBorder();
	}
	
	// CAMBIAR EL SHAPE
    private Shape shape;
	
    public void setShape(Shape shape) {
    	
    	if(shape == null) {
    		
    		setOpaque(true);
    		
    	} else {
    		
    		setOpaque(false);
    	}
    	
    	this.shape = shape;
    	
    	// RECREATE BORDER
    	setBorder(createBorder());
    }
        
    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);

        if(shape != null) {
        	
        	shape.paint(g, getWidth(), getHeight(), getBackground(), super.hasBorder());
        }
    }
    
	// SHAPE
	public interface Shape {
		
		public void paint(Graphics g, int width, int height, Color background, boolean stroke);
	}
	
	public static class CircleShape extends RoundedShape {
		
		public CircleShape() {
			
			super();			
			setArcs(new Dimension(2000, 2000));
		}

		public CircleShape(Color strokeColor, int strokeSize) {

			super(new Dimension(2000, 2000), strokeColor, strokeSize);		
		}
		
		public CircleShape(boolean shady, Color shadowColor, int shadowAlpha, int shadowGap, int shadowOffset) {
			
			super(shady, shadowColor, shadowAlpha, shadowGap, shadowOffset);
			setArcs(new Dimension(2000, 2000));
		}

		public CircleShape(boolean shady, Color shadowColor, int shadowAlpha) {
			
			super(shady, shadowColor, shadowAlpha);
			setArcs(new Dimension(2000, 2000));
		}
	}
	
	public static class RoundedShape implements Shape {

	    private boolean highQuality = true;
	    
	    // BORDE
	    private Color strokeColor = Color.GRAY;
	    private int strokeSize = 2;
	    
	    // ANGULO DE LOS ARCOS
	    private Dimension arcs = new Dimension(20, 20);

	    // LA SOMBRA
	    private boolean shady = false;

	    private Color shadowColor = Color.black;
	    
	    private int shadowGap = 5;
	    private int shadowOffset = 4;
	    private int shadowAlpha = 150;
	    	    
	    public RoundedShape() {

			this(new Dimension(20, 20), Color.GRAY, 2);
		}
	    
		public RoundedShape(Dimension arcs) {
			
			this(arcs, Color.GRAY, 2);
		}
	    
		public RoundedShape(Dimension arcs, Color strokeColor, int strokeSize) {
			
			this.arcs = arcs;
			
			this.strokeColor = strokeColor;
			this.strokeSize = strokeSize;
		}
		
		public RoundedShape(boolean shady, Color shadowColor, int shadowAlpha) {
			
			this(new Dimension(20, 20), Color.GRAY, 2);
			
			this.shady = shady;
			
			this.shadowColor = shadowColor;
			this.shadowAlpha = shadowAlpha;
		}

		public RoundedShape(boolean shady, Color shadowColor, int shadowAlpha, int shadowGap, int shadowOffset) {
			
			this(shady, shadowColor, shadowAlpha);
			
			this.shadowGap = shadowGap;
			this.shadowOffset = shadowOffset;
		}
		
		public void setArcs(Dimension arcs) {
			
			this.arcs = arcs;
		}

		public void setStrokeColor(Color strokeColor) {
			
			this.strokeColor = strokeColor;
		}
		
		public void setStrokeSize(int strokeSize) {
			
			this.strokeSize = strokeSize;
		}
		
		public void paint(Graphics g, int width, int height, Color background, boolean stroke) {
						
			int shadowGap = this.shadowGap;
			Color shadowColorA = new Color(shadowColor.getRed(), shadowColor.getGreen(), shadowColor.getBlue(), shadowAlpha);
			Graphics2D graphics = (Graphics2D) g;

			// ??
			if (highQuality) {
				
				graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			}

			// DIBUJAR SOMBRA
			if (shady) {

				graphics.setColor(shadowColorA);
				graphics.fillRoundRect(
						shadowOffset,// X position
						shadowOffset,// Y position
						width - strokeSize - shadowOffset, // width
						height - strokeSize - shadowOffset, // height
						arcs.width, arcs.height);// arc Dimension

			} else {

				shadowGap = 1;
			}

			// DIBUJAR EL CUADRADO CON CORNERS
			graphics.setColor(background);
			graphics.fillRoundRect(0, 0, width - shadowGap, height - shadowGap, arcs.width, arcs.height);
			
			if(stroke) {
				
				graphics.setColor(strokeColor);
				graphics.setStroke(new BasicStroke(strokeSize));
				graphics.drawRoundRect(0, 0, width - shadowGap, height - shadowGap, arcs.width, arcs.height);
			}
			
			// RESETEAR
			graphics.setStroke(new BasicStroke());
		}
	}
}
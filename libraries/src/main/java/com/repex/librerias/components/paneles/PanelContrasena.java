package com.repex.librerias.components.paneles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import com.repex.librerias.components.labels.LabelOver;
import com.repex.librerias.components.textfield.HintPasswordField;
import com.repex.librerias.components.textfield.HintTextField;
import com.repex.librerias.utilidades.Constantes;

public class PanelContrasena extends BorderPanel {
	
	private static final long serialVersionUID = 1L;

	private String hint;
	private int margin;
	private Color hintColor;
	private Color normal;
	
	private int columns;
	
	public PanelContrasena(String hint, int margin, Color hintColor, Color normalColor, int columns) {

		super(1, Color.BLACK, 5, false, true);
		
		this.hint = hint;

		this.margin = margin;
		
		this.hintColor = hintColor;
		this.normal = normalColor;
		
		this.columns = columns;
		
		inicializar();
	}
	
	public PanelContrasena(String hint) {
		
		this(hint, HintTextField.MARGIN_DEFAULT, HintTextField.HINT_COLOR_DEFAULT, HintTextField.NORMAL_COLOR_DEFAULT, 20);
	}
	
	public PanelContrasena(int colums) {

		this("", HintTextField.MARGIN_DEFAULT, HintTextField.HINT_COLOR_DEFAULT, HintTextField.NORMAL_COLOR_DEFAULT, colums);		
	}
	
	public PanelContrasena() {
		
		this(20);
	}
	
	public void setColumns(int columns) {
		
		this.columns = columns;
		password.setColumns(columns);
	}
	
	private HintPasswordField password;
	private LabelOver label;
	
	// POR SI SE QUIEREN MODIFICAR
	public HintPasswordField getPassword() {
		
		return password;
	}
	
	public LabelOver getLabel() {
		
		return label;
	}
	
	private void inicializar() {
				
		// CONFIGURACION
		setLayout(new FlowLayout(FlowLayout.LEADING));
				
		// TEXT FIELD			
		password = new HintPasswordField(hint, margin, hintColor, normal);
		password.setColumns(columns);
		password.setHasPopupMenu(true);
		add(password, BorderLayout.CENTER);
		
		// LABEL
		label = new LabelOver(Constantes.CARPETA_RECURSOS + "textfield/ver_contrasena.png", 30);
		add(label, BorderLayout.EAST);
		
		label.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {
				
				super.mousePressed(e);
				
				if(!password.isShow() && !password.getText().equals("")) {
					
					password.setEchoChar((char) 0);
				}
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				
				super.mouseReleased(e);

				if(!password.isShow() && !password.getText().equals("")) {
					
					password.setEchoChar('*');
				}
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				
				super.mouseExited(e);

				if(!password.isShow() && !password.getText().equals("")) {
					
					password.setEchoChar('*');
				}
			}
		});
	}
}
package com.repex.librerias.components.paneles.carga;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JPanel;

import com.repex.librerias.components.labels.MyJLabel;
import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.utilidades.IconUtils;

public abstract class PanelCargando extends BorderPanel {
	
	private static final long serialVersionUID = 1L;

	private JPanel cargando;
	
	public PanelCargando() {
		
		super(0);
		
		// CONFIGURACION
		setLayout(new BorderLayout(0, 0));
		
		// CREAR
		inicializar();
	}
	
	private void inicializar() {
		
		// CARGANDO
		cargando = new BorderPanel(0, Color.WHITE, 5, false, true);
		cargando.setLayout(new FlowLayout(FlowLayout.LEADING));
		
		MyJLabel label = new MyJLabel(IconUtils.escalar(getClass().getClassLoader().getResource("com/repex/librerias/gif/loading.gif")));
		cargando.add(label);
		
		// CONTENIDO	
		JPanel central = construir();
		if(central != null) {

			add(central, BorderLayout.CENTER);
			add(cargando, BorderLayout.NORTH);

		} else {

			add(cargando, BorderLayout.CENTER);
		}
	}
	
	public void setCargando(boolean cargando) {
		
		this.cargando.setVisible(cargando);
		
		revalidate();
		repaint();
	}
	
	protected abstract JPanel construir();
}

package com.repex.librerias.components.popupmenu;

import java.awt.datatransfer.StringSelection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JLabel;

import com.repex.librerias.utilidades.ClipboardController;

public class LabelPopupMenu extends MyPopupMenu {
	
	private static final long serialVersionUID = 1L;

	public LabelPopupMenu(final JLabel parent) {

		super(parent);

		List<Opcion> opciones = new LinkedList<Opcion>();
				
		// COPIAR
		Opcion copiar = new MyPopupMenu.Opcion("Copiar", new Runnable() {

			public void run() {
				
				ClipboardController.getSystemClipboard().setContents(new StringSelection(parent.getText()), null);
			}
		});
		
		copiar.addCondicion(new MyPopupMenu.Condicion() {

			public boolean comprobar() {
				
				return !parent.getText().equals("");
			}
		});
		
		opciones.add(copiar);
		
		this.addOpciones(opciones);
	}
}
package com.repex.librerias.components.over;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.MouseInfo;
import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class OverPanel extends JDialog {
	
	private static final long serialVersionUID = 1L;
	
	// VARIABLES
	private int width = 200;
	private int height = 200;
	
	private Color backgroundColor = Color.WHITE;
	private Color borderColor = Color.BLACK;
	
	private Margins margins;
	
	private Thread lanzamiento; // HILO PARA LANZAR EN DIFERIDO
	
	public Margins getMargins() {
		
		return margins;
	}
	
	// INTERFAZ
	private JPanel contentPane;
	
	// VARIABLES
	private boolean construido = false;	
	private boolean reconstruir = false;
	
	private List<Informacion> contenido;
	
	// CONSTANTES
	private static final int wait = 2; // TIEMPO A ESPERAR POR DEFECTO
	
	public OverPanel(int width, int height, Color backgroundColor, Color borderColor, Margins margins, List<Informacion> contenido) {
		
		this.width = width;
		this.height = height;
		
		this.backgroundColor = backgroundColor;
		this.borderColor = borderColor;
		
		this.margins = margins;
		
		this.contenido = contenido;
		
		inicializar();
	}

	public OverPanel(int width, int height, Color backgroundColor, Color borderColor, Margins margins) {
		
		this(width, height, backgroundColor, borderColor, margins, new LinkedList<Informacion>());
	}
	
	public OverPanel(int width, int height, Margins margins, List<Informacion> contenido) {
		
		this(width, height, Color.WHITE, Color.BLACK, margins, contenido);
	}
	
	public OverPanel(int width, int height, Margins margins) {
		
		this(width, height, margins, new LinkedList<Informacion>());
	}
	
	public OverPanel(int width, int height, List<Informacion> contenido) {
		
		this(width, height, new Margins(), contenido);
	}

	public OverPanel(int width, int height) {
		
		this(width, height, new Margins(), new LinkedList<Informacion>());
	}
	
	public OverPanel(List<Informacion> contenido) {
		
		this(200, 200, contenido);
	}

	public OverPanel() {
		
		this(200, 200, new LinkedList<Informacion>());
	}
	
	public void setBackgroundColor(Color backgroundColor) {
		
		this.backgroundColor = backgroundColor;
		contentPane.setBackground(backgroundColor);
	}
	
	public boolean isReconstruir() {
		
		return reconstruir;
	}
	
	public void setReconstruir(boolean reconstruir) {
		
		this.reconstruir = reconstruir;
	}
	
	// OPERACIONES CONTENIDO
	public void addContenido(Informacion informacion) {
		
		this.contenido.add(informacion);
	}

	public void addContenido(List<Informacion> informacion) {
		
		this.contenido.addAll(informacion);
	}
	
	private void inicializar() {
		
		this.setSize(width, height);
		this.setUndecorated(true);
		this.setResizable(false);

		this.setFocusableWindowState(false);
		this.setFocusable(false);

		contentPane = new JPanel();
		
		Border empty = BorderFactory.createEmptyBorder(margins.getTop(), margins.getLeft(), margins.getBotton(), margins.getRight());
		Border mate = BorderFactory.createMatteBorder(2, 2, 2, 2, borderColor);
		contentPane.setBorder(BorderFactory.createCompoundBorder(mate, empty));
		
		contentPane.setBackground(backgroundColor);
		setContentPane(contentPane);

		setVisible(false);
	}

	protected void crear() {

		if(!construido || reconstruir) {
			
			construido= true;
			
			// QUITAR ANTERIOR
			contentPane.removeAll();

			contentPane.revalidate();
			contentPane.repaint();

			// ANADIR NUEVO
			JPanel panel = new JPanel();
			panel.setOpaque(false);
			panel.setLayout(new BorderLayout(0, 0));			
			contentPane.add(panel);
			
			JPanel construir = construir();
			if(construir != null) {
				
				panel.add(construir, BorderLayout.CENTER); // LA CONSTRUCCION EN EL CENTRO
			}

			// ANADIR DEBAJO LA INFORMACION
			if(contenido != null && !contenido.isEmpty()) {
				
				JPanel panelInformacion = new JPanel();
				panelInformacion.setOpaque(false);
				panelInformacion.setLayout(new BoxLayout(panelInformacion, BoxLayout.Y_AXIS));
				panel.add(panelInformacion, BorderLayout.SOUTH);

				for(Informacion informacion : contenido) {

					try {

						panelInformacion.add(informacion.construir());
						
					} catch (Exception e) {
						
						e.printStackTrace();
					}
				}
			}
			
			contentPane.revalidate();
			contentPane.repaint();
		}
	}
	
	protected JPanel construir() {
		
		return null;
	}
	
	protected void localizacion() {

		// TODO: SI ESTA MUY A LA DERECHA PONER A LA IZQUIERDA
		// TODO: SI ESTA MUY ABAJO PONER ARRIBA
		Point punto = MouseInfo.getPointerInfo().getLocation();
		this.setLocation((int)punto.getX() + 50, (int)punto.getY() - 50);
	}
	
	// ACCIONES OVER
	public void lanzar() {
		
		lanzar(wait);
	}
	
	public void lanzar(final double wait) {
		
		if(lanzamiento == null) {
			
			lanzamiento = new Thread(new Runnable() {
				
				public void run() {
					
					// ESPERAR
					try {
						
						int waiting = (int)(wait * 1000);
						Thread.sleep(waiting);

						crear();
						localizacion();
						
						setVisible(true);
						
					} catch (InterruptedException e) {}
				}
			});
			
			lanzamiento.start();
		}
	}
	
	public void detener() {
		
		if(lanzamiento != null) {
			
			lanzamiento.interrupt();
			lanzamiento = null;
			
			dispose();
		}
	}
	
	// AUXILIAR
	public static abstract class Informacion {
	
		private OverPanel panel;
		private Object[] data;
		
		public Informacion(OverPanel panel, Object... data) {
			
			this.panel = panel;
			this.data = data;
		}
		
		public OverPanel getPanel() {
			
			return panel;
		}
		
		public void setPanel(OverPanel panel) {
			
			this.panel = panel;
		}
		
		public Object[] getData() {
			
			return data;
		}
		
		// PERMITE CONSTRUIR EN DIFERIDO EL CONTENIDO DEL PANEL. ASI NO HAY QUE CARGARLO ANTES
		public abstract JPanel construir(Object... data) throws ClassCastException;
	}
	
	public static class Margins {
		
		private int top = 0;
		private int botton = 0;
		private int right = 0;
		private int left = 0;
		
		public Margins(int top, int botton, int right, int left) {
			
			this.top = top;
			this.botton = botton;
			this.right = right;
			this.left = left;
		}

		public Margins() {
			
			this(0, 0, 0, 0);
		}
		
		public int getTop() {
			
			return top;
		}
		
		public int getBotton() {
			
			return botton;
		}
		
		public int getRight() {
			
			return right;
		}
		
		public int getLeft() {
			
			return left;
		}
	}
}
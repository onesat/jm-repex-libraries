package com.repex.librerias.components.labels;

import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;

import com.repex.librerias.components.labels.LabelPanel;
import com.repex.librerias.components.utilidades.UrlControler;
import com.repex.librerias.utilidades.Constantes;

public class LabelPanelUrl extends LabelPanel {

	private static final long serialVersionUID = 1L;
	
	private int tamano;
	
	private Icon defecto;
	
	public LabelPanelUrl(String url, int tamano) {
		
		super("com/repex/librerias/gif/loading_200.gif", tamano);
		
		this.tamano = tamano;
		
		load(url);
	}

	public LabelPanelUrl(String url) {		
		this(url, 0);
	}
	
	public void setDefecto(Icon defecto) {
		this.defecto = defecto;
	}
	
	private int contador = 0;
	private int limite = 2;
	
	public void setLimite(int limite) {
		this.limite = limite;
	}
	
	@Override
	public void setIcono(String icono) {
		
		if(icono.startsWith("http")) {
			
			load(icono);
			
		} else {
			
			super.setIcono(icono);
		}
	}
	
	private void load(final String url) {
		
		new Thread(new Runnable() {
			
			public void run() {
				
				// CARGAR EN EL CONTROLER			
				try {
				    
				    setIcono(UrlControler.getSingleton().cargar(url, tamano));
				    				    
				} catch (Exception e) {

					boolean error = false;
					
					if(defecto != null) {
						
						setIcono(defecto);
					
					} else if(UrlControler.getSingleton().getDefecto() != null && !UrlControler.getSingleton().getDefecto().equals("")) {
						
					    try {
							setIcono(UrlControler.getSingleton().cargar(UrlControler.getSingleton().getDefecto(), tamano));
						} catch (Exception e1) {
							error = true;
						}
					    
					} else {
						
						error = true;
					}
					
					if(error) {
						setIcono(Constantes.CARPETA_RECURSOS + "defecto.png");
					}
					
					contador++;
					
					if(contador < limite && onErrorListener != null) {
						for(OnErrorListener listener : onErrorListener) {
							listener.error();
						}
					}
				}
			}
			
		}).start();
	}
	
	public interface OnErrorListener {		
		public void error();
	}
	
	private List<OnErrorListener> onErrorListener;
	
	public void addOnErrorListener(OnErrorListener listener) {
		
		if(onErrorListener == null) {
			onErrorListener = new ArrayList<OnErrorListener>();
		}
		
		onErrorListener.add(listener);
	}
}
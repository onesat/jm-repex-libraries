package com.repex.librerias.components.utilidades;

public interface OnLaterConstructor {

	public void onConstructor();
}

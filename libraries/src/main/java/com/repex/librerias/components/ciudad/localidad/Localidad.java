package com.repex.librerias.components.ciudad.localidad;

import java.util.LinkedList;
import java.util.List;

import javax.swing.Icon;

import com.repex.librerias.utilidades.Constantes;
import com.repex.librerias.utilidades.IconUtils;

public class Localidad {

	// ENUM
	public static Localidad ALAVA = new Localidad(1, "ALAVA", Comunidad.PAIS_VASCO);
	public static Localidad ALBACETE = new Localidad(2, "ALBACETE", Comunidad.CASTILLA_MANCHA);
	public static Localidad ALICANTE = new Localidad(3, "ALICANTE", Comunidad.VALENCIA);
	public static Localidad ALMERIA = new Localidad(4, "ALMERIA", Comunidad.ANDALUCIA);
	public static Localidad ASTURIAS = new Localidad(5, "ASTURIAS", Comunidad.ASTURIAS);
	public static Localidad AVILA = new Localidad(6, "AVILA", Comunidad.CASTILLA_LEON);
	public static Localidad BADAJOZ = new Localidad(7, "BADAJOZ", Comunidad.EXTREMADURA);
	public static Localidad BALEARES = new Localidad(8, "BALEARES", Comunidad.ISLAS_BALEARES);
	public static Localidad BARCELONA = new Localidad(9, "BARCELONA", Comunidad.CATALUNA);
	public static Localidad BURGOS = new Localidad(10, "BURGOS", Comunidad.CASTILLA_LEON);
	public static Localidad CACERES = new Localidad(11, "CACERES", Comunidad.EXTREMADURA);
	public static Localidad CADIZ = new Localidad(12, "CADIZ", Comunidad.ANDALUCIA);
	public static Localidad CANTABRIA = new Localidad(13, "CANTABRIA", Comunidad.CANTABRIA);
	public static Localidad CASTELLON = new Localidad(14, "CASTELLON", Comunidad.VALENCIA);
	public static Localidad CEUTA = new Localidad(15, "CEUTA", Comunidad.CEUTA);
	public static Localidad CIUDAD_REAL = new Localidad(16, "CIUDAD REAL", Comunidad.CASTILLA_MANCHA);
	public static Localidad CORDOBA = new Localidad(17, "CORDOBA", Comunidad.ANDALUCIA);
	public static Localidad CORUNA = new Localidad(18, "CORUNA", Comunidad.GALICIA);
	public static Localidad CUENCA = new Localidad(19, "CUENCA", Comunidad.CASTILLA_MANCHA);
	public static Localidad GIRONA = new Localidad(20, "GIRONA", Comunidad.CATALUNA);
	public static Localidad GRANADA = new Localidad(21, "GRANADA", Comunidad.ANDALUCIA);
	public static Localidad GUADALAJARA = new Localidad(22, "GUADALAJARA", Comunidad.CASTILLA_MANCHA);
	public static Localidad GUIPUZCOA = new Localidad(23, "GUIPUZCOA", Comunidad.PAIS_VASCO);
	public static Localidad HUELVA = new Localidad(24, "HUELVA", Comunidad.ANDALUCIA);
	public static Localidad HUESCA = new Localidad(25, "HUESCA", Comunidad.ARAGON);
	public static Localidad JAEN = new Localidad(26, "JAEN", Comunidad.ANDALUCIA);
	public static Localidad LEON = new Localidad(27, "LEON", Comunidad.CASTILLA_LEON);
	public static Localidad LUGO = new Localidad(28, "LUGO", Comunidad.GALICIA);
	public static Localidad LLEIDA = new Localidad(29, "LLEIDA", Comunidad.CATALUNA);
	public static Localidad MADRID = new Localidad(30, "MADRID", Comunidad.MADRID);
	public static Localidad MALAGA = new Localidad(31, "MALAGA", Comunidad.ANDALUCIA);
	public static Localidad MELILLA = new Localidad(32, "MELILLA", Comunidad.MELILLA);
	public static Localidad MURCIA = new Localidad(33, "MURCIA", Comunidad.MURCIA);
	public static Localidad NAVARRA = new Localidad(34, "NAVARRA", Comunidad.NAVARRA);
	public static Localidad OURENSE = new Localidad(35, "OURENSE", Comunidad.GALICIA);
	public static Localidad PALENCIA = new Localidad(36, "PALENCIA", Comunidad.CASTILLA_LEON);
	public static Localidad LAS_PALMAS = new Localidad(37, "LAS PALMAS", Comunidad.ISLAS_CANARIAS);
	public static Localidad PONTEVEDRA = new Localidad(38, "PONTEVEDRA", Comunidad.GALICIA);
	public static Localidad LA_RIOJA = new Localidad(39, "LA RIOJA", Comunidad.LA_RIOJA);
	public static Localidad SALAMANCA = new Localidad(40, "SALAMANCA", Comunidad.CASTILLA_LEON);
	public static Localidad SANTA_CRUZ_DE_TENERIFE = new Localidad(41, "SANTA CRUZ DE TENERIFE", Comunidad.ISLAS_CANARIAS);
	public static Localidad SEGOVIA = new Localidad(42, "SEGOVIA", Comunidad.CASTILLA_LEON);
	public static Localidad SEVILLA = new Localidad(43, "SEVILLA", Comunidad.ANDALUCIA);
	public static Localidad SORIA = new Localidad(44, "SORIA", Comunidad.CASTILLA_LEON);
	public static Localidad TARRAGONA = new Localidad(45, "TARRAGONA", Comunidad.CATALUNA);
	public static Localidad TERUEL = new Localidad(46, "TERUEL", Comunidad.ARAGON);
	public static Localidad TOLEDO = new Localidad(47, "TOLEDO", Comunidad.CASTILLA_MANCHA);
	public static Localidad VALENCIA = new Localidad(48, "VALENCIA", Comunidad.VALENCIA);
	public static Localidad VALLADOLID = new Localidad(49, "VALLADOLID", Comunidad.CASTILLA_LEON);
	public static Localidad VIZCAYA = new Localidad(50, "VIZCAYA", Comunidad.PAIS_VASCO);
	public static Localidad ZAMORA = new Localidad(51, "ZAMORA", Comunidad.CASTILLA_LEON);
	public static Localidad ZARAGOZA = new Localidad(52, "ZARAGOZA", Comunidad.ARAGON);
	
	// CONSTANTES
	public final static String CARPETA_RECURSOS = Constantes.CARPETA_RECURSOS + "localidad/";
	
	// VARIABLES
	private int id;
	private String nombre;
	
	private Comunidad comunidad;
	
	private Icon icono;

	private Localidad(int id, String nombre, Comunidad comunidad, Icon icono) {
		
		this.id = id;
		this.nombre = nombre;
		
		this.comunidad = comunidad;
		
		this.icono =  icono;
	}
	
	private Localidad(int id, String nombre, Comunidad comunidad) {

		this.id = id;
		this.nombre = nombre;
		
		this.comunidad = comunidad;
		
		String prueba = CARPETA_RECURSOS + comunidad.carpeta + "/" + nombre.toLowerCase().replaceAll(" ", "_") + ".png";
		this.icono =  IconUtils.escalar(getClass().getClassLoader().getResource(prueba));
	}
	
	public int getId() {
		
		return id;
	}
	
	public String getNombre() {
		
		return nombre;
	}
	
	public Comunidad getComunidad() {
		
		return comunidad;
	}
	
	public Icon getIcono() {
		
		return icono;
	}
	
	// VALUES
	public static List<Localidad> values() {
		
		List<Localidad> values = new LinkedList<Localidad>();
		values.add(ALAVA);
		values.add(ALBACETE);
		values.add(ALICANTE);
		values.add(ALMERIA);
		values.add(ASTURIAS);
		values.add(AVILA);
		values.add(BADAJOZ);
		values.add(BALEARES);
		values.add(BARCELONA);
		values.add(BURGOS);
		values.add(CACERES);
		values.add(CADIZ);
		values.add(CANTABRIA);
		values.add(CASTELLON);
		values.add(CEUTA);
		values.add(CIUDAD_REAL);
		values.add(CORDOBA);
		values.add(CORUNA);
		values.add(CUENCA);
		values.add(GIRONA);
		values.add(GRANADA);
		values.add(GUADALAJARA);
		values.add(GUIPUZCOA);
		values.add(HUELVA);
		values.add(HUESCA);
		values.add(JAEN);
		values.add(LEON);
		values.add(LUGO);
		values.add(LLEIDA);
		values.add(MADRID);
		values.add(MALAGA);
		values.add(MELILLA);
		values.add(MURCIA);
		values.add(NAVARRA);
		values.add(OURENSE);
		values.add(PALENCIA);
		values.add(LAS_PALMAS);
		values.add(PONTEVEDRA);
		values.add(LA_RIOJA);
		values.add(SALAMANCA);
		values.add(SANTA_CRUZ_DE_TENERIFE);
		values.add(SEGOVIA);
		values.add(SEVILLA);
		values.add(SORIA);
		values.add(TARRAGONA);
		values.add(TERUEL);
		values.add(TOLEDO);
		values.add(VALENCIA);
		values.add(VALLADOLID);
		values.add(VIZCAYA);
		values.add(ZAMORA);
		values.add(ZARAGOZA);
		
		return values;
	}
	
	public static List<Localidad> values(Comunidad comunidad) {
		
		List<Localidad> values = new LinkedList<Localidad>();
		
		for(Localidad localidad : values()) {
			
			if(localidad.getComunidad() == comunidad) {
				
				values.add(localidad);
			}
		}
		
		return values;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Localidad other = (Localidad) obj;
		return id == other.id;
	}
}
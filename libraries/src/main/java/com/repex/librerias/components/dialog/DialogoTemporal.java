package com.repex.librerias.components.dialog;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;

public class DialogoTemporal extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private float tiempo; // TIEMPO EN SEGUNDOS
	
	public DialogoTemporal(JFrame parent, float tiempo) {
		
		super(parent);
		
		this.tiempo = tiempo;
		
		lanzar();
		
		addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosed(WindowEvent e) {
				cerrar();
			}
		});
	}

	public DialogoTemporal(JFrame parent) {
		this(parent, 2.5f);
	}
	
	private Thread hilo;
	
	private void lanzar() {
		
		if(tiempo > 0) {
			
			hilo = new Thread(new Runnable() {

				public void run() {

					try {
						Thread.sleep((int)(tiempo * 1000));
					} catch (InterruptedException e) {}

					dispose();
				}
			});

			hilo.start();
		}
	}
	
	public void cerrar() {
		
		if(hilo.isAlive()) {
			
			hilo.interrupt();
		}
	}
}
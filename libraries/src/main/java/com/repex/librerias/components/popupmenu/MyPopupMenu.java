package com.repex.librerias.components.popupmenu;

import java.awt.Color;
import java.awt.Component;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class MyPopupMenu extends JPopupMenu {

	private static final long serialVersionUID = 1L;

	private Component padre;
	private List<Opcion> opciones;
	
	public MyPopupMenu(Component padre, List<Opcion> opciones) {

		this.padre = padre;
		this.opciones = opciones;

		// QUITAR FOCUS PARA QUE LO TENGA EL COMPONENT
		this.setFocusable(false);
	}

	public MyPopupMenu(Component parent) {

		this(parent, new LinkedList<Opcion>());
	}
	
	public Component getPadre() {
		
		return padre;
	}
	
	public List<Opcion> getOpciones() {
		
		return opciones;
	}
	
	public void addOpcion(Opcion opcion) {
		
		this.opciones.add(opcion);
	}
	
	public void addOpciones(List<Opcion> opciones) {
		
		this.opciones.addAll(opciones);
	}
	
	public void overrideOpciones(List<Opcion> opciones) {
		
		this.opciones.clear();
		this.opciones.addAll(opciones);
	}
	
	public void clearOpciones() {
		
		this.opciones.clear();
	}
	
	public void crear() {
	
		construir(padre, opciones);
	}
	
	private void construir(Component parent, List<Opcion> opciones) {
		
		if(!opciones.isEmpty()) {

			// LIMPIAR
			this.removeAll();
			
			// CREAR NUEVAS
			for(Opcion opcion : opciones) {

				opcion.construir(this);
			}

			// LOCALIZACION
			localizacion(parent);
		}
	}
	
	private void localizacion(Component parent) {

		// LOCALIZACION
		Point point = parent.getMousePosition();
		if(point != null) {

			show(parent, (int) point.getX()+5, (int) point.getY()+5);
			
		} else {
			
			point = MouseInfo.getPointerInfo().getLocation();
			
			int x = point.x - parent.getLocationOnScreen().x;
			int y = point.y - parent.getLocationOnScreen().y;
			
			show(parent, x+5, y+5);
		}
	}
	
	public static class Opcion {
		
		private Icon icono;
		
		private String nombre;
		private Runnable accion;
		
		private Color foreground;

		private List<Condicion> condiciones;
		
		public Opcion(Icon icono, String nombre, Runnable accion, Color foreground, List<Condicion> condiciones) {
			
			this.icono = icono;
			
			this.nombre = nombre;
			this.accion = accion;
			
			this.foreground = foreground;
			
			this.condiciones = condiciones;
		}

		public Opcion(Icon icono, String nombre, Runnable accion, Color foreground) {
			
			this(icono, nombre, accion, foreground, new LinkedList<Condicion>());
		}
		
		public Opcion(Icon icono, String nombre, Runnable accion) {

			this(icono, nombre, accion, Color.BLACK);
		}
		
		public Opcion(String nombre, Runnable accion, Color foreground) {
			
			this(null, nombre, accion, foreground);
		}

		public Opcion(String nombre, Runnable accion) {
			
			this(nombre, accion, Color.BLACK);
		}

		public Opcion(String nombre) {
			
			this(nombre, null, Color.BLACK);
		}
		
		public String getNombre() {
			
			return nombre;
		}
		
		public Runnable getAccion() {
			
			return accion;
		}
		
		public void setCondiciones(List<Condicion> condiciones) {
			
			this.condiciones = condiciones;
		}
		
		public void addCondicion(Condicion condicion) {
			
			this.condiciones.add(condicion);
		}
		
		public boolean isCondiciones() {
			
			if(condiciones.isEmpty())
				return true;
			
			for(Condicion condicion : condiciones) {
				
				if(!condicion.comprobar())
					return false;
			}
			
			return true;
		}
		
		public void construir(MyPopupMenu menu) {
			
			JMenuItem menuItem = new JMenuItem(getNombre());
			if(icono != null) {
				
				menuItem.setIcon(icono);
			}
			
			if(isCondiciones()) {
				
				menuItem.setForeground(foreground);

				menuItem.addMouseListener(new MouseAdapter() {

					@Override
					public void mousePressed(MouseEvent e) {

						if(getAccion() != null) {

							new Thread(getAccion()).start();
						}					
					}
				});
				
			} else {

				menuItem.setForeground(Color.GRAY);
			}
			
	        menu.add(menuItem);	
		}
	}
	
	public interface Condicion {
		
		public boolean comprobar();
	}
	
	public static class OpcionSeparator extends Opcion {

		public OpcionSeparator() {
			
			super("", null);
		}
		
		@Override
		public void construir(MyPopupMenu menu) {
			
			menu.addSeparator();
		}
	}
}
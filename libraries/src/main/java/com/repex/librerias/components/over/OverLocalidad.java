package com.repex.librerias.components.over;

import java.awt.Color;
import java.awt.FlowLayout;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.repex.librerias.components.ciudad.localidad.Localidad;

public class OverLocalidad extends OverPanel {

	private static final long serialVersionUID = 1L;
	
	// CONSTANTES
	private static final int MARGIN = 5;
	
	public OverLocalidad(final Localidad localidad, List<OverPanel.Informacion> contenido) {
		
		super(0, 0, Color.decode("#666666"), Color.WHITE, new Margins(MARGIN, MARGIN, MARGIN, MARGIN));
		
		addContenido(new Informacion(this) {
			
			@Override
			public JPanel construir(Object... data) {

				JPanel panel = new JPanel();
				panel.setLayout(new FlowLayout(FlowLayout.CENTER));
				panel.setBackground(Color.BLACK);
				panel.setOpaque(false);
				
				JLabel labelCiudad = new JLabel(localidad.getNombre());
				labelCiudad.setForeground(Color.WHITE);
				panel.add(labelCiudad);
				
				return panel;
			}
		});
		
		if(contenido != null && !contenido.isEmpty()) {
			
			addContenido(contenido);
		}
	}
	
	public OverLocalidad(Localidad localidad) {
		
		this(localidad, new LinkedList<OverPanel.Informacion>());
	}
	
	@Override
	protected void crear() {
		
		super.crear();
		pack();
	}
}
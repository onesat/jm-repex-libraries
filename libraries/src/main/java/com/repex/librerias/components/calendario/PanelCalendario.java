package com.repex.librerias.components.calendario;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jfree.ui.tabbedui.VerticalLayout;

import com.repex.librerias.components.calendario.PanelDia.OnDaySelected;
import com.repex.librerias.components.labels.LabelPanel;
import com.repex.librerias.components.over.OverPanel;
import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.date.Fecha;
import com.repex.librerias.utilidades.Constantes;

public class PanelCalendario extends BorderPanel {

	private static final long serialVersionUID = 1L;
	
	// CONSTANTES
	public final static int SIZE_DEFAULT = 200;
	public final static Color ACTUAL_COLOR_DEFAULT = Color.decode("#FFC57A");
	
	private final static int DAY_SPACE = 2;
	private final static int COLUMNS = 7;
	
	// VARIABLES
	private int size;
	
	// INTERFAZ
	private JPanel panelTitulo;
	private JLabel titulo;
	
	private JPanel semanas;
	private JPanel contenido;
	
	// VARIABLES INTERNAS
	private Fecha actual; // INDICA EL MES Y ANO ACTUALES

	private float overTime = 1.5f;
	
	private boolean markFirstDay = true;

	private List<OverPanel.Informacion> contenidoComun;
	private List<OverPanel.Informacion> contenidoOver;
	
	private List<PanelDia.OnDaySelected> acciones;
	private List<OnMonthChanged> accionesCambio;
	
	private OnResetData onResetData;
	
	private List<PanelDia> paneles;
	
	public PanelCalendario(int size, int borderSize, Color borderColor) {
		
		super(borderSize, borderColor);

		this.actual = new Fecha();
		
		this.size = Math.max(size, SIZE_DEFAULT);

		this.contenidoComun = new LinkedList<OverPanel.Informacion>();
		this.contenidoOver = new LinkedList<OverPanel.Informacion>();
		
		this.acciones = new LinkedList<PanelDia.OnDaySelected>();
		this.accionesCambio = new LinkedList<OnMonthChanged>();
		
		this.paneles = new LinkedList<PanelDia>();
		
		this.contenidoPaneles = new HashMap<Fecha, List<OverPanel.Informacion>>();		
		this.contenidoOverPaneles = new HashMap<Fecha, List<OverPanel.Informacion>>();
		
		inicializar();

		// WHEL LISTENER
		addMouseWheelListener(new MouseWheelListener() {
			
			public void mouseWheelMoved(MouseWheelEvent e) {
				
				// CAMBIAR MES
				cambiarMes(e.getWheelRotation());				
			}
		});
	}
	
	public PanelCalendario(int size, int borderSize) {
		
		this(size, borderSize, BorderPanel.BORDER_COLOR_DEFAULT);
	}

	public PanelCalendario(int size) {
		
		this(size, BorderPanel.BORDER_SIZE_DEFAULT, BorderPanel.BORDER_COLOR_DEFAULT);
	}

	// HERENCIA
	@Override
	public void setEmptyBorderSize(int emptyBorderSize) {
		
		super.setEmptyBorderSize(emptyBorderSize);
		updateSize();
	}
	
	@Override
	public void setBorderSize(int borderSize) {
		
		super.setBorderSize(borderSize);
		updateSize();
	}
	
	@Override
	public void setOutterEmptyBorder(boolean outterEmptyBorder) {
		
		super.setOutterEmptyBorder(outterEmptyBorder);
		updateSize();
	}
	
	@Override
	public void setInnerEmptyBorder(boolean innerEmptyBorder) {
		
		super.setInnerEmptyBorder(innerEmptyBorder);
		updateSize();
	}
	
	public int returnSize() {
		
		return size;
	}
	
	public void changeSize(int size) {
		
		this.size = size;
		
		// QUITAR ANTERIOR
		removeAll();
		
		// PONER DE NUEVO
		inicializar();
		
		revalidate();
		repaint();
	}
	
	private void updateSize() {

		int borderSize = 0;
		
		if(hasBorder()) {
			
			borderSize += getBorderSize();
		}
		
		if(isInnerEmptyBorder()) {
			
			borderSize += (getInnerBorderSize()*2);
		}

		if(isOutterEmptyBorder()) {
			
			borderSize += (getOutterBorderSize()*2);
		}
		
		int tamanoBordePanel = 2;
		int tamanoPanel = size / COLUMNS - tamanoBordePanel - DAY_SPACE; // 7 PANELES DE IZQUIERDA A DERECHA
		
		int sum = (int) (20 - ((size - 350) * 0.133333333)); // FORMULA DECRECIENTE: EN 350 = 20; A PARTIR DE 450 NEGATIVO		
		Dimension d = new Dimension(size+borderSize+tamanoPanel+(DAY_SPACE*2), size+borderSize+sum);
		
		setPreferredSize(d);
		setMaximumSize(d);
		setMinimumSize(d);
	}
	
	// VARIABLES
	public boolean isMarkFirstDay() {
		
		return markFirstDay;
	}
	
	public void setMarkFirstDay(boolean markFirstDay) {
		
		this.markFirstDay = markFirstDay;

		for(PanelDia panel : getPaneles()) {

			if(panel.getFecha().comprobar(new Fecha())) {
				
				if(markFirstDay) {
					
					panel.setBackgroundColor(ACTUAL_COLOR_DEFAULT);
				
				} else {
					
					panel.setBackgroundColor(PanelDia.BACKGROUND_COLOR_DEFAULT);
				}
			}
		}
	}

	public void addContenido(OverPanel.Informacion contenido) {
		
		this.contenidoComun.add(contenido);

		for(PanelDia panel : paneles) {
			
			panel.addContenido(contenido);
		}
	}
	
	public void addContenido(List<OverPanel.Informacion> contenido) {
		
		this.contenidoComun.addAll(contenido);

		for(PanelDia panel : paneles) {
			
			panel.addContenido(contenido);
		}
	}
	
	public void addContenidoOver(OverPanel.Informacion contenido) {
		
		this.contenidoOver.add(contenido);

		for(PanelDia panel : paneles) {
			
			panel.addContenidoOver(contenido);
		}
	}
	
	public void addContenidoOver(List<OverPanel.Informacion> contenido) {
		
		this.contenidoOver.addAll(contenido);

		for(PanelDia panel : paneles) {
			
			panel.addContenidoOver(contenido);
		}
	}
	
	public void addCambioMes(OnMonthChanged accion) {
	
		this.accionesCambio.add(accion);
	}
	
	public void addAccion(OnDaySelected accion) {
		
		this.acciones.add(accion);

		for(PanelDia panel : paneles) {
			
			panel.addAccion(accion);
		}
	}
	
	public void setOnResetData(OnResetData onResetData) {
		
		this.onResetData = onResetData;
	}
	
	public List<PanelDia> getPaneles() {
	
		return paneles;
	}
	
	public PanelDia getPanel(Fecha fecha) {
		
		for(PanelDia panel : paneles) {
			
			if(panel.getFecha().comprobar(fecha)) {
				
				return panel;
			}
		}
		
		return null;
	}
	
	public void setOverTime(float overTime) {

		this.overTime = overTime;
		
		for(PanelDia panel : paneles) {
			
			panel.setOverTime(overTime);
		}
	}
	
	// CONTENIDO DE PANELES
	private Map<Fecha, List<OverPanel.Informacion>> contenidoPaneles;
	
	public List<OverPanel.Informacion> getContenido(Fecha fecha) {
		
		for (Map.Entry<Fecha, List<OverPanel.Informacion>> entry : contenidoPaneles.entrySet()) {
			
			if(entry.getKey().comprobar(fecha)) {
				
				return entry.getValue();
			}
		}
		
		return null;
	}
	
	
	public void addContenido(Fecha fecha, List<OverPanel.Informacion> contenido) {
		
		List<OverPanel.Informacion> acciones = getContenido(fecha);
		if(acciones != null) {
			
			acciones.addAll(contenido);
			
		} else {
			
			contenidoPaneles.put(fecha, contenido);
		}

		PanelDia panel = getPanel(fecha);
		if(panel != null) {
			
			panel.addContenido(contenido);
		}
	}

	public void addContenido(Fecha fecha, OverPanel.Informacion contenido) {
		
		List<OverPanel.Informacion> c = new LinkedList<OverPanel.Informacion>();
		c.add(contenido);
		
		this.addContenido(fecha, c);
	}
	
	public void overrideContenido(Fecha fecha, List<OverPanel.Informacion> contenido) {
		
		List<OverPanel.Informacion> acciones = getContenido(fecha);		
		if(acciones != null) {
			
			acciones.clear();
			acciones.addAll(contenido);
			
		} else {
			
			contenidoPaneles.put(fecha, contenido);
		}

		PanelDia panel = getPanel(fecha);
		if(panel != null) {
			
			panel.clearContenido();
			panel.addContenido(contenido);
		}
	}
	
	// CONTENIDO OVER DE PANELES
	private Map<Fecha, List<OverPanel.Informacion>> contenidoOverPaneles;
	
	public List<OverPanel.Informacion> getContenidoOver(Fecha fecha) {
		
		for (Map.Entry<Fecha, List<OverPanel.Informacion>> entry : contenidoOverPaneles.entrySet()) {
			
			if(entry.getKey().comprobar(fecha)) {
				
				return entry.getValue();
			}
		}
		
		return null;
	}
	
	
	public void addContenidoOver(Fecha fecha, List<OverPanel.Informacion> contenido) {
		
		List<OverPanel.Informacion> acciones = getContenidoOver(fecha);		
		if(acciones != null) {
			
			acciones.addAll(contenido);
			
		} else {
			
			contenidoOverPaneles.put(fecha, contenido);
		}
		
		PanelDia panel = getPanel(fecha);
		if(panel != null) {
			
			panel.addContenidoOver(contenido);
		}
	}

	public void addContenidoOver(Fecha fecha, OverPanel.Informacion contenido) {
		
		List<OverPanel.Informacion> c = new LinkedList<OverPanel.Informacion>();
		c.add(contenido);
		
		this.addContenidoOver(fecha, c);
	}
	
	public void overrideContenidoOver(Fecha fecha, List<OverPanel.Informacion> contenido) {
		
		List<OverPanel.Informacion> acciones = getContenidoOver(fecha);		
		if(acciones != null) {
			
			acciones.clear();
			acciones.addAll(contenido);
			
		} else {

			contenidoOverPaneles.put(fecha, contenido);			
		}

		PanelDia panel = getPanel(fecha);
		if(panel != null) {
			
			panel.clearOver();
			panel.addContenidoOver(contenido);
		}
	}
	
	private void inicializar() {
		
		// tamano
		updateSize();

		// CONFIGURACION
		setBackground(Color.WHITE);
		setLayout(new BorderLayout(DAY_SPACE, 0));
		
		setInnerEmptyBorder(true);
		setEmptyBorderSize(10);

		// IZQUIERDA. SEMANAS
		semanas = new JPanel();
		semanas.setOpaque(false);
		semanas.setLayout(new VerticalLayout());
		add(semanas, BorderLayout.WEST);
		
		// DATOS
		JPanel datos = new JPanel();
		datos.setOpaque(false);
		datos.setLayout(new BorderLayout(0, 5));
		add(datos, BorderLayout.CENTER);
		
		// TITULOS
		panelTitulo = new JPanel();
		panelTitulo.setOpaque(false);
		panelTitulo.setLayout(new BoxLayout(panelTitulo, BoxLayout.Y_AXIS));
		datos.add(panelTitulo, BorderLayout.NORTH);
		
		JPanel panelMes = new BorderPanel(0, Color.BLACK);
		panelMes.setOpaque(false);
		panelMes.setLayout(new BorderLayout(0, 0));
		panelTitulo.add(panelMes);
		
		int butonSize = 16;
		Color butonOver = Color.decode("#C6C6C6");
		
		// BOTON MES ANTERIOR		
		LabelPanel izquierda = new LabelPanel(Constantes.CARPETA_RECURSOS + "modular/izquierda.png", butonSize);
		izquierda.setShape(new LabelPanel.RoundedShape(new Dimension(5, 5)));
		izquierda.setOverColor(butonOver);
		panelMes.add(izquierda, BorderLayout.WEST);

		izquierda.addAccion(new Runnable() {
			
			@Override
			public void run() {
				cambiarMes(-1);
			}
		});
		
		// TITULO
		titulo = new JLabel();
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		titulo.setFont(new Font("Tahoma", Font.BOLD, 14));
		panelMes.add(titulo, BorderLayout.CENTER);

		titulo.setToolTipText("Pulsa para volver al mes actual");
		titulo.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {

				accionMesActual();
			}
		});
		
		// BOTON MES SIGUIENTE
		LabelPanel derecha = new LabelPanel(Constantes.CARPETA_RECURSOS + "modular/derecha.png", butonSize);
		derecha.setShape(new LabelPanel.RoundedShape(new Dimension(5, 5)));
		derecha.setOverColor(butonOver);
		panelMes.add(derecha, BorderLayout.EAST);

		derecha.addAccion(new Runnable() {
			
			@Override
			public void run() {
				cambiarMes(+1);
			}
		});
		
		panelTitulo.add(Box.createVerticalStrut(5));
		
		// CABECERA
		JPanel panelCabecera = new JPanel();
		panelCabecera.setOpaque(false);
		panelCabecera.setLayout(new FlowLayout(FlowLayout.CENTER, DAY_SPACE, 0));
		panelCabecera.setBorder(BorderFactory.createMatteBorder(getBorderSize(), 0, 0, 0, getBorderColor()));
		panelTitulo.add(panelCabecera);
		
		int tamanoBordePanel = 2;
		int tamanoPanel = size / COLUMNS - tamanoBordePanel - DAY_SPACE; // 7 PANELES DE IZQUIERDA A DERECHA
		
		String[] dias = new String[] {"L", "M", "Mi", "J", "V", "S", "D"};
		for(String dia : dias) {

			panelCabecera.add(new PanelCabecera(dia, tamanoPanel, tamanoBordePanel));
		}
		
		// CENTRO. CONTENIDO
		contenido = new JPanel();
		contenido.setOpaque(false);
		contenido.setLayout(new VerticalLayout());
		datos.add(contenido, BorderLayout.CENTER);
		
		addCambioMes(new OnMonthChanged() {
			
			public void change(Fecha firstDay) {
				
				if(markFirstDay) {
					
					PanelDia panel = getPanel(new Fecha());
					if(panel != null) {
						
						panel.setBackgroundColor(ACTUAL_COLOR_DEFAULT);
					}
				}
			}
		});

		// CARGAR MES ACTUAL
		cambiarMes(0);
	}

	protected void cambiarMes(Fecha fecha) {
	
		int anos = fecha.getAno() - actual.getAno();
		int diff = (anos * 12) + fecha.getMes() - actual.getMes(); 
		cambiarMes(diff);
	}
	
	private LabelPanel opciones;
	private Runnable accionOpciones;
	
	protected void cambiarMes(int cambio) {
		
		if(cambio != 0) {
			
			actual.anadir(Calendar.MONTH, cambio);
		}
		
		// LIMPIAR ANTERIOR
		for(PanelDia panel : getPaneles()) {
			
			panel.clearOver();
		}
		
		semanas.removeAll();
				
		contenido.removeAll();		
		paneles.clear();
		
		// CAMBIAR TITULO
		String mes = actual.parse(new Fecha.Formato() {
			
			public SimpleDateFormat getFormato() {

				return new SimpleDateFormat("MMMMM");
			}
			
		}).toUpperCase();
		
		titulo.setText(mes + " DE " + actual.getAno());
		
		// PONER FECHA A PRIMER DIA
		actual.reiniciar();
		
		// CALCULAR tamano PANEL
		int tamanoBordePanel = 1;
		int tamanoPanel = size / COLUMNS - tamanoBordePanel - DAY_SPACE; // 7 PANELES DE IZQUIERDA A DERECHA
		
		List<Fecha[]> fechas = new LinkedList<Fecha[]>();
		Fecha[] semana = new Fecha[7];
		
		boolean inicio = true;
		
		Fecha control = new Fecha(actual);
		while(control.getMes() == actual.getMes()) {

			int dia = control.getDiaSemana(); 
			if(dia == 0 && !inicio) {
				
				fechas.add(semana);
				semana = new Fecha[7];		
			}
			
			semana[dia] = new Fecha(control);
			
			control.anadir(Calendar.DAY_OF_YEAR, 1);
			
			inicio = false;
		}
		
		fechas.add(semana);

		// BOTON OPCIONES
		JPanel auxiliar = new JPanel();
		auxiliar.setOpaque(false);
		auxiliar.setLayout(new BorderLayout(0, 0));
		semanas.add(auxiliar);
		
		opciones = new LabelPanel(Constantes.CARPETA_RECURSOS + "horario/informacion.png", 15);
		opciones.setShape(new LabelPanel.CircleShape());
		opciones.setBackgroundColor(Color.LIGHT_GRAY);
		opciones.setOverColor(Color.decode("#b6c0d4"));
		auxiliar.add(opciones, BorderLayout.WEST);

		opciones.addAccion(new Runnable() {
			
			@Override
			public void run() {
				new Thread(accionOpciones).start();
			}
		});
		
		int margin = 64 - (accionOpciones != null ? 26 : 0);
		opciones.setVisible(accionOpciones != null);
		
		// ANADIR OFFSET A SEMANAS
		semanas.add(Box.createVerticalStrut(margin + (tamanoBordePanel*2))); // 64 es el tamano de la cabecera
		
		// CREAR PANELES		
		for(Fecha[] s : fechas) {
			
			// CONTENIDO
			if(contenido.getComponentCount() > 0) {
				contenido.add(Box.createVerticalStrut(DAY_SPACE));
			}
			
			JPanel panelSemana = new JPanel();
			panelSemana.setOpaque(false);
			panelSemana.setLayout(new FlowLayout(FlowLayout.CENTER, DAY_SPACE, 0));
			contenido.add(panelSemana);
			
			Fecha primera = null;
			
			for(Fecha f : s) {
				
				if(f == null) {
					
					panelSemana.add(Box.createHorizontalStrut(tamanoPanel + tamanoBordePanel));
					
				} else {
					
					if(primera == null)
						primera = f;
					
					PanelDia panel = new PanelDia(f, tamanoPanel, tamanoBordePanel);
					panel.setOverTime(overTime);
					
					// ACCIONES
					panel.addAcciones(acciones);
					
					// CONTENID
					panel.addContenido(getContenido(f));
					panel.addContenido(contenidoComun);

					// CONTENIDO OVER
					panel.addContenidoOver(getContenidoOver(f));
					panel.addContenidoOver(contenidoOver);
					
					paneles.add(panel);
					panelSemana.add(panel);
				}
			}

			// SEMANAS
			if(semanas.getComponentCount() > 0) {
				semanas.add(Box.createVerticalStrut(DAY_SPACE + tamanoBordePanel));
			}
			
			PanelSemana panelSemanaAno = new PanelSemana(primera, tamanoPanel);
			semanas.add(panelSemanaAno);
		}
		
		// ACTUALIZAR
		semanas.revalidate();
		semanas.repaint();
		
		contenido.revalidate();
		contenido.repaint();
		
		// ACCIONES DE CAMBIO
		for(OnMonthChanged accion : accionesCambio) {			
			accion.change(actual);
		}
	}
	
	public void accionMesActual() {

		actual = new Fecha();
		reiniciar();
	}
	
	public void reiniciar() {
		
		cambiarMes(0);
		if(onResetData != null) {
			
			onResetData.reset();
		}
	}
	
	public interface OnMonthChanged {
		
		public void change(Fecha firstDay);
	}

	public interface OnResetData {
		
		public void reset();
	}
	
	public void activarOpciones(Runnable accion) {
		this.accionOpciones = accion;
		cambiarMes(0);
	}
}
package com.repex.librerias.components.utilidades;

public interface OnDestroyPanel {

	public void onDestroy();
}
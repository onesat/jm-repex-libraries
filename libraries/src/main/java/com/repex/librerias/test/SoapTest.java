package com.repex.librerias.test;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.xml.sax.SAXException;

import com.repex.librerias.soap.SOAPControler;
import com.repex.librerias.soap.SOAPEnvironment;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;
import com.repex.librerias.soap.utilidades.SOAPResponseEnvelopment;
import com.repex.librerias.soap.utilidades.SOAPResponseList;
import com.repex.librerias.soap.utilidades.SOAPResponseMap;
import com.repex.librerias.soap.utilidades.SOAPUser;
import com.repex.librerias.soap.utilidades.SoapFormatter;

public class SoapTest {

	public static void main(String[] args) {

		// DATOS DE SOAP
    	SOAPControler.getSingleton().setSoapUser(new SOAPUser("juanmi", "juanmi"));

    	List<SOAPEnvironment.Header> headers = new LinkedList<SOAPEnvironment.Header>();

        headers.add(new SOAPEnvironment.Header("ns1","http://repexgroup.eu/apps/onesat-app/admin/"));
        headers.add(new SOAPEnvironment.Header("SOAP-ENC","http://schemas.xmlsoap.org/soap/encoding/"));
        headers.add(new SOAPEnvironment.Header("xsi","http://www.w3.org/2001/XMLSchema-instance"));
        headers.add(new SOAPEnvironment.Header("tns","urn:mi_ws1"));
        headers.add(new SOAPEnvironment.Header("wsdl","http://schemas.xmlsoap.org/wsdl/"));
        headers.add(new SOAPEnvironment.Header("xsd","http://www.w3.org/2001/XMLSchema"));
        
        SOAPControler.getSingleton().addCommonHeaders(headers);
        
        SOAPControler.getSingleton().addCommonNode(new SOAPEnvironment.Nodo("database", "xsd:string", "repexgro_onesat_admin_pre"));
        
		// END POINT
    	SOAPControler.getSingleton().setEndPoint("http://repexgroup.eu/apps/onesat-app/axpre/server/wsdl/wsdl_clientes.php?wsdl");
    	
    	// FORMATTER
    	/*
    	SoapFormatter formatter = new SoapFormatter() {
			
			@Override
			public String toFormat(String texto) {
				return texto.replaceAll("á", "\\$a");
			}
			
			@Override
			public String fromFormat(String texto) {
				return texto.replaceAll("\\$a", "á");
			}
		};
		
		SOAPControler.getSingleton().addFormatter(formatter);
		*/
    	
    	// recuperar();
		crearCategoria();
		// modificar();
	}
	
	private static void recuperar() {
		
		// FUNCION
		String FUNCION = "recuperar_profesionales";
    	
    	// RESULTADO
		List<SOAPEnvironment.Header> funcionHeaders = new LinkedList<SOAPEnvironment.Header>();
		funcionHeaders.add(new SOAPEnvironment.Header("SOAP-ENV:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/"));

		SOAPEnvironment.Funcion funcion = new SOAPEnvironment.Funcion(FUNCION, "ns1", funcionHeaders);

		// NODOS
		List<SOAPEnvironment.Nodo> nodos = new LinkedList<SOAPEnvironment.Nodo>();		
		nodos.add(new SOAPEnvironment.Nodo("DISTRIBUCION", "xsd:int", 1));
		
		// LISTAS
		List<SOAPEnvironment.Lista> listas = new LinkedList<SOAPEnvironment.Lista>();
		
		// PARAMETROS
		List<SOAPEnvironment.Parametro> parametros = new LinkedList<SOAPEnvironment.Parametro>();
		parametros.add(new SOAPEnvironment.Parametro("busqueda", "tns:BusquedaProfesionales", nodos, listas));

		SOAPEnvironment soapEnvironment = new SOAPEnvironment(funcion, parametros, null);

		try {
			
			SOAPMessage soapResponse = SOAPControler.getSingleton().callSoapWebService(FUNCION, soapEnvironment);

			try {

				SOAPResponseEnvelopment lastResponse = new SOAPResponseEnvelopment(soapResponse);
				if(lastResponse.isError() == null) {
					
					SOAPResponseList response = lastResponse.getMultiData();
					for(SOAPResponseMap map : response.getDatos()) {

						String id = map.getAtributte("ID_PROFESIONAL").getDato();
						String nombre = map.getAtributte("NICK").getDato();
						
						System.out.println(id + "  -  " + nombre);
					}
				}
				
			} catch (IOException e) {			
				e.printStackTrace();
			} catch (ParserConfigurationException e) {			
				e.printStackTrace();			
			} catch (SAXException e) {			
				e.printStackTrace();
			}
		} catch (SOAPException e1) {
			e1.printStackTrace();
		} catch (RequiredAuthenticationException e1) {
			e1.printStackTrace();
		}
	}
	
	private static void crearProfesional() {

		// FUNCION
		String FUNCION = "crear_profesional";

		List<SOAPEnvironment.Header> funcionHeaders = new LinkedList<SOAPEnvironment.Header>();
		funcionHeaders.add(new SOAPEnvironment.Header("SOAP-ENV:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/"));

		SOAPEnvironment.Funcion funcion = new SOAPEnvironment.Funcion(FUNCION, "ns1", funcionHeaders);

		// NODOS
		List<SOAPEnvironment.Nodo> nodos = new LinkedList<SOAPEnvironment.Nodo>();
		nodos.add(new SOAPEnvironment.Nodo("DISTRIBUCION", "xsd:int", 1));
		
		nodos.add(new SOAPEnvironment.Nodo("USUARIO_ALTA", "xsd:int", 1));
		nodos.add(new SOAPEnvironment.Nodo("ESTADO", "xsd:int", 1));

		nodos.add(new SOAPEnvironment.Nodo("NICK", "xsd:string", "Pruebá"));
		nodos.add(new SOAPEnvironment.Nodo("NOMBRE", "xsd:string", "PRUEBA NUEVA"));
		
		nodos.add(new SOAPEnvironment.Nodo("CALLE", "xsd:string", ""));
		nodos.add(new SOAPEnvironment.Nodo("NUMERO", "xsd:string", ""));
		nodos.add(new SOAPEnvironment.Nodo("PISO", "xsd:string", ""));
		
		nodos.add(new SOAPEnvironment.Nodo("CIF", "xsd:string", ""));

		nodos.add(new SOAPEnvironment.Nodo("COMENTARIO", "xsd:string", ""));
		
		// LISTAS
		List<SOAPEnvironment.Lista> listas = new LinkedList<SOAPEnvironment.Lista>();

		List<SOAPEnvironment.Parametro> metodos = new LinkedList<SOAPEnvironment.Parametro>();
		metodos.add(new SOAPEnvironment.Parametro("item", "xsd:int", 1));

		listas.add(new SOAPEnvironment.Lista("METODOS_ENVIO", "xsd:int", metodos));
		
		// PARAMETROS
		List<SOAPEnvironment.Parametro> parametros = new LinkedList<SOAPEnvironment.Parametro>();
		parametros.add(new SOAPEnvironment.Parametro("profesional", "tns:Profesional", nodos, listas));

		SOAPEnvironment soapEnvironment = new SOAPEnvironment(funcion, parametros, null);
		
		// PARSE RESPONSE
		try {
			
			SOAPMessage soapResponse = SOAPControler.getSingleton().callSoapWebService(FUNCION, soapEnvironment);

			try {

				SOAPResponseEnvelopment lastResponse = new SOAPResponseEnvelopment(soapResponse);
				if(lastResponse.isError() != null) {

					throw new SOAPException(lastResponse.getError());
				}

				SOAPResponseMap response = lastResponse.getData();
				String res = response.getAtributte("ID_PROFESIONAL").getDato();
				System.out.println(res);
				
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			}
			
		} catch (SOAPException e1) {
			e1.printStackTrace();
		} catch (RequiredAuthenticationException e1) {
			e1.printStackTrace();
		}
	}
	
	private static void crearCategoria() {

		// FUNCION
		String FUNCION = "crear_categoria";

		List<SOAPEnvironment.Header> funcionHeaders = new LinkedList<SOAPEnvironment.Header>();
		funcionHeaders.add(new SOAPEnvironment.Header("SOAP-ENV:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/"));

		SOAPEnvironment.Funcion funcion = new SOAPEnvironment.Funcion(FUNCION, "ns1", funcionHeaders);

		// NODOS
		List<SOAPEnvironment.Nodo> nodos = new LinkedList<SOAPEnvironment.Nodo>();
		nodos.add(new SOAPEnvironment.Nodo("DISTRIBUCION", "xsd:int", 1));
		
		nodos.add(new SOAPEnvironment.Nodo("PADRE", "xsd:int", 1));
		nodos.add(new SOAPEnvironment.Nodo("CATEGORIA", "xsd:string", "Camión"));
		
		// PARAMETROS
		List<SOAPEnvironment.Parametro> parametros = new LinkedList<SOAPEnvironment.Parametro>();
		parametros.add(new SOAPEnvironment.Parametro("categoria", "tns:Categoria", nodos));

		SOAPEnvironment soapEnvironment = new SOAPEnvironment(funcion, parametros, null);
		
		// PARSE RESPONSE
		try {
			
			SOAPMessage soapResponse = SOAPControler.getSingleton().callSoapWebService(FUNCION, soapEnvironment);

			try {

				SOAPResponseEnvelopment lastResponse = new SOAPResponseEnvelopment(soapResponse);
				if(lastResponse.isError() != null) {

					throw new SOAPException(lastResponse.getError());
				}

				SOAPResponseMap response = lastResponse.getData();
				String res = response.getAtributte("ID_CATEGORIA").getDato();
				System.out.println(res);
				
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			}
			
		} catch (SOAPException e1) {
			e1.printStackTrace();
		} catch (RequiredAuthenticationException e1) {
			e1.printStackTrace();
		}	
	}
	
	private static void modificar() {

		// FUNCION
		String FUNCION = "modificar_averia";

		List<SOAPEnvironment.Header> funcionHeaders = new LinkedList<SOAPEnvironment.Header>();
		funcionHeaders.add(new SOAPEnvironment.Header("SOAP-ENV:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/"));

		SOAPEnvironment.Funcion funcion = new SOAPEnvironment.Funcion(FUNCION, "ns1", funcionHeaders);

		// NODOS
		List<SOAPEnvironment.Nodo> nodos = new LinkedList<SOAPEnvironment.Nodo>();
		nodos.add(new SOAPEnvironment.Nodo("ID_AVERIA", "xsd:int", 56));
		
		nodos.add(new SOAPEnvironment.Nodo("AVERIA", "xsd:string", "SE HA ROTO COSA 12 a"));
		nodos.add(new SOAPEnvironment.Nodo("ESTADO", "xsd:int", 1));
		nodos.add(new SOAPEnvironment.Nodo("CALLE", "xsd:string", "PRUEBAS"));
		nodos.add(new SOAPEnvironment.Nodo("NUMERO", "xsd:string", ""));
		nodos.add(new SOAPEnvironment.Nodo("PISO", "xsd:string", ""));
		nodos.add(new SOAPEnvironment.Nodo("COMENTARIO", "xsd:string", ""));
		
		nodos.add(new SOAPEnvironment.Nodo("REFERENCIA_ORIGEN", "xsd:string", "1"));

		List<SOAPEnvironment.Parametro> parametrosAuxiliares = new LinkedList<SOAPEnvironment.Parametro>();
		
		// CIUDAD, ES UN PARAMETRO
		{			
			List<SOAPEnvironment.Nodo> nodosAux = new LinkedList<SOAPEnvironment.Nodo>();
			nodosAux.add(new SOAPEnvironment.Nodo("ID_CIUDAD", "xsd:int", 61));
			
			parametrosAuxiliares.add(new SOAPEnvironment.Parametro("CIUDAD", "tns:Ciudad", nodosAux));
		}

		// CODIGO POSTAL, ES UN PARAMETRO
		{			
			List<SOAPEnvironment.Nodo> nodosAux = new LinkedList<SOAPEnvironment.Nodo>();
			nodosAux.add(new SOAPEnvironment.Nodo("ID_CODIGO_POSTAL", "xsd:int", 32965));
			
			parametrosAuxiliares.add(new SOAPEnvironment.Parametro("CODIGO_POSTAL", "tns:CodigoPostal", nodosAux));
		}

		// PROFESIONAL, ES UN PARAMETRO
		{			
			List<SOAPEnvironment.Nodo> nodosAux = new LinkedList<SOAPEnvironment.Nodo>();
			nodosAux.add(new SOAPEnvironment.Nodo("ID_PROFESIONAL", "xsd:int", 3));
			
			parametrosAuxiliares.add(new SOAPEnvironment.Parametro("PROFESIONAL", "tns:Profesional", nodosAux));
		}
		
		// LISTAS
		List<SOAPEnvironment.Lista> listas = new LinkedList<SOAPEnvironment.Lista>();
		{
			List<SOAPEnvironment.Parametro> categorias = new LinkedList<SOAPEnvironment.Parametro>();

			List<SOAPEnvironment.Nodo> nodo = new LinkedList<SOAPEnvironment.Nodo>();			
			nodo.add(new SOAPEnvironment.Nodo("ID_CATEGORIA", "xsd:int", 8));

			categorias.add(new SOAPEnvironment.Parametro("item", "tns:Categoria", nodo));

			listas.add(new SOAPEnvironment.Lista("CATEGORIAS", "tns:Categoria", categorias));
		}
		
		// PARAMETROS
		SOAPEnvironment.Parametro parametro = new SOAPEnvironment.Parametro("averia", "tns:Averia");
		parametro.setNodos(nodos);
		parametro.setListas(listas);
		parametro.setParametros(parametrosAuxiliares);

		List<SOAPEnvironment.Parametro> parametros = new LinkedList<SOAPEnvironment.Parametro>();
		parametros.add(parametro);

		// OTROS DATOS
		List<SOAPEnvironment.Nodo> otros = new LinkedList<SOAPEnvironment.Nodo>();
		otros.add(new SOAPEnvironment.Nodo("usuario", "xsd:int", 1));
		
		SOAPEnvironment soapEnvironment = new SOAPEnvironment(funcion, parametros, otros);
		
		// PARSE RESPONSE
		try {
			
			SOAPMessage soapResponse = SOAPControler.getSingleton().callSoapWebService(FUNCION, soapEnvironment);

			try {

				SOAPResponseEnvelopment lastResponse = new SOAPResponseEnvelopment(soapResponse);
				if(lastResponse.isError() != null) {

					throw new SOAPException(lastResponse.getError());
				}

				SOAPResponseMap response = lastResponse.getData();
				Boolean res = Boolean.parseBoolean(response.getAtributte("BOOLEAN").getDato());
				System.out.println(res);
				
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			}
			
		} catch (SOAPException e1) {
			e1.printStackTrace();
		} catch (RequiredAuthenticationException e1) {
			e1.printStackTrace();
		}
	
	}
}
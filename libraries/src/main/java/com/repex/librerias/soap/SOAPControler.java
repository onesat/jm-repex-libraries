package com.repex.librerias.soap;

import java.util.Base64;
import java.util.LinkedList;
import java.util.List;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.repex.librerias.date.Fecha;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;
import com.repex.librerias.soap.utilidades.SOAPUser;
import com.repex.librerias.soap.utilidades.SoapFormatter;

/**
 * Realiza las comunicaciones con el servidor que se le ha asignado
 * @author JM
 */
public class SOAPControler {
	
	private SOAPControler() {
		
		formatters = new LinkedList<SoapFormatter>();
		
		commonHeaders = new LinkedList<SOAPEnvironment.Header>();
		commonNodes = new LinkedList<SOAPEnvironment.Nodo>();
	}

	private static SOAPControler singleton;
	
	public static SOAPControler getSingleton() {

		if(singleton == null) {
			
			singleton = new SOAPControler();
		}
		
		return singleton;
	}
	
	// DEVUELVE UNA COPIA DEL ORIGINAL
	public SOAPControler state() {
		
		SOAPControler state = new SOAPControler();
		
		state.setEndPoint(endPoint);
		state.setSoapUser(soapUser);
		
		state.addCommonHeaders(commonHeaders);
		state.addCommonNodes(commonNodes);
		
		for(SoapFormatter formatter : getFormatters()) {
			state.addFormatter(formatter);
		}
		
		return state;
	}
	
	/**
	 * Return this to the parameter state
	 * @param state
	 */
	public static void change(SOAPControler state) {		
		singleton = state;
	}
	
	/**
	 * 
	 */
	private SOAPUser soapUser;
	
	public SOAPUser getSoapUser() {		
		return soapUser;
	}
	
	public void setSoapUser(SOAPUser soapUser) {		
		this.soapUser = soapUser;
	}
	
	/**
	 * 
	 */
	private String endPoint;
	
	public String getEndPoint() {		
		return endPoint;
	}
	
	public void setEndPoint(String endPoint) {		
		this.endPoint = endPoint;
	}
	
	private List<SoapFormatter> formatters;
	
	public void addFormatter(SoapFormatter formatter) {
		this.formatters.add(formatter);
	}
	
	public List<SoapFormatter> getFormatters() {
		return formatters;
	}
	
	/**
	 * 
	 */
	private List<SOAPEnvironment.Header> commonHeaders;
	
	public void addCommonHeader(SOAPEnvironment.Header header) {		
		if(!this.commonHeaders.contains(header))
			this.commonHeaders.add(header);
	}

	public void addCommonHeaders(List<SOAPEnvironment.Header> headers) {		
		for(SOAPEnvironment.Header header : headers)
			addCommonHeader(header);
	}
	
	public void clearCommonHeaders() {	
		this.commonHeaders.clear();
	}
	
	/**
	 * 
	 */
	private List<SOAPEnvironment.Nodo> commonNodes;
	
	public void addCommonNode(SOAPEnvironment.Nodo node) {		
		if(!this.commonNodes.contains(node))
			commonNodes.add(node);
	}
	
	public void addCommonNodes(List<SOAPEnvironment.Nodo> nodes) {		
		for(SOAPEnvironment.Nodo node : nodes)
			addCommonNode(node);
	}
	
	public void clearCommonNodes() {	
		this.commonNodes.clear();
	}
	
	/**
	 * Remove all data
	 */
	public void clear() {
		
		// CLEAR USER
		soapUser = null;
		
		// CLEAR END POINT
		endPoint = "";
		
		// CLEAR COMMON DATA
		clearCommonHeaders();
		clearCommonNodes();
	}
	
	/**
	 * 
	 * @param soapAction
	 * @param soapEnvironment
	 * @return
	 * @throws SOAPException
	 */
	public SOAPMessage callSoapWebService(String soapAction, SOAPEnvironment soapEnvironment) throws SOAPException, RequiredAuthenticationException {

		// Create SOAP Connection
		SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
		SOAPConnection soapConnection = soapConnectionFactory.createConnection();

		// Create SOAP request
	    MessageFactory messageFactory = MessageFactory.newInstance();
	    SOAPMessage soapMessage = messageFactory.createMessage();
	    
	    // COMMON HEADERS
	    for(SOAPEnvironment.Header header : commonHeaders) {	    	
	    	header.complete(soapMessage);
	    }
	    
	    // COMMON NODES
	    soapEnvironment.addNodos(commonNodes);
	    
	    // CONSTRUIR
		SOAPMessage soapRequest = soapEnvironment.construct(soapMessage, formatters);
		if(soapRequest == null) {			
			return null;
		}
		
		// MIME HEADERS
		action(soapRequest, soapAction);
		autentication(soapRequest);

		soapRequest.saveChanges();

		// TODO: ANADIR USUARIO AL LOG
		
		// TODO: LOG
		print(new Fecha(), "SOAP Request Message:", soapRequest);
		
		SOAPMessage soapResponse = null;
		try {

			// Recover SOAP Response
			soapResponse = soapConnection.call(soapRequest, endPoint);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RequiredAuthenticationException("Failed authentication");
		}

		// Close Connection
		soapConnection.close();

		// TODO: LOG
		print(new Fecha(), "SOAP Response Message:", soapResponse);
        
		// RETURN
		return soapResponse;
	}
	
	private void print(Fecha fecha, String title, SOAPMessage message) {

		try {

	        System.out.println(fecha.parse() + " - " + title);
	        message.writeTo(System.out);
	        System.out.println();
	        System.out.println();
	        
		} catch (Exception e) {}
	}
	
	private void action(SOAPMessage soapMessage, String soapAction) throws SOAPException {

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", soapAction);
	}
	
	private void autentication(SOAPMessage soapMessage) throws RequiredAuthenticationException {

		if(soapUser == null) {
			
			throw new RequiredAuthenticationException("U need to authenticate");
		}
		
        MimeHeaders headers = soapMessage.getMimeHeaders();
        
        String authorization = new String(Base64.getEncoder().encode((soapUser.toString()).getBytes()));
        headers.addHeader("Authorization", "Basic " + authorization);
	}
}
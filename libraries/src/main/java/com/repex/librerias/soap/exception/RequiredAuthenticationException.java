package com.repex.librerias.soap.exception;

public class RequiredAuthenticationException extends Exception {

	private static final long serialVersionUID = 1L;

    public RequiredAuthenticationException() {}

    public RequiredAuthenticationException(String message) {
    	
        super(message);
    }

    public RequiredAuthenticationException(Throwable cause) {
    	
        super(cause);
    }

    public RequiredAuthenticationException(String message, Throwable cause) {
    	
        super(message, cause);
    }
}
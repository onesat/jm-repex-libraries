package com.repex.librerias.soap.utilidades;

public interface SoapFormatter {

	/**
	 * Convierte los datos para el WS
	 * @param texto
	 * @return
	 */
	public String toFormat(String texto);
	
	/**
	 * Convierte los datos desde el WS
	 * @param texto
	 * @return
	 */
	public String fromFormat(String texto);
}
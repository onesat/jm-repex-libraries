package com.repex.librerias.soap.utilidades;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.repex.librerias.soap.SOAPControler;

public class SOAPResponseMap {
	
	private Map<String, SOAPResponseData> datos;
	
	public SOAPResponseMap(NodeList lista, Boolean format) {
		
		datos = new HashMap<String, SOAPResponseData>();
				
		if(lista != null && lista.getLength() > 0) {

			for(int i=0; i<lista.getLength(); i++) {
				
				Node node = lista.item(i);
				
				String key = node.getNodeName();

				String tipo = null;
				try {
					tipo = node.getAttributes().getNamedItem("xsi:type").getFirstChild().getNodeValue();				
				} catch (Exception e) {}

				if(tipo == null) {
					
					// ES UN DATO
					String data = node.getNodeValue();
					datos.put("VALUE", new SOAPResponseDataString(data, format));
					
				} else if(tipo.equals("SOAP-ENC:Array")) {
					
					datos.put(key, new SOAPResponseDataList(node.getChildNodes(), format));
					
				} else if(tipo.startsWith("tns")) {
					
					datos.put(key, new SOAPResponseDataMap(node.getChildNodes(), format));
					
				} else if(tipo.startsWith("xsd")) {

					String data = null;
					if(node.getChildNodes().getLength() > 0) {

						data = node.getFirstChild().getNodeValue();
					}

					datos.put(key, new SOAPResponseDataString(data, format));
				}				
			}
		}
	}
	
	public SOAPResponseMap(Element element, Boolean format) {				
		this(element.getChildNodes(), format);
	}
	
	public SOAPResponseDataString getAtributte(String key) {		
		return (SOAPResponseDataString) datos.get(key);
	}
	
	public SOAPResponseDataMap getMap(String key) {		
		return (SOAPResponseDataMap) datos.get(key);
	}
	
	public SOAPResponseDataList getList(String key) {		
		return (SOAPResponseDataList) datos.get(key);
	}
	
	public SOAPResponseDataString getValue() {
		return getAtributte("VALUE");
	}
	
	public interface SOAPResponseData {}
	
	public static class SOAPResponseDataList implements SOAPResponseData {
		
		private List<SOAPResponseMap> datos;

		public SOAPResponseDataList(NodeList nodes, Boolean format) {
			
			datos = new LinkedList<SOAPResponseMap>();
			
			if(nodes != null && nodes.getLength() > 0) {
				
				for(int i = 0 ; i < nodes.getLength();i++) {
					// DATOS
					datos.add(new SOAPResponseMap((Element) nodes.item(i), format));
				}
			}
		}
		
		public List<SOAPResponseMap> getDatos() {
			return datos;
		}
	}
	
	public static class SOAPResponseDataMap implements SOAPResponseData {
		
		private SOAPResponseMap datos;

		public SOAPResponseDataMap(NodeList nodes, Boolean format) {			
			datos = new SOAPResponseMap(nodes, format);
		}
		
		public SOAPResponseMap getDatos() {
			return datos;
		}
	}
	
	public static class SOAPResponseDataString implements SOAPResponseData {

		private String dato;
		private Boolean format;

		public SOAPResponseDataString(String dato, Boolean format) {				
			this.dato = dato;
			this.format = format;			
		}
		
		public String getDato() {        	
			return getDato(format);
		}

		/**
		 * Devuelve el dato con o sin formato
		 * @param format
		 * @return
		 */
		public String getDato(boolean format) {
			
			if(dato == null)
				return dato;
			
			String resultado = dato;
			
			if(format) {
				for(SoapFormatter formatter : SOAPControler.getSingleton().getFormatters()) {
					resultado = formatter.fromFormat(resultado);
				}
			}
			
			return resultado;
		}
		
		@Override
		public String toString() {
			
			if(dato == null)
				return "";
			
			return getDato();
		}
	}
}
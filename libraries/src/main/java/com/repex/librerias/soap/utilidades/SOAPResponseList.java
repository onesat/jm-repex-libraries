package com.repex.librerias.soap.utilidades;

import java.util.LinkedList;
import java.util.List;

import org.w3c.dom.Element;

public class SOAPResponseList {

	private List<SOAPResponseMap> datos;
	
	public SOAPResponseList(List<Element> elements, Boolean format) {
		
		datos = new LinkedList<SOAPResponseMap>();
		
		for(Element element : elements) {
			
			datos.add(new SOAPResponseMap(element, format));
		}
	}
	
	public List<SOAPResponseMap> getDatos() {
		
		return datos;
	}
}
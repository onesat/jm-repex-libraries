package com.repex.librerias.soap.utilidades;

public class SOAPUser {

	private String nombre;
	private String contrasena;
	
	public SOAPUser(String nombre, String contrasena) {
		
		this.nombre = nombre;
		this.contrasena = contrasena;
	}
	
	public String getNombre() {
		
		return nombre;
	}
	
	public String getContrasena() {
		
		return contrasena;
	}
	
	@Override
	public String toString() {
		
		return getNombre() + ":" + getContrasena();
	}
}
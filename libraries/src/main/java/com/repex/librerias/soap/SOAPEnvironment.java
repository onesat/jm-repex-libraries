package com.repex.librerias.soap;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import com.repex.librerias.soap.utilidades.SoapFormatter;

public class SOAPEnvironment {
	
	private List<Header> headers;
	
	private Funcion funcion;
	
	private List<Parametro> parametros;
	private List<Lista> listas;
	private List<Nodo> nodos;
	
	private Boolean format = true; // FORMATEAR AL ENVIAR
	
	public Boolean getFormat() {
		return format;
	}
	
	public void setFormat(Boolean format) {
		this.format = format;
	}
	
	public SOAPEnvironment(List<Header> headers, Funcion funcion, List<Parametro> parametros, List<Lista> listas, List<Nodo> nodos) {
		
		this.headers = headers;
		
		this.funcion = funcion;
		
		this.parametros = parametros;
		this.listas = listas;
		this.nodos = nodos;
	}

	public SOAPEnvironment(List<Header> headers, Funcion funcion, List<Parametro> parametros, List<Nodo> nodos) {

		this(headers, funcion, parametros, new LinkedList<Lista>(), nodos);
	}
	
	public SOAPEnvironment(List<Header> headers, List<Parametro> parametros, List<Nodo> nodos) {
		
		this(headers, null, parametros, nodos);
	}
	
	public SOAPEnvironment(Funcion funcion, List<Parametro> parametros, List<Nodo> nodos) {
		
		this(new LinkedList<Header>(), funcion, parametros, nodos);
	}
	
	public SOAPEnvironment(Funcion funcion, List<Parametro> parametros, List<Lista> listas, List<Nodo> nodos) {
		
		this(new LinkedList<Header>(), funcion, parametros, listas, nodos);
	}
	
	public SOAPEnvironment(Funcion funcion, List<Nodo> nodos) {
		
		this(new LinkedList<Header>(), funcion, new LinkedList<Parametro>(), nodos);
	}
	
	public void setFuncion(Funcion funcion) {
		
		this.funcion = funcion;
	}

	public void addLista(Lista lista) {

		if(this.listas == null) {
			
			this.listas = new LinkedList<Lista>();
		}
		
		this.listas.add(lista);
	}

	public void addLista(List<Lista> listas) {
		
		if(this.listas == null) {
			
			this.listas = new LinkedList<Lista>();
		}
		
		this.listas.addAll(listas);
	}

	public void addParametro(Parametro parametro) {

		if(this.parametros == null) {
			
			this.parametros = new LinkedList<Parametro>();
		}
		
		this.parametros.add(parametro);
	}

	public void addParametros(List<Parametro> parametros) {
		
		if(this.parametros == null) {
			
			this.parametros = new LinkedList<Parametro>();
		}
		
		this.parametros.addAll(parametros);
	}
	
	public void addNodo(Nodo nodo) {

		if(this.nodos == null) {
			
			this.nodos = new LinkedList<Nodo>();
		}
		
		this.nodos.add(nodo);
	}

	public void addNodos(List<Nodo> nodos) {
		
		if(this.nodos == null) {
			
			this.nodos = new LinkedList<Nodo>();
		}
		
		this.nodos.addAll(nodos);
	}
	
    /**
     * Construct a new message complete
     * @return
     * @throws SOAPException
     */
	public SOAPMessage construct(List<SoapFormatter> formatters) throws SOAPException {

	    MessageFactory messageFactory = MessageFactory.newInstance();
	    SOAPMessage soapMessage = messageFactory.createMessage();
	    
	    return construct(soapMessage, formatters);
	}
	
	/**
	 * Construct a message over another message
	 * @param soapMessage
	 * @return
	 * @throws SOAPException
	 */
	public SOAPMessage construct(SOAPMessage soapMessage, List<SoapFormatter> formatters) throws SOAPException {

		if(funcion == null || (parametros.isEmpty() && nodos.isEmpty())) {			
			return null;
		}
		        
        // HEADERS
		if(headers != null) {			
			for(Header header : headers) {
				header.complete(soapMessage);
			}
		}
        
        // FUNCION
        SOAPElement funcion = this.funcion.complete(soapMessage);
        
        // PARAMETROS
        if(parametros != null) {        	
        	for(Parametro parametro : parametros) {
        		parametro.complete(soapMessage, funcion, format ? formatters : new ArrayList<SoapFormatter>());
        	}
        }
        
        // LISTAS
        if(listas != null) {        	
        	for(Lista lista : listas) {        		
        		lista.complete(soapMessage, funcion, format ? formatters : new ArrayList<SoapFormatter>());
        	}
        }
        
        // NODOS
        if(nodos != null) {        	
        	for(Nodo nodo : nodos) {
        		nodo.complete(funcion, format ? formatters : new ArrayList<SoapFormatter>());
        	}
        }
        
        return soapMessage;
	}
	
	public static class Lista {
		
		private String nombre;
		private String tipo;
		
		private List<Parametro> parametros;

		public Lista(String nombre, String tipo) {
			
			this(nombre, tipo, new LinkedList<Parametro>());
		}
		
		public Lista(String nombre, String tipo, List<Parametro> parametros) {
			
			this.nombre = nombre;
			this.tipo = tipo;
			
			this.parametros = parametros;
		}

		public void setNombre(String nombre) {
			
			this.nombre = nombre;
		}

		public void setTipo(String tipo) {
			
			this.tipo = tipo;
		}

		public void setParametros(List<Parametro> parametros) {
			
			this.parametros = parametros;
		}
		
		public void complete(SOAPMessage soapMessage, SOAPElement soapElement, List<SoapFormatter> formatters) throws SOAPException {

	        SOAPPart soapPart = soapMessage.getSOAPPart();
	        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
	        
	        SOAPElement lista = soapElement.addChildElement(soapEnvelope.createName(nombre));
	        lista.addAttribute(soapEnvelope.createName("SOAP-ENC:arrayType"), tipo + "[" + parametros.size() + "]");
	        lista.addAttribute(soapEnvelope.createName("xsi:type"), "SOAP-ENC:Array");
	        
	        for(Parametro parametro : this.parametros) {
	        	
	        	parametro.complete(soapMessage, lista, formatters);
	        }
		}
	}
	
	public static class Parametro {
	
		private String nombre;
		private String tipo;
		
		private String value;
		
		private List<Nodo> nodos;
		private List<Parametro> parametros;
		private List<Lista> listas;

		public Parametro(String nombre, String tipo) {

			this(nombre, tipo, new LinkedList<Nodo>());
		}
		
		public Parametro(String nombre, String tipo, List<Nodo> nodos) {
			
			this(nombre, tipo, nodos, new LinkedList<Parametro>(), new LinkedList<Lista>());
		}
		
		public Parametro(String nombre, String tipo, List<Nodo> nodos, List<Lista> listas) {
			
			this(nombre, tipo, nodos, new LinkedList<Parametro>(), listas);
		}
		
		public Parametro(String nombre, String tipo, List<Nodo> nodos, List<Parametro> parametros, List<Lista> listas) {
			
			this.nombre = nombre;
			this.tipo = tipo;
			
			this.nodos = nodos;
			this.parametros = parametros;
			this.listas = listas;
		}

		public Parametro(String nombre, String tipo, String value) {

			this(nombre, tipo, new LinkedList<Nodo>());
			this.value = value;
		}
		
		public Parametro(String nombre, String tipo, int value) {

			this(nombre, tipo, Integer.toString(value));
		}

		public void setNombre(String nombre) {
			
			this.nombre = nombre;
		}

		public void setTipo(String tipo) {
			
			this.tipo = tipo;
		}

		public void setNodos(List<Nodo> nodos) {
			
			this.nodos = nodos;
		}

		public void setParametros(List<Parametro> parametros) {
			
			this.parametros = parametros;
		}
		
		public void setListas(List<Lista> listas) {
			
			this.listas = listas;
		}
		
		public void setValue(String value) {
			this.value = value;
		}
		
		public void complete(SOAPMessage soapMessage, SOAPElement soapElement, List<SoapFormatter> formatters) throws SOAPException {

	        SOAPPart soapPart = soapMessage.getSOAPPart();
	        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
	        
	        SOAPElement p = soapElement.addChildElement(soapEnvelope.createName(nombre));        
	        p.addAttribute(soapEnvelope.createName("xsi:type"), tipo);
	        
	        if(value != null && !value.equals("")) {
	        	
	        	// FORMAT TO WS
	        	String resultado = value;
	        	for(SoapFormatter format : formatters) {
	        		resultado = format.toFormat(resultado);
	        	}
	        	
	        	p.addTextNode(resultado);
	        	
	        } else {

	        	for(Nodo nodo : this.nodos) {

	        		nodo.complete(p, formatters);
	        	}

	        	for(Parametro parametro : this.parametros) {

	        		parametro.complete(soapMessage, p, formatters);
	        	}

	        	for(Lista lista : listas) {

	        		lista.complete(soapMessage, p, formatters);
	        	}
	        }
		}
	}
	
	public static class Nodo {
		
		private String nombre;
		private String tipo;
		
		private String valor;

		public Nodo(String nombre, String tipo, String valor) {
			
			this.nombre = nombre;
			this.tipo = tipo;
			
			this.valor = valor;
		}

		public Nodo(String nombre, String tipo, int valor) {
			
			this(nombre, tipo, Integer.toString(valor));
		}

		public Nodo(String nombre, String tipo, long valor) {
			
			this(nombre, tipo, Long.toString(valor));
		}

		public Nodo(String nombre, String tipo, double valor) {
			
			this(nombre, tipo, Double.toString(valor));
		}

		public Nodo(String nombre, String tipo, boolean valor) {
			
			this(nombre, tipo, Boolean.toString(valor));
		}
		
		public String getNombre() {
			
			return nombre;
		}

		public String getTipo() {
			
			return tipo;
		}

		public String getValor() {
			
			return valor;
		}

		public void complete(SOAPElement soapElement, List<SoapFormatter> formatters) throws SOAPException {

			if(getNombre() != null && getTipo() != null && getValor() != null) {

	        	// FORMAT TO WS
	        	String resultado = valor;
	        	for(SoapFormatter format : formatters) {
	        		resultado = format.toFormat(resultado);
	        	}
	        	
				SOAPFactory soapFactory = SOAPFactory.newInstance();

				SOAPElement a1 = soapFactory.createElement(nombre);
				a1.addTextNode(resultado).setAttribute("xsi:type", tipo);

				soapElement.addChildElement(a1);
			}
		}

		@Override
		public boolean equals(Object obj) {
			
			if (this == obj)
				return true;
			
			if (obj == null)
				return false;
			
			if (getClass() != obj.getClass())
				return false;
			
			Nodo other = (Nodo) obj;
			if (nombre == null && other.nombre != null)
				return false;
			
			if (!nombre.equals(other.nombre))
				return false;
			
			if (tipo == null && other.tipo != null)
				return false;
			
			if (!tipo.equals(other.tipo))
				return false;
			
			if(valor == null && other.valor != null)
				return false;
			
			return valor.equals(other.valor);
		}
	}
	
	public static class Funcion {
	
		private String funcion;
		private String namespace;
		
		private List<Header> headers;

		public Funcion(String funcion, String namespace, List<Header> headers) {
			
			this.funcion = funcion;
			this.namespace = namespace;
			
			this.headers = headers;
		}

		public String getFuncion() {
			
			return funcion;
		}

		public String getNamespace() {
			
			return namespace;
		}

		public List<Header> getHeaders() {
			
			return headers;
		}
		
		public SOAPElement complete(SOAPMessage soapMessage) throws SOAPException {

	        SOAPBody soapBody  = soapMessage.getSOAPBody(); 
	                  
	        SOAPElement funcion = soapBody.addChildElement(this.funcion, this.namespace);
	        
	        for(Header header : this.headers) {
	        	
	        	header.complete(funcion);
	        }
	        
	        return funcion;
		}
	}
	
	public static class Header {
		
		private String title;
		private String value;
		
		public Header(String title, String value) {
			
			this.title = title;
			this.value = value;
		}
		
		public String getTitle() {
			
			return title;
		}
		
		public String getValue() {
			
			return value;
		}

		public void complete(SOAPElement soapElement) throws SOAPException {

	        soapElement.setAttribute(title, value);
		}
		
		public void complete(SOAPMessage soapMessage) throws SOAPException {

	        SOAPPart soapPart = soapMessage.getSOAPPart();
	        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
	        
	        soapEnvelope.addNamespaceDeclaration(title, value);
		}

		@Override
		public boolean equals(Object obj) {
			
			if (this == obj)
				return true;
			
			if (obj == null)
				return false;
			
			if (getClass() != obj.getClass())
				return false;
			
			Header other = (Header) obj;
			if (title == null && other.title != null)
				return false;
			
			if (!title.equals(other.title))
				return false;
			
			if(value == null && other.value != null)
				return false;
			
			return value.equals(other.value);
		}
	}
}
package com.repex.librerias.soap.timeout;

public final class TimeoutController {

    private TimeoutController() {}

    public static void execute(Thread task) throws TimeoutException {
    	
    	execute(task, 5000);
    }
    
    public static void execute(Thread task, long timeout) throws TimeoutException {
    	
        task.start();
        
        try {
        	
            task.join(timeout);
            
        } catch (InterruptedException e) {}
        
        if (task.isAlive()) {
        	
            task.interrupt();
            throw new TimeoutException();
        }
    }

    public static void execute(Runnable task) throws TimeoutException {
    	
    	execute(task, 5000);
    }
    
    public static void execute(Runnable task, long timeout) throws TimeoutException {
    	
        Thread t = new Thread(task, "Timeout guard");
        t.setDaemon(true);
        execute(t, timeout);
    }

    public static class TimeoutException extends Exception {
    	
    	private static final long serialVersionUID = 1L;

        public TimeoutException() {}

        public TimeoutException(String message) {
        	
            super(message);
        }

        public TimeoutException(Throwable cause) {
        	
            super(cause);
        }

        public TimeoutException(String message, Throwable cause) {
        	
            super(message, cause);
        }
    }
}

package com.repex.librerias.soap.utilidades;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class SOAPResponseEnvelopment {
	
	private Document document;
	
	private Boolean format = true; // FORMATEAR AL RECIBIR
	
	public Boolean getFormat() {
		return format;
	}
	
	public void setFormat(Boolean format) {
		this.format = format;
	}
	
	public SOAPResponseEnvelopment(SOAPMessage soapResponse) throws SOAPException, IOException, ParserConfigurationException, SAXException {
			
		ByteArrayOutputStream out = new ByteArrayOutputStream();		
		soapResponse.writeTo(out);
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		document = dBuilder.parse(new InputSource(new ByteArrayInputStream(out.toString().getBytes("utf-8"))));
		
		document.getDocumentElement().normalize();
	}
	
	public Error isError() {

		Element parent = document.getDocumentElement();		
		NodeList nodeList = parent.getElementsByTagName("faultcode");
		
		if(nodeList != null && nodeList.getLength() == 0)
			return null;
		
		return Error.values()[Integer.parseInt(nodeList.item(0).getFirstChild().getNodeValue())-1];
	}
	
	public String getError() {

		Element parent = document.getDocumentElement();		
		NodeList nodeList = parent.getElementsByTagName("faultstring");
		
		if(nodeList != null && nodeList.getLength() > 0) {
			
			return nodeList.item(0).getFirstChild().getNodeValue();
		}
		
		return "";
	}
	
	public List<String> getErrorList() {

		List<String> lista = new LinkedList<String>();
		
		Element parent = document.getDocumentElement();		
		NodeList nodeList = parent.getElementsByTagName("faultstring");
		
		if(nodeList != null && nodeList.getLength() > 0) {
			
			for(String error : nodeList.item(0).getFirstChild().getNodeValue().split(";")) {
				
				lista.add(error);
			}
		}
		
		return lista;
	}

	/**
	 * Tansforma los datos que no son de una lista
	 * @return
	 */
	public SOAPResponseMap getData() {
		
		if(isError() != null)
			return null;
		
		Element parent = document.getDocumentElement();
		NodeList nodeList = parent.getFirstChild().getFirstChild().getFirstChild().getChildNodes();

		return new SOAPResponseMap(nodeList, format);
	}

	public SOAPResponseList getMultiData() {
		
		if(isError() != null)
			return null;
		
		List<Element> datos = new LinkedList<Element>();
		
		// ELEMENTO PADRE
		Element parent = document.getDocumentElement();
		
		NodeList node = parent.getFirstChild().getFirstChild().getFirstChild().getChildNodes();
		
		if(node != null && node.getLength() > 0) {
			
			for(int i = 0 ; i < node.getLength();i++) {

				// DATOS
				datos.add((Element) node.item(i));
			}
		}
		
		return new SOAPResponseList(datos, format);
	}
	
	public static enum Error {

		ERROR_BASE_DATOS((short) 1, ""),
		ERROR_ARGUMENTO((short) 2, ""),
		ERROR_INSERTAR((short) 3, ""),
		ERROR_CORREO((short) 4, "");
		
		public short id;
		public String error;
		
		private Error(short id, String error) {
			
			this.id = id;
			this.error = error;
		}
	}
}
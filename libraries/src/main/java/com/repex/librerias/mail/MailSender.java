package com.repex.librerias.mail;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.xml.sax.SAXException;

import com.repex.librerias.soap.SOAPControler;
import com.repex.librerias.soap.SOAPEnvironment;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;
import com.repex.librerias.soap.utilidades.SOAPResponseEnvelopment;
import com.repex.librerias.soap.utilidades.SOAPResponseMap;
import com.repex.librerias.soap.utilidades.SOAPUser;
import com.repex.librerias.utilidades.Constantes;

public class MailSender {
	
	public static boolean send(Origen origen, Mail mail, SOAPUser soapUser, String database) throws SOAPException, RequiredAuthenticationException {
		return send(origen, mail, soapUser, database, false);
	}
	
	public static boolean send(Origen origen, Mail mail, SOAPUser soapUser, String database, Boolean format) throws SOAPException, RequiredAuthenticationException {
		
		// DATOS SOAP ORIGINALES
		SOAPControler state = SOAPControler.getSingleton().state();
				
		// DATOS SOAP
		SOAPControler.getSingleton().clear();

    	SOAPControler.getSingleton().setEndPoint(Constantes.PRO_END_POINT);
    	SOAPControler.getSingleton().setSoapUser(soapUser);

    	// HEADERS
    	List<SOAPEnvironment.Header> headers = new LinkedList<SOAPEnvironment.Header>();

        headers.add(new SOAPEnvironment.Header("ns1","http://repexgroup.eu/apps/onesat-app/admin/"));
        headers.add(new SOAPEnvironment.Header("SOAP-ENC","http://schemas.xmlsoap.org/soap/encoding/"));
        headers.add(new SOAPEnvironment.Header("xsi","http://www.w3.org/2001/XMLSchema-instance"));
        headers.add(new SOAPEnvironment.Header("tns","urn:mi_ws1"));
        headers.add(new SOAPEnvironment.Header("wsdl","http://schemas.xmlsoap.org/wsdl/"));
        headers.add(new SOAPEnvironment.Header("xsd","http://www.w3.org/2001/XMLSchema"));
        
        SOAPControler.getSingleton().addCommonHeaders(headers);
        
        // NODES
        SOAPControler.getSingleton().addCommonNode(new SOAPEnvironment.Nodo("database", "xsd:string", database));
          
        // RESULTADO 
        boolean resultado = false;
        
		// FUNCION
		String FUNCION = "enviar_correo";

		List<SOAPEnvironment.Header> funcionHeaders = new LinkedList<SOAPEnvironment.Header>();
		funcionHeaders.add(new SOAPEnvironment.Header("SOAP-ENV:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/"));

		SOAPEnvironment.Funcion funcion = new SOAPEnvironment.Funcion(FUNCION, "ns1", funcionHeaders);

		List<SOAPEnvironment.Parametro> parametros = new LinkedList<SOAPEnvironment.Parametro>();
		
		// CORREO
		List<SOAPEnvironment.Nodo> correo = new LinkedList<SOAPEnvironment.Nodo>();
		correo.add(new SOAPEnvironment.Nodo("CORREO", "xsd:string", origen.getCorreo()));
		if(origen.getReply() != null && !origen.getReply().equals("")) {
			correo.add(new SOAPEnvironment.Nodo("REPLY", "xsd:string", origen.getReply()));
		}
		
		correo.add(new SOAPEnvironment.Nodo("CONTRASENA", "xsd:string", origen.getContrasena()));
		
		parametros.add(new SOAPEnvironment.Parametro("correo", "tns:Origen", correo));

		// EMAIL
		List<SOAPEnvironment.Nodo> email = new LinkedList<SOAPEnvironment.Nodo>();
		email.add(new SOAPEnvironment.Nodo("TITULO", "xsd:string", mail.getTitulo()));
		email.add(new SOAPEnvironment.Nodo("CONTENIDO", "xsd:string", mail.getContenido()));


		List<SOAPEnvironment.Parametro> destinos = new LinkedList<SOAPEnvironment.Parametro>();
		for(String destino : mail.getDestinos()) {

			List<SOAPEnvironment.Nodo> d = new LinkedList<SOAPEnvironment.Nodo>();
			d.add(new SOAPEnvironment.Nodo("DESTINO", "xsd:string", destino));
			
			destinos.add(new SOAPEnvironment.Parametro("item", "tns:Destino", d));
		}

		List<SOAPEnvironment.Lista> listaDestinos = new LinkedList<SOAPEnvironment.Lista>();
		listaDestinos.add(new SOAPEnvironment.Lista("DESTINOS", "tns:Destino", destinos));
		
		parametros.add(new SOAPEnvironment.Parametro("email", "tns:Email", email, listaDestinos));
		
		// ADJUNTOS		
		List<SOAPEnvironment.Parametro> adjuntos = new LinkedList<SOAPEnvironment.Parametro>();
		
		for(Mail.Adjunto adjunto : mail.getAdjuntos()) {

			List<SOAPEnvironment.Nodo> a = new LinkedList<SOAPEnvironment.Nodo>();
			a.add(new SOAPEnvironment.Nodo("NOMBRE", "xsd:string", adjunto.getNombre()));
			a.add(new SOAPEnvironment.Nodo("ADJUNTO", "xsd:string", adjunto.getAdjunto()));

			adjuntos.add(new SOAPEnvironment.Parametro("item", "tns:Adjunto", a));
		}

		List<SOAPEnvironment.Lista> listas = new LinkedList<SOAPEnvironment.Lista>();
		listas.add(new SOAPEnvironment.Lista("adjuntos", "tns:Adjunto", adjuntos));
		
		// CONSTRUIR
		SOAPEnvironment soapEnvironment = new SOAPEnvironment(funcion, parametros, listas, null);
		soapEnvironment.setFormat(format);
		
		// PARSE RESPONSE
		SOAPMessage soapResponse = SOAPControler.getSingleton().callSoapWebService(FUNCION, soapEnvironment);

		try {

			SOAPResponseEnvelopment lastResponse = new SOAPResponseEnvelopment(soapResponse);
			if(lastResponse.isError() != null) {

				throw new SOAPException(lastResponse.getError());
			}

			SOAPResponseMap response = lastResponse.getData();
			resultado = Boolean.parseBoolean(response.getAtributte("BOOLEAN").getDato());

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		
		// RECUPERAR SOAP
    	SOAPControler.change(state);
		
		return resultado;
	}
}
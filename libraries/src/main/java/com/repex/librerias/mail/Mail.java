package com.repex.librerias.mail;

import java.util.LinkedList;
import java.util.List;

public class Mail {

	private List<String> destinos;
	
	private String titulo;
	private String contenido;
	
	private List<Adjunto> adjuntos;
	
	public Mail() {
		this.destinos = new LinkedList<String>();
		this.adjuntos = new LinkedList<Mail.Adjunto>();
	}
	
	public List<String> getDestinos() {
		return destinos;
	}

	public void addDestino(String destino) {
		this.destinos.add(destino);
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public List<Adjunto> getAdjuntos() {
		return adjuntos;
	}
	
	public void addAdjunto(Adjunto adjunto) {
		this.adjuntos.add(adjunto);
	}
	
	public static class Adjunto {
		
		private String nombre;
		private String adjunto;
		
		public String getNombre() {
			return nombre;
		}
		
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		
		public String getAdjunto() {
			return adjunto;
		}
		
		public void setAdjunto(String adjunto) {
			this.adjunto = adjunto;
		}
	}
}
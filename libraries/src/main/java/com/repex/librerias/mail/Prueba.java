package com.repex.librerias.mail;

import javax.xml.soap.SOAPException;

import com.repex.librerias.mail.Mail.Adjunto;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;
import com.repex.librerias.soap.utilidades.SOAPUser;

public class Prueba {

	public static void main(String[] args) {

		Mail mail = new Mail();
		mail.addDestino("jmiguel.guillamon@repexgroup.eu");
		
		if(!mail.getDestinos().isEmpty()) {
			
			// SUBIR ADJUNTOS
			Adjunto adjunto = new Adjunto();
			adjunto.setNombre("chat.psd");
			adjunto.setAdjunto("/apps/onesat-app/axpre/adjuntos/tmp/2019/01/28/tmp_1548711978593.psd");
			mail.addAdjunto(adjunto);
			
			mail.setTitulo("Prueba");
			mail.setContenido("Prueba desde libreria");

			Origen origen = new Origen();
			origen.setCorreo("jmiguel.guillamon@repexgroup.eu");
			origen.setContrasena("Tel685283135");

			SOAPUser soapUser = new SOAPUser("juanmi", "juanmi");
			String database = "repexgro_onesat_admin_pre";

			try {

				if(!MailSender.send(origen, mail, soapUser, database)) {

				}

			} catch (SOAPException e) {
				e.printStackTrace();
			} catch (RequiredAuthenticationException e) {
				e.printStackTrace();
			}
		}
	}
}
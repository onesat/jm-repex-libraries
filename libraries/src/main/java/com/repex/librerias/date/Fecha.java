package com.repex.librerias.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Fecha {
	
	// FORMATOS PREDEFINIDOS
	public static Formato FORMATO_COMPLETO = new Formato() {
		
		public SimpleDateFormat getFormato() {
			
			return new SimpleDateFormat("yyy-MM-dd H:mm:ss");
		}
	};

	public static Formato FORMATO_SIN_HORA = new Formato() {
		
		public SimpleDateFormat getFormato() {
			
			return new SimpleDateFormat("yyy-MM-dd");
		}
	};
	
	public static Formato FORMATO_INICIO = new Formato() {

		public SimpleDateFormat getFormato() {

			return new SimpleDateFormat("yyy-MM-dd 0:00:00");
		}
	};
	
	public static Formato FORMATO_FIN = new Formato() {

		public SimpleDateFormat getFormato() {

			return new SimpleDateFormat("yyy-MM-dd 23:59:59");
		}
	};
	
	// MODOS PREDEFINIDOS
	public static Diferencia DIFERENCIA_DIAS = new Diferencia() {
		
		public int diferencia(Fecha origen, Fecha destino) {
			
			int rango = 1000; // 1 segundo
			rango = 60 * rango; // 1 minuto
			rango = 60 * rango; // 1 hora
			rango = 24 * rango; // 1 dia
			
			return Math.abs((int) ((origen.getFecha().getTime()-destino.getFecha().getTime())/rango));
		}
	};
	
	public static Diferencia DIFERENCIA_MES = new Diferencia() {
		
		public int diferencia(Fecha origen, Fecha destino) {
			
			if(origen.despues(destino)) {
				
				return dif(destino, origen);
			}
			
			return dif(origen, destino);
		}

		private int dif(Fecha origen, Fecha destino) {
			
			int diferencia = DIFERENCIA_ANOS.diferencia(origen, destino) * 12;			
			
			int diferenciaMes = destino.getMes() - origen.getMes();
			if(diferenciaMes < 0) {
			
				diferenciaMes = 12 + diferenciaMes;
			}
			
			if(diferenciaMes > 0 && destino.getDia() < origen.getDia()) {
				
				diferenciaMes--;
			}
			
			return diferencia + diferenciaMes;
		}
	};
	
	public static Diferencia DIFERENCIA_ANOS = new Diferencia() {
		
		public int diferencia(Fecha origen, Fecha destino) {
			
			if(origen.despues(destino)) {
				
				return dif(destino, origen);
			}
			
			return dif(origen, destino);
		}
		
		private int dif(Fecha origen, Fecha destino) {
			
			int diferencia = destino.getAno() - origen.getAno();
			if(diferencia == 0)
				return diferencia;
				
			if(origen.getMes() > destino.getMes()) {
				
				diferencia--;
				
			} else if(origen.getMes() == destino.getMes() && origen.getDia() > destino.getDia()) {
				
				diferencia--;
			}
			
			return diferencia;
		}
	};
	
	// VARIABLES
	private Date fecha;

	public Fecha(Fecha fecha) {
		
		this.fecha = fecha.fecha;
	}
	
	public Fecha(Date fecha) {
		
		this.fecha = fecha;
	}

	public Fecha() {
		
		this.fecha = new Date();
	}
	
	public Fecha(String fecha, Formato formato) {
	
		try {

			this.fecha = formato.getFormato().parse(fecha);
			
		} catch (Exception e) {}
	}

	public Fecha(String fecha) {
		
		this(fecha, FORMATO_COMPLETO);
	}
	
	public String parse(Formato formato) {

		return formato.getFormato().format(fecha);
	}
	
	public String parse() {
	
		return parse(FORMATO_COMPLETO);
	}
	
	// AUXILIARES
	private Calendar getCalendar() {

		Calendar calendar = Calendar.getInstance(Locale.getDefault());
		calendar.setTime(fecha);
		
		return calendar;
	}
	
	// FUNCIONES
	public Date getFecha() {
		
		return fecha;
	}
	
	/**
	 * DEVUELVE SI LA FECHA ES ANTERIOR A LA FECHA PASADA COMO PARAMETRO
	 * @param fecha
	 * @return
	 */
	public boolean antes(Fecha fecha) {
	
		return getFecha().before(fecha.getFecha());
	}
	
	/**
	 * DEVUELVE SI LA FECHA ES DESPUES DE LA FECHA PASADA COMO PARAMETRO
	 * @param fecha
	 * @return
	 */
	public boolean despues(Fecha fecha) {
	
		return getFecha().after(fecha.getFecha());
	}
	
	public int getDia() {
		
		Calendar calendar = getCalendar();		
		return calendar.get(Calendar.DAY_OF_MONTH);
	}
	
	public int getMes() {

		Calendar calendar = getCalendar();
		return calendar.get(Calendar.MONTH)+1;
	}
	
	public int getAno() {

		Calendar calendar = getCalendar();
		return calendar.get(Calendar.YEAR);
	}

	public int getSemana() {
		
		Calendar calendar = getCalendar();		
		return calendar.get(Calendar.WEEK_OF_YEAR);
	}
	
	// FUNCIONES PARA EL PANEL CALENDARIO
	public void reiniciar() {

		Calendar calendar = getCalendar();
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		
		fecha = calendar.getTime();
	}
	
	public int getMaximoDias() {

		Calendar calendar = getCalendar();		
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
	
	public int getDiaSemana() {

		Calendar calendar = getCalendar();		
		return (calendar.get(Calendar.DAY_OF_WEEK) + 5) % 7;
	}
	
	public Fecha anadir(int tipo, int cambio) {
		
		if(tipo < 0 || tipo >= Calendar.FIELD_COUNT)
			return this;

		Calendar calendar = getCalendar();
		calendar.add(tipo, cambio);
		
		fecha = calendar.getTime();
		return this;
	}
	
	@Deprecated
	public Fecha cambiar(int tipo, int cambio) {
		
		if(tipo < 0 || tipo >= Calendar.FIELD_COUNT)
			return this;

		Calendar calendar = getCalendar();
		calendar.add(tipo, cambio);
		
		fecha = calendar.getTime();
		return this;
	}
	
	public Fecha first() {

		Calendar calendar = getCalendar();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		fecha = calendar.getTime();
		return this;
	}
	
	public Fecha last() {

		Calendar calendar = getCalendar();
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		
		fecha = calendar.getTime();
		return this;
	}
	
	public int diferencia(Diferencia modo, Fecha destino) {
		
		return modo.diferencia(this, destino);
	}
	
	public int diferencia(Fecha destino) {
		
		return DIFERENCIA_DIAS.diferencia(this, destino);
	}
	
	/**
	 * 
	 * @return TRUE si son el mismo dia sin contar la hora
	 */
	public boolean comprobar(Fecha fecha) {
		
		return fecha != null && getDia() == fecha.getDia() && getMes() == fecha.getMes() && getAno() == fecha.getAno();
	}
	
	// EQUALS
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Fecha other = (Fecha) obj;
		return fecha != null && other != null && fecha.equals(other.getFecha());
	}
	
	// INTERFAZ DE FORMATO DE FECHA
	public interface Formato {
		
		public SimpleDateFormat getFormato();
	}
	
	public interface Diferencia {
		
		public int diferencia(Fecha origen, Fecha destino);
	}
}
package com.repex.librerias.criptografia;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

public class Criptografia {
	
	private static Criptografia singleton;

	// SINGLETON
	private Criptografia() {}

	public static Criptografia getSingleton() {
		
		if(singleton == null) {
		
			singleton = new Criptografia();
		}
		
		return singleton;
	}
	
	// VARIABLES DE CONFIGURACION
	private String key = "MySecretKey";
	
	private byte[] salt = {(byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32, (byte) 0x56, (byte) 0x35, (byte) 0xE3, (byte) 0x03};
	private int iterationCount = 1;

	public void generateKey(int seed, int length, int maxSeedValue) {
		
		// COMPROBAR
		length = Math.max(10, length);
		maxSeedValue = Math.max(1, maxSeedValue);
		
		// RANDOM
		Random r = new Random(seed);
		
		key = "";
		
		for(int i=0; i<length; i++) {
			
			key += Integer.toString(r.nextInt(maxSeedValue+1));
		}
	}
	
	public void generateKey(int seed, int length) {
		
		this.generateKey(seed, length, 1);
	}
	
	public void generateKey(int seed) {
		
		this.generateKey(seed, 128);
	}

	public String getKey() {
		
		return key;
	}
	
	public void setKey(String key) {
		
		this.key = key;
	}
	
	public void setIterationCount(int iterationCount) {
		
		this.iterationCount = Math.max(1, iterationCount);
	}
	
	public String encrypt(String plainText) {
		
		try {
			
			Cipher cipher = generateCipher(Cipher.ENCRYPT_MODE);
			
			String charSet = "UTF-8";
			byte[] in = plainText.getBytes(charSet);
			byte[] out = cipher.doFinal(in);
			
			return new String(Base64.getEncoder().encode(out));
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return "";
	}

	public String decrypt(String encryptedText) {

		try {

			Cipher cipher = generateCipher(Cipher.DECRYPT_MODE);

			String charSet = "UTF-8";
			byte[] enc = Base64.getDecoder().decode(encryptedText);
			byte[] utf8 = cipher.doFinal(enc);
			
			return new String(utf8, charSet);
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return "";
	}
		
	private Cipher generateCipher(int opmode) throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
			
		KeySpec keySpec = new PBEKeySpec(key.toCharArray(), salt, iterationCount);
		SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);

		// Prepare the parameter to the ciphers
		AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

		//Decryption process; same key will be used for decr
		Cipher cipher = Cipher.getInstance(key.getAlgorithm());
		cipher.init(opmode, key, paramSpec);
		
		return cipher;
	}
}
package com.repex.librerias.estado;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import com.repex.librerias.components.labels.LabelOver;

public class VentanaEstado extends JFrame {
	
	private static final long serialVersionUID = 1L;

	// CONSTANTES
	private static int VELOCIDAD_SCROLL = 16;
	
	
	// INTERFAZ
	private JPanel contentPane;
	private JPanel panelEstado;
	
	private LabelOver limpiar;
	
	// VENTANA MAXIMIZADA
	private EstadoVentana estado;
	
	public EstadoVentana getEstado() {
		
		return estado;
	}

	// VARIABLES	
	private int tamanoOpciones;
	
	public VentanaEstado(final String tag, int tamanoOpciones) {
				
		this.tamanoOpciones = tamanoOpciones;
		
		inicializar();

		// AL SALIR QUITAR DEL ESTADO
		addWindowListener(new WindowAdapter() {
			
			@Override     
			public void windowClosing(WindowEvent we) {

				EstadoController.getSingleton().removeVentana(tag);
			}
		});
	}

	public VentanaEstado(String tag) {
		
		this(tag, 80);
	}
	
	private void inicializar() {
		
		// CONFIGURACION
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setBounds(50, 50, 450, 300);
		setMinimumSize(new Dimension(1350, 600));
		setTitle("Estado");
		
		// MINIMIZADO
		addWindowStateListener(new WindowStateListener() {
			
			@Override
			public void windowStateChanged(WindowEvent e) {

				if ((e.getNewState() & Frame.ICONIFIED) == Frame.ICONIFIED) {
					
					estado = EstadoVentana.MINIMIZADO;
					
				} else if ((e.getNewState() & Frame.MAXIMIZED_BOTH) == Frame.MAXIMIZED_BOTH) {
					
					estado = EstadoVentana.MAXIMIZADO;
				}
			}
		});
		
		// CONTENIDO
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		// TODO: PONER FILTROS??
		
		// PANEL ESTADO
		panelEstado = new JPanel();
		panelEstado.setLayout(new BoxLayout(panelEstado, BoxLayout.Y_AXIS));
		panelEstado.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.GRAY));
		panelEstado.setBackground(Color.WHITE);

		JScrollPane scrollEstado = new JScrollPane(panelEstado);
		scrollEstado.setOpaque(false);
		scrollEstado.getVerticalScrollBar().setUnitIncrement(VELOCIDAD_SCROLL);
		contentPane.add(scrollEstado, BorderLayout.CENTER);
		
		// PANEL OPCIONES		
		JPanel panelOpciones = new JPanel();
		panelOpciones.setLayout(new BoxLayout(panelOpciones, BoxLayout.Y_AXIS));
		contentPane.add(panelOpciones, BorderLayout.EAST);
					
		// LIMPIAR
		limpiar = new LabelOver(Estado.RESOURCES_FOLDER + "limpiar.png", tamanoOpciones);
		limpiar.addAccion(new Runnable() {
			
			@Override
			public void run() {

				panelEstado.removeAll();

				revalidate();
				repaint();	
			}
		});
		
		panelOpciones.add(limpiar);
	}

	void addEstado(Estado estado) {
		
		panelEstado.add(estado, 0);
		
		revalidate();
		repaint();
	}
	
	void removeEstado(Estado estado) {
		
		panelEstado.remove(estado);
		
		revalidate();
		repaint();
	}
}
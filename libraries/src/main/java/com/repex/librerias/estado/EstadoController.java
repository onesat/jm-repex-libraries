package com.repex.librerias.estado;

import java.util.HashMap;
import java.util.Map;

public class EstadoController {
	
	// SINGLETON
	private static EstadoController estado;
		
	private EstadoController() {
		
		paneles = new HashMap<String, PanelEstado>();
		ventanas = new HashMap<String, VentanaEstado>();
		
		tipos = Estado.Tipo.getDefault();
	}
	
	public static EstadoController getSingleton() {
		
		if(estado == null)
			estado = new EstadoController();
		
		return estado;
	}

	// PANELES
	private Map<String, PanelEstado> paneles;
	private Map<String, VentanaEstado> ventanas;
	
	public void addPanel(String tag, PanelEstado estado) {		
		paneles.put(tag, estado);
	}
	
	public void removePanel(String tag) {
		paneles.remove(tag);
	}
	
	public void addVentana(String tag, VentanaEstado ventana) {		
		ventanas.put(tag, ventana);
	}
	
	public void removeVentana(String tag) {
		ventanas.remove(tag);
	}
	
	public PanelEstado getPanel(String tag) {
		
		return paneles.get(tag);
	}
	
	public VentanaEstado getVentana(String tag) {
		
		return ventanas.get(tag);
	}
	
    public void actualizarEstado(String tag, Estado estado) {
		
    	// ASIGNAR TAG
    	estado.setTag(tag);
    	
    	// ACTUALIZAR
    	PanelEstado panel = paneles.get(tag);
    	if(panel != null) {    		
    		panel.addEstado(estado);
    	}

    	VentanaEstado ventana = ventanas.get(tag);
		if(ventana != null) {			
			ventana.addEstado(estado);
		}
    }
    
    public void removeEstado(String tag, Estado estado) {

    	PanelEstado panel = paneles.get(tag);
    	if(panel != null) {    		
    		panel.removeEstado(estado);
    	}

    	VentanaEstado ventana = ventanas.get(tag);
		if(ventana != null) {
			ventana.removeEstado(estado);
		}
    }
    
    // TIPOS
    public Map<String, Estado.Tipo> tipos;
    
    public Estado.Tipo getTipo(String tag) {
    	
    	return tipos.get(tag);
    }
    
    public void addTipo(String tag, Estado.Tipo tipo) {
    	
    	tipos.put(tag, tipo);
    }
}
package com.repex.librerias.estado;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.repex.librerias.components.labels.LabelOver;
import com.repex.librerias.date.Fecha;
import com.repex.librerias.utilidades.Constantes;
import com.repex.librerias.utilidades.IconUtils;

public class Estado extends JPanel {
	
	private static final long serialVersionUID = 1L;

	// Estado
	public static String RESOURCES_FOLDER = Constantes.CARPETA_RECURSOS + "estado/";
	public static String TIPOS_RESOURCES_FOLDER = RESOURCES_FOLDER + "tipos/";

	private static Color COLOR_ARRANQUE = new Color(250, 250, 180);
	private static Color COLOR_ERROR = new Color(255, 180, 180);
	private static Color COLOR_ACIERTO = new Color(180, 255, 180);
	
	// VARIABLES
	private String mensaje;
	private Tipo tipo;
	
	private Fecha fecha;
	
	public Estado(String mensaje, Tipo tipo, Fecha fecha, List<Opcion> opciones) {
	
		this.mensaje = mensaje;
		this.tipo = tipo;

		if(this.tipo == null) {
			
			this.tipo = EstadoController.getSingleton().getTipo(Tipo.NORMAL);
		}
		
		this.fecha = fecha;
		
		this.opciones = opciones;
		
		this.inicializar();
	}

	public Estado(String mensaje, Tipo tipo, Fecha fecha) {
		
		this(mensaje, tipo, fecha, new LinkedList<Opcion>());
	}
	
	public Estado(String mensaje, String tipo, Fecha fecha) {

		this(mensaje, EstadoController.getSingleton().getTipo(tipo), fecha);		
	}
	
	public Estado(String mensaje, Tipo tipo) {
	
		this(mensaje, tipo, new Fecha());
	}

	public Estado(String mensaje, String tipo) {
	
		this(mensaje, EstadoController.getSingleton().getTipo(tipo), new Fecha());
	}
	
	public Estado(Estado other) {
	
		this(other.mensaje, other.tipo, other.fecha, other.opciones);
	}
	
	public String getMensaje() {
		
		return mensaje;
	}

	public Tipo getTipo() {
		
		return tipo;
	}
	
	private String tag;
	
	public String getTag() {
		return tag;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	private void inicializar() {
		
		this.setMaximumSize(new Dimension(2000, 42));
		this.setLayout(new BorderLayout(0, 0));
		
		this.setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, Color.GRAY));
		
		if(tipo.fondo != null) {
		
			this.setBackground(tipo.fondo);
			
		} else {
			
			this.setOpaque(false);
		}
		
		// INFORMACION
		JPanel panelEstado = new JPanel();
		panelEstado.setOpaque(false);
		panelEstado.setLayout(new BoxLayout(panelEstado, BoxLayout.X_AXIS));
		panelEstado.setAlignmentX(LEFT_ALIGNMENT);
		add(panelEstado, BorderLayout.WEST);

		panelEstado.add(Box.createHorizontalStrut(5));
		
		if(tipo.imagen != null) {
			
			// ICONO
			JLabel icono = new JLabel(IconUtils.escalar(tipo.imagen, 35));
			panelEstado.add(icono);
			
			panelEstado.add(Box.createHorizontalStrut(15));
		}
		
		// FECHA		
		JLabel lblFecha = new JLabel(fecha.parse());
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 16));			
		panelEstado.add(lblFecha);

		// INFORMACION
		panelEstado.add(Box.createHorizontalStrut(25));

		JLabel lblEstado = new JLabel(mensaje);
		lblEstado.setFont(new Font("Tahoma", Font.BOLD, 14));			
		panelEstado.add(lblEstado);
				
		// OPCIONES
		panelOpciones = new JPanel();
		panelOpciones.setOpaque(false);
		panelOpciones.setLayout(new FlowLayout(FlowLayout.TRAILING, 5, 0));
		add(panelOpciones, BorderLayout.EAST);
			
		for(Opcion opcion : opciones) {

			LabelOver nueva = new LabelOver(opcion.icono, 30);
			nueva.addAccion(opcion.accion);
			
			panelOpciones.add(nueva, 0);
		}
		
		// ELIMINAR
		LabelOver eliminar = new LabelOver(RESOURCES_FOLDER+"eliminar.png", 30);
		eliminar.addMouseListener(new MouseAdapter() {
		
			@Override
			public void mouseClicked(MouseEvent e) {
				
				EstadoController.getSingleton().removeEstado(tag, Estado.this);
			}
			
		});
		
		panelOpciones.add(eliminar);
	}
	
	private List<Opcion> opciones;
	private JPanel panelOpciones;
	
	public void addOpcion(Opcion opcion) {
				
		// ANADIR A LISTA
		this.opciones.add(0, opcion);
		
		// ANADIR A INTERFAZ
		LabelOver nueva = new LabelOver(opcion.icono, 30);
		nueva.addAccion(opcion.accion);
		
		panelOpciones.add(nueva, 0);
		
		revalidate();
		repaint();
	}
	
	public static class Opcion {
		
		private String icono;
		private Runnable accion;
		
		public Opcion(String icono, Runnable accion) {
			
			this.icono = icono;
			this.accion = accion;
		}
	}
	
	// CLASES AUXILIARES
    public static class Tipo {

    	// CONSTANTES
    	public static String ARRANQUE = "ARRANQUE";
    	public static String NORMAL = "NORMAL";
    	public static String ERROR = "ERROR";
    	public static String ACIERTO = "ACIERTO";
    	
    	// VARIABLES
    	public Icon imagen;
    	public Color fondo;

    	public Tipo(Icon imagen, Color fondo) {
    		
    		this.imagen = imagen;
    		this.fondo = fondo;
    	}
    	
    	public Tipo(String imagen, Color fondo) {
    		
    		try {

        		this.imagen = IconUtils.escalar(getClass().getClassLoader().getResource(imagen));
        		
			} catch (Exception e) {

        		this.imagen = IconUtils.escalar(getClass().getClassLoader().getResource(Estado.TIPOS_RESOURCES_FOLDER + "defecto.png"));
			}
    		
    		this.fondo = fondo;
    	}
    	
    	public static Map<String, Tipo> getDefault() {
    		
    		Map<String, Tipo> tipos = new HashMap<String, Tipo>();
    		tipos.put(ARRANQUE, new Tipo(Estado.TIPOS_RESOURCES_FOLDER + "normal.png", Estado.COLOR_ARRANQUE));
    		tipos.put(NORMAL, new Tipo(Estado.TIPOS_RESOURCES_FOLDER + "normal.png", null));
    		tipos.put(ERROR, new Tipo(Estado.TIPOS_RESOURCES_FOLDER + "error.png", Estado.COLOR_ERROR));
    		tipos.put(ACIERTO, new Tipo(Estado.TIPOS_RESOURCES_FOLDER + "normal.png", Estado.COLOR_ACIERTO));
    		
    		return tipos;
    	}
    }
}
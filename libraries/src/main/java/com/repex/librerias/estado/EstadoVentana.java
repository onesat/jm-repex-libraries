package com.repex.librerias.estado;

public enum EstadoVentana {
	
	MAXIMIZADO,
	MINIMIZADO;
}
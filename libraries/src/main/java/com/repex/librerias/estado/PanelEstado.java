package com.repex.librerias.estado;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.repex.librerias.components.labels.LabelOver;

public class PanelEstado extends JPanel {

	private static final long serialVersionUID = 1L;

	// VARIABLES
	private String tag;
	
	private int tamano;
	private int tamanoIconos;

	// INTERFAZ
	private JPanel panelEstado;
	private JScrollPane scrollEstado;
	
	private LabelOver ventana;
	private LabelOver limpiar;
	private LabelOver minimizar;

	// CONSTANTES
	private static int VELOCIDAD_SCROLL = 16;
	
	private static int TAMANO_MAXIMO_ICONOS = 60;
	private static int TAMANO_MINIMO_ICONOS = 30;
	
	public PanelEstado(String tag, int tamano, int tamanoIconos) {

		this.tag = tag;
		
		this.tamano = tamano;
		
		this.tamanoIconos = tamanoIconos;
		this.tamanoIconos = Math.max(this.tamanoIconos, TAMANO_MINIMO_ICONOS);
		this.tamanoIconos = Math.min(this.tamanoIconos, TAMANO_MAXIMO_ICONOS);
		
		// ANADIR A ESTADO CONTROLLER
		EstadoController.getSingleton().addPanel(tag, this);
		
		inicializar();
	}
	
	public PanelEstado(String tag, int Tamano) {
		
		this(tag, Tamano, Tamano/5);		
	}

	public PanelEstado(String tag) {
		
		this(tag, 200);
	}

	public PanelEstado() {
		
		this("DEFAULT");
	}
	
	public PanelEstado(int Tamano) {
		
		this("DEFAULT", 200);
	}
	
	private void inicializar() {
			
		setLayout(new BorderLayout(0, 0));
		
		setBackground(Color.WHITE);
		
		setPreferredSize(new Dimension(0, tamano));
		setMinimumSize(getPreferredSize());
		setMaximumSize(getPreferredSize());
				
		add(Box.createVerticalStrut(5), BorderLayout.NORTH);
		
		// PANEL ESTADOS
		panelEstado = new JPanel();
		panelEstado.setBackground(Color.WHITE);
		panelEstado.setLayout(new BoxLayout(panelEstado, BoxLayout.Y_AXIS));
		panelEstado.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.GRAY));

		scrollEstado = new JScrollPane(panelEstado);
		scrollEstado.setOpaque(false);
		scrollEstado.getViewport().setOpaque(false);
		scrollEstado.getVerticalScrollBar().setUnitIncrement(VELOCIDAD_SCROLL);
		add(scrollEstado, BorderLayout.CENTER);
		
		// PANEL OPCIONES		
		JPanel panelOpciones = new JPanel();
		panelOpciones.setOpaque(false);
		panelOpciones.setLayout(new BoxLayout(panelOpciones, BoxLayout.PAGE_AXIS));
		add(panelOpciones, BorderLayout.EAST);
		
		ventana = new LabelOver(Estado.RESOURCES_FOLDER + "ventana.png", tamanoIconos);
		panelOpciones.add(ventana);
		
		limpiar = new LabelOver(Estado.RESOURCES_FOLDER + "limpiar.png", tamanoIconos);
		panelOpciones.add(limpiar);
		
		minimizar = new LabelOver(Estado.RESOURCES_FOLDER + "minimizar.png", tamanoIconos);
		panelOpciones.add(minimizar);
		
		// SACAR EN OTRA VENTANA
		ventana.addAccion(new Runnable() {
			
			@Override
			public void run() {
				
				// COMPROBAR SI YA EXISTE OTRA VENTANA
				if(EstadoController.getSingleton().getVentana(tag) == null) {
				
					VentanaEstado ventana = new VentanaEstado(tag);
					ventana.setVisible(true);
					
					EstadoController.getSingleton().addVentana(tag, ventana);
					
					// AÑADIR LOS ESTADOS ANTERIORES
					for(int i=panelEstado.getComponentCount()-1; i>=0; i--) {
						
						Component hijo = panelEstado.getComponent(i);					
						ventana.addEstado(new Estado((Estado) hijo));
					}
					
				} else {
					
					// PONER EL FOCUS SOBRE ESA VENTANA
					VentanaEstado ventana = EstadoController.getSingleton().getVentana(tag);
					
					if(ventana.getEstado() == EstadoVentana.MINIMIZADO) {
					
						// MAXIMIZAR
						ventana.setExtendedState(JFrame.MAXIMIZED_BOTH);
					}
					
					ventana.setVisible(false);
					ventana.setVisible(true);
				}
				
				// MINIMIZAR
				minimizar();
			}
		});
		
		// LIMPIAR
		limpiar.addAccion(new Runnable() {
			
			@Override
			public void run() {

				panelEstado.removeAll();

				revalidate();
				repaint();	
			}
		});
		
		// MINIMIZAR
		minimizar.addAccion(new Runnable() {
			
			@Override
			public void run() {
				
				if(scrollEstado.isVisible()) {

					minimizar();
					
				} else {

					maximizar();
				}				
			}
		});
	}
	
	// FUNCIONES COMUNES
	void addEstado(Estado estado) {

		panelEstado.add(estado, 0);
		
		revalidate();
		repaint();
	}
	
	void removeEstado(Estado estado) {
	
		panelEstado.remove(estado);
		
		revalidate();
		repaint();
	}
	
	// FUNCIONES PRIVADAS
	public void minimizar() {
		
		minimizar.setIcono(Estado.RESOURCES_FOLDER + "maximizar.png");
		minimizar.setTamano(tamanoIconos/2 + 5);

		// MINIMIZAR
		setPreferredSize(new Dimension(0, tamanoIconos/2 + 25));
		setMinimumSize(getPreferredSize());
		setMaximumSize(getPreferredSize());
		
		scrollEstado.setVisible(false);

		ventana.setVisible(false);
		limpiar.setVisible(false);
		
		revalidate();
		repaint();
	}
	
	public void maximizar() {

		minimizar.setIcono(Estado.RESOURCES_FOLDER + "minimizar.png");
		minimizar.setTamano(tamanoIconos);

		// MAXIMIZAR
		setPreferredSize(new Dimension(0, tamano));
		setMinimumSize(getPreferredSize());
		setMaximumSize(getPreferredSize());

		scrollEstado.setVisible(true);

		ventana.setVisible(true);
		limpiar.setVisible(true);
		
		revalidate();
		repaint();
	}
}
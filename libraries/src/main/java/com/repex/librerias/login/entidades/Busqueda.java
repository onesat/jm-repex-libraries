package com.repex.librerias.login.entidades;

public class Busqueda {
	
	private Integer tipo;
	private Boolean administrador;
	
	public Integer getTipo() {
		return tipo;
	}
	
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	
	public Boolean isAdministrador() {
		return administrador;
	}
	
	public void setAdministrador(Boolean administrador) {
		this.administrador = administrador;
	}
}
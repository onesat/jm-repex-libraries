package com.repex.librerias.login.dao;

import javax.xml.soap.SOAPException;

import com.repex.librerias.login.entidades.Busqueda;
import com.repex.librerias.login.entidades.Usuario;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;

public interface UsuarioDao {

	public Usuario login(String nombre, Busqueda tipo) throws SOAPException, RequiredAuthenticationException;
}
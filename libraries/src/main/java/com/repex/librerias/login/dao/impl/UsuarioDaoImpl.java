package com.repex.librerias.login.dao.impl;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.xml.sax.SAXException;

import com.repex.librerias.criptografia.Criptografia;
import com.repex.librerias.login.dao.UsuarioDao;
import com.repex.librerias.login.entidades.Busqueda;
import com.repex.librerias.login.entidades.Usuario;

import com.repex.librerias.soap.SOAPControler;
import com.repex.librerias.soap.SOAPEnvironment;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;
import com.repex.librerias.soap.utilidades.SOAPResponseEnvelopment;
import com.repex.librerias.soap.utilidades.SOAPResponseMap;

public class UsuarioDaoImpl implements UsuarioDao {
	
	@Override
	public Usuario login(String nombre, Busqueda busqueda) throws SOAPException, RequiredAuthenticationException {

		// FUNCION
		String FUNCION = "login";

		List<SOAPEnvironment.Header> funcionHeaders = new LinkedList<SOAPEnvironment.Header>();
		funcionHeaders.add(new SOAPEnvironment.Header("SOAP-ENV:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/"));

		SOAPEnvironment.Funcion funcion = new SOAPEnvironment.Funcion(FUNCION, "ns1", funcionHeaders);

		// NODOS
		List<SOAPEnvironment.Nodo> nodos = new LinkedList<SOAPEnvironment.Nodo>();
		nodos.add(new SOAPEnvironment.Nodo("NOMBRE", "xsd:string", nombre));
		if(busqueda != null) {

			if(busqueda.isAdministrador() != null) {
				nodos.add(new SOAPEnvironment.Nodo("ADMINISTRADOR", "xsd:boolean", busqueda.isAdministrador()));
			}

			if(busqueda.getTipo() != null && busqueda.getTipo() > 0) {
				nodos.add(new SOAPEnvironment.Nodo("TIPO", "xsd:int", busqueda.getTipo()));			
			}
		}
		
		// PARAMETROS
		List<SOAPEnvironment.Parametro> parametros = new LinkedList<SOAPEnvironment.Parametro>();
		parametros.add(new SOAPEnvironment.Parametro("entrada", "tns:Login", nodos));

		SOAPEnvironment soapEnvironment = new SOAPEnvironment(funcion, parametros, null);

		// PARSE RESPONSE
		SOAPMessage soapResponse = SOAPControler.getSingleton().callSoapWebService(FUNCION, soapEnvironment);

		try {
			
			SOAPResponseEnvelopment soapResponseEnvelopment = new SOAPResponseEnvelopment(soapResponse);
			if(soapResponseEnvelopment.isError() != null) {
				
				throw new SOAPException(soapResponseEnvelopment.getError());
			}

			SOAPResponseMap response = soapResponseEnvelopment.getData();
			if(response != null && response.getAtributte("ID_USUARIO") != null) {
				
				Usuario usuario = new Usuario();
				usuario.setId(Integer.parseInt(response.getAtributte("ID_USUARIO").getDato()));
				usuario.setNombre(response.getAtributte("NOMBRE").getDato());
				usuario.setContrasena(Criptografia.getSingleton().decrypt(response.getAtributte("CONTRASENA").getDato()));
				
				return usuario;
			}
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ParserConfigurationException e) {
			
			e.printStackTrace();
			
		} catch (SAXException e) {
			
			e.printStackTrace();
		}
				
		return null;
	}

}

package com.repex.librerias.login.screen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.xml.soap.SOAPException;

import com.repex.librerias.components.labels.LabelOver;
import com.repex.librerias.components.labels.LabelPanelUrl;
import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.components.paneles.BorderPanelOverSelected;
import com.repex.librerias.components.paneles.PanelContrasena;
import com.repex.librerias.components.paneles.VerticalPanelPadding;
import com.repex.librerias.components.textfield.HintTextField;
import com.repex.librerias.components.textfield.HintTextLabel;
import com.repex.librerias.components.utilidades.UrlControler;
import com.repex.librerias.criptografia.Criptografia;
import com.repex.librerias.login.dao.UsuarioDao;
import com.repex.librerias.login.dao.impl.UsuarioDaoImpl;
import com.repex.librerias.login.entidades.Busqueda;
import com.repex.librerias.login.entidades.Usuario;
import com.repex.librerias.soap.SOAPControler;
import com.repex.librerias.soap.SOAPEnvironment;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;
import com.repex.librerias.soap.timeout.TimeoutController;
import com.repex.librerias.soap.timeout.TimeoutController.TimeoutException;
import com.repex.librerias.soap.utilidades.SOAPUser;
import com.repex.librerias.utilidades.Constantes;
import com.repex.librerias.utilidades.IconUtils;

public class LoginScreen extends JFrame {
	
	private static final long serialVersionUID = 1L;

	// CONSTANTES
	public final static String TAG_ADMINISTRADOR = "ADMINISTRADOR";
	
	private final static int MARGIN = 2;
	private final static int TEXT_COLUMNS = 25;
	
	// VARIABLES
	private String titulo;
	
	private String icono; // URL ICONO
	private String logo; // URL LOGO
	
	private Busqueda parametros;
	
	private HintTextField nombre;
	private LabelOver desactivado;
	
	private PanelContrasena contrasena;
	
	// CONSTRUCTORES
	public LoginScreen(String titulo, String icono, String logo, DatabaseConfiguration databaseConfiguration) {
		
		this(titulo, icono, logo, null, databaseConfiguration);
	}
	
	public LoginScreen(String titulo, String icono, String logo, Busqueda parametros, DatabaseConfiguration databaseConfiguration) {
		
		this(titulo, icono, logo, parametros, databaseConfiguration, null);
	}

	public LoginScreen(String titulo, String icono, String logo, DatabaseConfiguration databaseConfiguration, OnLoginListener listener) {
		
		this(titulo, icono, logo, null, databaseConfiguration, listener);
	}
	
	public LoginScreen(String titulo, String icono, String logo, Busqueda parametros, DatabaseConfiguration databaseConfiguration, OnLoginListener listener) {

		super();
		
		// SOAP CONTROLLER
		SOAPControler.getSingleton().clear(); // LIMPIAR PRIMERO
		
		SOAPControler.getSingleton().setEndPoint(databaseConfiguration.getSoapEndPoint());
		SOAPControler.getSingleton().setSoapUser(databaseConfiguration.getSoapUser());

		List<SOAPEnvironment.Header> headers = new LinkedList<SOAPEnvironment.Header>();

	    headers.add(new SOAPEnvironment.Header("ns1","http://repexgroup.eu/apps/onesat-app/admin/"));
	    headers.add(new SOAPEnvironment.Header("SOAP-ENC","http://schemas.xmlsoap.org/soap/encoding/"));
	    headers.add(new SOAPEnvironment.Header("xsi","http://www.w3.org/2001/XMLSchema-instance"));
	    headers.add(new SOAPEnvironment.Header("tns","urn:mi_ws1"));
	    headers.add(new SOAPEnvironment.Header("wsdl","http://schemas.xmlsoap.org/wsdl/"));
	    headers.add(new SOAPEnvironment.Header("xsd","http://www.w3.org/2001/XMLSchema"));
	    
	    SOAPControler.getSingleton().addCommonHeaders(headers);
	    
	    SOAPControler.getSingleton().addCommonNode(new SOAPEnvironment.Nodo("database", "xsd:string", databaseConfiguration.getDatabase()));
		
		// INICIAR CRIPTOGRAFIA
		Criptografia.getSingleton().generateKey(758, 60);

		// LISTENER
		listeners = new LinkedList<OnLoginListener>();
		addListener(listener);
		
		// VARIABLES
		this.titulo = titulo;
		
		this.icono = icono;
		this.logo = logo; // Se pone de icono de Escritorio
		
		this.parametros = parametros;
		
		// CONFIGURACION
		setSize(400, 500);
		setResizable(false);
		setLocationRelativeTo(null);

		setTitle(titulo);
		setIconImage(((ImageIcon) IconUtils.escalar(getClass().getClassLoader().getResource(Constantes.CARPETA_RECURSOS + "login/logo.png"))).getImage());
		
		if(this.logo != null) {
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					
					try {
						
						setIconImage(load());
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
			}).start();
		}
		
		// INICIALIZAR
		inicializar();

		// SALIR
		addWindowListener(new WindowAdapter() {
			
			@Override     
			public void windowClosing(WindowEvent we) {

				salir();
			}
		});
	}
	
	private Image load() throws Exception {
		return ((ImageIcon) UrlControler.getSingleton().cargar(logo, 100)).getImage();
	}
	
	private void inicializar() {
		
		BorderPanel contentPane = new BorderPanel(0, Color.BLACK, 0, 10, false, true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(new BorderLayout(5, 5));
		setContentPane(contentPane);
		
		// LOGO. NORTE
		BorderPanel panelLogo = new BorderPanel(0, Color.decode("#DDDDDD"), 5, false, true);
		panelLogo.setOpaque(false);
		panelLogo.setLayout(new BorderLayout(5, 0));
		contentPane.add(panelLogo, BorderLayout.NORTH);
		
		LabelPanelUrl logo = new LabelPanelUrl(icono);
		logo.setDefecto(IconUtils.escalar(getClass().getClassLoader().getResource(Constantes.CARPETA_RECURSOS + "login/logo.png")));
		logo.setOpaque(false);
		logo.setSelectableOverable(false);
		panelLogo.add(logo, BorderLayout.CENTER);
				
		JPanel panelInformacion = new VerticalPanelPadding(0, Color.LIGHT_GRAY, MARGIN);
		panelInformacion.setOpaque(false);
		panelLogo.add(panelInformacion, BorderLayout.SOUTH);

		HintTextLabel titulo = new HintTextLabel("", SwingConstants.CENTER, MARGIN);
		titulo.setText(this.titulo);
		titulo.setForeground(Color.BLUE);
		titulo.setFont(new Font("Tahoma", Font.BOLD, 16));
		panelInformacion.add(titulo);

		HintTextLabel informacion = new HintTextLabel("", SwingConstants.CENTER, MARGIN);
		informacion.setText("App de gestión y Administración");
		informacion.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelInformacion.add(informacion);
		
		// DATOS. CENTRO		
		BorderPanel panelDatos = new VerticalPanelPadding(0, Color.decode("#DDDDDD"), MARGIN);
		panelDatos.setOpaque(false);
		contentPane.add(panelDatos, BorderLayout.CENTER);
		
		BorderPanel panelNombre = new BorderPanel(0, Color.BLACK, 5, false, true);
		panelNombre.setOpaque(false);
		panelNombre.setLayout(new FlowLayout(FlowLayout.LEADING, MARGIN, MARGIN));
		panelDatos.add(panelNombre);
		
		nombre = new HintTextField("USUARIO", SwingConstants.LEADING);
		nombre.setHasPopupMenu(true);
		nombre.setColumns(TEXT_COLUMNS);
		panelNombre.add(nombre, BorderLayout.CENTER);
		
		nombre.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {

				if (e.getKeyCode()==KeyEvent.VK_ENTER) {

					login();
					
				} else {

					comprobar();
				}
			}
		});
		
		desactivado = new LabelOver("/resources/desactivado.png", 30);
		desactivado.setVisible(false);
		panelNombre.add(desactivado, BorderLayout.EAST);
		
		contrasena = new PanelContrasena("CONTRASENA");
		contrasena.setHasBorder(false);
		contrasena.setInnerBorderSize(2);
		contrasena.setColumns(TEXT_COLUMNS);
		contrasena.setOpaque(false);
		panelDatos.add(contrasena);
		
		contrasena.getPassword().addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {

				if (e.getKeyCode()==KeyEvent.VK_ENTER) {

					login();
				}
			}
		});
		
		// BOTON. SUR
		BorderPanelOverSelected comprobar = new BorderPanelOverSelected(2, Color.GRAY, 5, false, true);
		comprobar.setBackgroundColor(Color.decode("#CCCCCC"));
		comprobar.setOverColor(Color.decode("#889977"));
		comprobar.setSelectable(false);
		contentPane.add(comprobar, BorderLayout.SOUTH);
		
		JLabel login = new JLabel("LOGIN");
		login.setHorizontalAlignment(SwingConstants.CENTER);
		login.setFont(new Font("Tahoma", Font.BOLD, 16));
		comprobar.add(login);
		
		comprobar.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
				login();
			}
		});
	}
	
	// SALIR
	private void salir() {
		
		System.exit(0);
	}
	
	// COMPROBAR
	private Thread comprobar;
	private void comprobar() {
		
		if(comprobar != null) {
			
			comprobar.interrupt();
		}
		
		final String nombre = this.nombre.getText();
		if(nombre.length() > 2) {

			desactivado.setIcono(Constantes.CARPETA_RECURSOS + "login/desactivado.png");
			
			comprobar = new Thread(new Runnable() {

				@Override
				public void run() {

					try {

						Thread.sleep(150);
						
						UsuarioDao usuarioDao = new UsuarioDaoImpl();						
						usuario = usuarioDao.login(nombre, parametros);
						
						if(usuario != null) {
							desactivado.setIcono(Constantes.CARPETA_RECURSOS + "login/activado.png");
						}
						
					} catch (RequiredAuthenticationException e) {

						// TODO: GUARDAR EN EL LOG
						e.printStackTrace();
						
					} catch (SOAPException e) {

						// TODO: GUARDAR EN EL LOG
						e.printStackTrace();
						
					} catch (InterruptedException e) {}
				}
			});
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {

					try {
						
						TimeoutController.execute(comprobar, 5000);
						
					} catch (TimeoutException e) {

						desactivado.setIcono(Constantes.CARPETA_RECURSOS + "login/no_conexion.png");
					}
				}
				
			}).start();
		}

		desactivado.setVisible(nombre.length() > 2);
	}
	
	private Usuario usuario;	
	private void login() {
		
		boolean error = (usuario == null);
		
		if(!error) {
			
			String contrasena = this.contrasena.getPassword().getText();
			if(contrasena.equals(usuario.getContrasena())) {
				
				for(OnLoginListener listener : listeners) {
					
					listener.onLogin(usuario);
				}
				
				dispose();
				
			} else {
				
				error = true;
			}
		}
		
		if(error) {

			for(OnLoginListener listener : listeners) {
				
				listener.onError();
			}
		}
	}
	
	public void cambiar(JPanel contentPane) {
		
		// TODO: CAMBIAR CONTENIDO DEL LOGIN, PARA FUNCIONES EXTRA
	}
	
	// LISTENER
	private List<OnLoginListener> listeners;
	
	public void addListener(OnLoginListener listener) {
	
		if(listener != null) {
		
			listeners.add(listener);
		}
	}
	
	public interface OnLoginListener {
		
		public void onLogin(Usuario usuario);
		public void onError();
	}
	
	public static class DatabaseConfiguration {
	
		private String soapEndPoint;
		private String database;
		
		private SOAPUser soapUser;
		
		public DatabaseConfiguration(String soapEndPoint, String database, SOAPUser soapUser) {
			
			this.soapEndPoint = soapEndPoint;
			
			this.database = database;
			this.soapUser = soapUser;
		}

		public String getSoapEndPoint() {
			return soapEndPoint;
		}

		public String getDatabase() {
			return database;
		}

		public SOAPUser getSoapUser() {
			return soapUser;
		}
	}
	
	public static class OnLoginAdapter implements OnLoginListener {

		@Override
		public void onLogin(Usuario usuario) {}

		@Override
		public void onError() {}
	}
 }
package com.repex.librerias.login.entidades;

public class Usuario {
	
	private Integer id;
	
	private String nombre;
	private String contrasena;
	
	public Usuario() { }
	
	public Usuario(Integer id, String nombre, String contrasena) {
		
		this.id = id;
		
		this.nombre = nombre;
		this.contrasena = contrasena;
	}

	public Integer getId() {
		
		return id;
	}

	public void setId(Integer id) {
		
		this.id = id;
	}
	
	public String getNombre() {
		
		return nombre;
	}

	public void setNombre(String nombre) {
		
		this.nombre = nombre;
	}

	public String getContrasena() {
		
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		
		this.contrasena = contrasena;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Usuario other = (Usuario) obj;
		return id == other.id;
	}
	
	@Override
	public Object clone() {
		
		try {
			
			return new Usuario(getId(), new String(getNombre()), new String(getContrasena()));
			
		} catch (Exception e) {}
		
		return null;
	}
	
	public static class Busqueda {
		
		private String nombre;
		
		public Busqueda() {
			
			this.nombre = "";
		}

		public String getNombre() {			
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
	}
}
package com.repex.librerias.horario;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Horario {

	// CONSTANTES
	public final static String SEPARADOR_DIAS = ";";
	public final static String SEPARADOR_HORAS = ",";
	
	public static final int HORARIO_LUNES = 1;
	public static final int HORARIO_MARTES = 2;
	public static final int HORARIO_MIERCOLES = 3;
	public static final int HORARIO_JUEVES = 4;
	public static final int HORARIO_VIERNES = 5;
	public static final int HORARIO_SABADO = 6;
	public static final int HORARIO_DOMINGO = 7;
	
	// VARIABLES
	private Map<Integer, List<Horas>> horarios;
	
	public Horario(String horario) {
		
		this();
		
		String[] split = horario.split(SEPARADOR_DIAS);
		if(split.length == HORARIO_DOMINGO) {
			
			for(int i=0; i<split.length; i++) {
				
				String[] horas = split[i].split(SEPARADOR_HORAS);
				for(String h : horas) {
					
					add(i+1, new Horas(h));
				}
			}
		}
	}
	
	public Horario() {
		
		horarios = new HashMap<Integer, List<Horas>>();
		
		for(int i=HORARIO_LUNES; i<=HORARIO_DOMINGO; i++) {
			
			horarios.put(i, new LinkedList<Horas>());
		}
	}
	
	public void add(int dia, Horas horas) {
		
		if(dia < HORARIO_LUNES || dia > HORARIO_DOMINGO || horarios.get(dia).contains(horas))
			return;
		
		horarios.get(dia).add(horas);
	}
	
	public List<Horas> get(int dia) {

		if(dia < HORARIO_LUNES || dia > HORARIO_DOMINGO)
			return new LinkedList<Horas>();
		
		return horarios.get(dia);
	}
	
	@Override
	public String toString() {
		
		String resultado = "";
		for(Map.Entry<Integer, List<Horas>> entrada : horarios.entrySet()) {
			
			List<Horas> horas = entrada.getValue();
			if(!horas.isEmpty()) {
				
				resultado += horas.get(0).toString();
			}
			
			for(int i=1; i<horas.size(); i++) {
				
				resultado += SEPARADOR_HORAS;
				resultado += horas.get(i).toString();
			}
			
			resultado += SEPARADOR_DIAS;
		}
		
		if(resultado.length() > 0) {
			
			resultado = resultado.substring(0, resultado.length()-1);
		}
		
		return resultado;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Horario other = (Horario) obj;
		return toString().equals(other.toString());
	}
}
package com.repex.librerias.horario.componentes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.JTextComponent;

import com.repex.librerias.components.labels.LabelPanel;
import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.components.popupmenu.MyPopupMenu;
import com.repex.librerias.components.popupmenu.TextFieldPopupMenu;
import com.repex.librerias.components.textfield.HintTextField;
import com.repex.librerias.horario.Horas;
import com.repex.librerias.utilidades.Constantes;

public class PanelDia extends BorderPanel {

	private static final long serialVersionUID = 1L;
	
	// VARIABLES
	private String titulo;
	private int maximo;
	
	// INTERFAZ
	private JPanel opciones;
	
	private List<HintTextField> horas;
	
	public PanelDia(String titulo, int maximo) {

		super(2, Color.GRAY, 3, true, true);
		
		setHasBorder(false);
		
		this.titulo = titulo;
		this.maximo = maximo;
		
		this.horas = new LinkedList<HintTextField>();
		
		inicializar();
	}

	private void inicializar() {
		
		setBackground(Color.WHITE);
		setLayout(new BorderLayout(5, 0));
		
		JPanel titulo = new JPanel();
		titulo.setOpaque(false);
		titulo.setLayout(new FlowLayout(FlowLayout.LEADING));
		add(titulo, BorderLayout.WEST);
		
		JLabel texto = new JLabel(this.titulo);
		titulo.add(texto);
		
		opciones = new JPanel();
		opciones.setOpaque(false);
		opciones.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 0));
		add(opciones, BorderLayout.EAST);

		LabelPanel anadir = new LabelPanel(Constantes.CARPETA_RECURSOS + "horario/nueva.png", 15);
		anadir.setBackgroundColor(Color.LIGHT_GRAY);
		anadir.setShape(new LabelPanel.RoundedShape(new Dimension(10, 10)));
		
		anadir.addAccion(new Runnable() {
			
			public void run() {
				
				if(horas.size() < maximo) {
					
					addHora();
				}
			}
		});
		
		opciones.add(anadir);
		
		addHora();			
	}
	
	private void addHora() {
				
		HintTextField nueva = new HintTextField("00:00-00:00", 2);
		nueva.setHasPopupMenu(true);
		nueva.setPopupMenu(new DiaPopupMenu(this, nueva));
		nueva.setColumns(6);
		
		opciones.add(nueva, opciones.getComponentCount() - 1);
		horas.add(nueva);
		
		revalidate();
		repaint();
	}

	public void addHora(Horas hora) {

		// RECUPERAR ULTIMA HORA Y ASIGNAR
		horas.get(horas.size()-1).setText(hora.toString());
		
		// anadir AL FINAL
		addHora();
	}
	
	private void removeHora(JTextComponent hora) {

		opciones.remove(hora);
		horas.remove(hora);

		revalidate();
		repaint();
	}
	
	public List<Horas> getHoras() {
		
		List<Horas> horas = new LinkedList<Horas>();
		for(HintTextField hora : this.horas) {
			
			Horas h = new Horas(hora.getText());
			if(h.getFin() != null) {
				
				horas.add(h);
			}
		}
		
		return horas;
	}
	
	private class DiaPopupMenu extends TextFieldPopupMenu {
		
		private static final long serialVersionUID = 1L;

		public DiaPopupMenu(final PanelDia contenedor, final JTextComponent parent) {
			
			super(parent);

			List<MyPopupMenu.Opcion> opciones = new LinkedList<MyPopupMenu.Opcion>();
			opciones.add(new MyPopupMenu.OpcionSeparator());
			
			// ELIMINAR
			MyPopupMenu.Opcion eliminar = new MyPopupMenu.Opcion("Eliminar", new Runnable() {
				
				public void run() {
					
					contenedor.removeHora(parent);
				}
			});
			
			opciones.add(eliminar);
			
			eliminar.addCondicion(new Condicion() {
				
				public boolean comprobar() {
					
					try {

						return contenedor.horas.size() > 1;
						
					} catch (Exception e) {}
					
					return false;
				}
			});
			
			this.addOpciones(opciones);
		}
	}
}
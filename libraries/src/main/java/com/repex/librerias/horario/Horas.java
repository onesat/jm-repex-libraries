package com.repex.librerias.horario;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.repex.librerias.date.Fecha;

public class Horas {

	// CONSTANTES
	public final static String SEPARADOR = "-";
	
	public final static Fecha.Formato FORMATO_HORA = new Fecha.Formato() {
		
		public SimpleDateFormat getFormato() {
			
			return new SimpleDateFormat("H:mm");
		}
	};
	
	// FECHAS
	private Fecha inicio;
	private Fecha fin;
	
	public Horas(String horas) {
		
		Fecha inicio = null;
		Fecha fin = null;
		
		try {
		
			inicio = new Fecha(horas.split(SEPARADOR)[0], FORMATO_HORA);
			
		} catch (Exception e) {}

		try {
		
			fin = new Fecha(horas.split(SEPARADOR)[1], FORMATO_HORA);
			
		} catch (Exception e) {}
		
		cargar(inicio, fin);
	}
	
	public Horas(Fecha inicio, Fecha fin) {

		cargar(inicio, fin);
	}
	
	private void cargar(Fecha inicio, Fecha fin) {

		if(inicio == null) {
			
			inicio = new Fecha();
		}
		
		if(fin != null && fin.antes(inicio)) {
			
			fin = inicio.anadir(Calendar.HOUR_OF_DAY, 1);
		}
		
		this.inicio = inicio;
		this.fin = fin;
	}
	
	public Fecha getInicio() {
		
		return inicio;
	}
	
	public void setInicio(Fecha inicio) {
		
		this.inicio = inicio;
	}
	
	public Fecha getFin() {
		
		return fin;
	}
	
	public void setFin(Fecha fin) {

		if(fin != null && fin.antes(inicio)) {
			
			fin = inicio.anadir(Calendar.HOUR_OF_DAY, 1);
		}
		
		this.fin = fin;
	}
	
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Horas other = (Horas) obj;		
		return other.inicio.equals(inicio) && other.fin.equals(fin);
	}
	
	@Override
	public String toString() {
		
		return inicio.parse(FORMATO_HORA) + SEPARADOR + fin.parse(FORMATO_HORA);
	}
}
package com.repex.librerias.horario.componentes;

import java.awt.Dimension;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;

import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.horario.Horario;
import com.repex.librerias.horario.Horas;

public class PanelHorario extends BorderPanel {
	
	private static final long serialVersionUID = 1L;
	
	private List<PanelDia> paneles;
	
	private Estilo estilo;
	private int maximo;
	
	public PanelHorario(Estilo estilo, int maximo) {
		
		super();
		
		this.paneles = new LinkedList<PanelDia>();
		
		this.estilo = estilo;
		this.maximo = maximo;
		
		inicializar();
	}

	public PanelHorario(Estilo estilo) {

		this(estilo, Integer.MAX_VALUE);
	}
	
	public PanelHorario(int maximo) {

		this(Estilo.CORTO, maximo);
	}
	
	public PanelHorario() {
		
		this(Estilo.CORTO, Integer.MAX_VALUE);
	}
	
	private void inicializar() {
		
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		clear();		
	}
	
	public enum Estilo {
		
		CORTO,
		LARGO
	}
	
	private List<String> crearTitulos() {

		List<String> lista = new LinkedList<String>();
		
		switch (estilo) {
		
			case LARGO:

				lista.add("Lunes");
				lista.add("Martes");
				lista.add("Miercoles");
				lista.add("Jueves");
				lista.add("Viernes");
				lista.add("Sabado");
				lista.add("Domingo");
				break;
	
			default:
				
				lista.add("Lu");
				lista.add("Ma");
				lista.add("Mi");
				lista.add("Ju");
				lista.add("Vi");
				lista.add("Sa");
				lista.add("Do");
				break;
		}
		
		return lista;
	}
	
	public Horario getHorario() {
		
		Horario horario = new Horario();
		
		for(int i=0; i<paneles.size(); i++) {
			
			for(Horas horas : paneles.get(i).getHoras()) {

				horario.add(i+1, horas);
			}
		}
		
		return horario;
	}
	
	public void clear() {
		
		removeAll();
		paneles.clear();

		for(String hora : crearTitulos()) {
			
			PanelDia panel = new PanelDia(hora, maximo);
			panel.setMaximumSize(new Dimension(2000, 38));
			
			add(panel);
			paneles.add(panel);
		}
		
		revalidate();
		repaint();
	}
	
	public void addHora(int dia, Horas hora) {
		
		if(dia >= Horario.HORARIO_LUNES && dia < paneles.size() && dia < Horario.HORARIO_DOMINGO) {
			
			paneles.get(dia-1).addHora(hora);
		}
	}
}
package com.repex.librerias.ftp.control;

import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;

import com.repex.librerias.ftp.entidades.Tipo;
import com.repex.librerias.utilidades.Constantes;
import com.repex.librerias.utilidades.IconUtils;

public class AtachmentController {

	public static String DESCONOCIDO = "desconocido";

	public static String CARPETA = "carpeta";

	public static String TEXTO = "txt";
	public static String PDF = "pdf";
	public static String PNG = "png";
	public static String JPG = "jpg";
	public static String JPEG = "jpeg";
	
	// SINGLETON
	private static AtachmentController singleton;
	
	// CONSTANTES
	public static final String CARPETA_RECURSOS = Constantes.CARPETA_RECURSOS + "ftp/";
	
	private static final int TAMANO = 20;
	
	private AtachmentController() {
		
		tipos = new HashMap<String, Tipo>();

		// VALORES POR DEFECTO
		Tipo desconocido = new Tipo(IconUtils.escalar(getClass().getClassLoader().getResource(CARPETA_RECURSOS + "desconocido.png")), DESCONOCIDO);

		Tipo carpeta = new Tipo(IconUtils.escalar(getClass().getClassLoader().getResource(CARPETA_RECURSOS + "carpeta.png")), CARPETA);

		Tipo texto = new Tipo(IconUtils.escalar(getClass().getClassLoader().getResource(CARPETA_RECURSOS + "texto.png")), TEXTO);
		Tipo pdf = new Tipo(IconUtils.escalar(getClass().getClassLoader().getResource(CARPETA_RECURSOS + "pdf.png")), PDF);
		Tipo imagen = new Tipo(IconUtils.escalar(getClass().getClassLoader().getResource(CARPETA_RECURSOS + "imagen.png")), PNG, JPG, JPEG);
		
		// ANADIR
		for(String id : desconocido.getIdentificadores()) {

			tipos.put(id, new Tipo(desconocido));
		}

		for(String id : carpeta.getIdentificadores()) {

			tipos.put(id, new Tipo(carpeta));
		}
		
		for(String id : texto.getIdentificadores()) {

			tipos.put(id, new Tipo(texto));
		}

		for(String id : pdf.getIdentificadores()) {

			tipos.put(id, new Tipo(pdf));
		}

		for(String id : imagen.getIdentificadores()) {

			tipos.put(id, new Tipo(imagen));
		}
		
		// CAMBIAR TAMANO
		setSize(TAMANO);
	}
	
	public static AtachmentController getSingleton() {
		
		if(singleton == null) {
			
			singleton = new AtachmentController();
		}
		
		return singleton;
	}
	
	// MAPA TIPOS
	private static Map<String, Tipo> tipos; // SOLO CUANDO NO SON CARPETAS
	
	/**
	 * Cambia el tamano por defecto de todos los tipos almacenados en tiempo de ejecucion
	 * @param size
	 */
	public void setSize(int size) {
				
		for (Map.Entry<String, Tipo> entry : tipos.entrySet()) {
			
			entry.getValue().setTamano(size);
		}
	}
	
	/**
	 * Cambia el tamano por defecto de todos los tipos almacenados en tiempo de ejecucion que corresponden a los identificadores
	 * @param size
	 * @param identificadores
	 */
	public void setSize(int size, String... identificadores) {
		
		for(String id : identificadores) {
			
			Tipo tipo = tipos.get(id);
			if(tipo != null) {
				
				tipo.setTamano(size);
			}
		}
	}
	
	public int getSize() {

		int size = 0;
		
		for (Map.Entry<String, Tipo> entry : tipos.entrySet()) {
			
			size = Math.max(size, entry.getValue().getTamano());
		}
		
		return size;
	}
	
	public Tipo get(String identificador) {
		
		Tipo tipo = tipos.get(identificador);
		
		if(tipo == null) {
			
			return tipos.get(DESCONOCIDO);
		}
		
		return tipo;
	}
	
	/**
	 * Almacena un nuevo tipo, si ya existe uno con ese identificador lo sobrescribe
	 * @param tipo
	 */
	public void add(Tipo tipo) {

		for(String id : tipo.getIdentificadores()) {

			tipos.put(id, new Tipo(tipo));
		}
	}

	/**
	 * Cambia el icono de un tipo almacenado
	 * @param identificador
	 * @param icono
	 * @return True si ha cambiado el icono, False si no existe el tipo
	 */
	public boolean override(String identificador, Icon icono, int tamano) {

		Tipo tipo = tipos.get(identificador);
		
		if(tipo == null) {
			
			return false;
		}
		
		tipo.setOriginal(icono);
		
		if(tamano > 0) {
			
			tipo.setTamano(tamano);
		}
		
		return true;
	}
	
	public boolean override(String identificador, Icon icono) {

		return this.override(identificador, icono, 0);
	}
}
package com.repex.librerias.ftp.entidades;

import java.io.File;

public class Upload {

	private File file;
	private String nombre;
	
	public Upload(File file, String nombre) {
		
		this.file = file;
		this.nombre = nombre;
	}
	
	public File getFile() {
		
		return file;
	}
	
	public String getNombre() {
		
		return nombre;
	}
}
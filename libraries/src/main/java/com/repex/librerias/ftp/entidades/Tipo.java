package com.repex.librerias.ftp.entidades;

import java.util.Arrays;

import javax.swing.Icon;

import com.repex.librerias.utilidades.IconUtils;

public class Tipo {
		
	// VARIABLES
	private Icon original;	
	private int tamano;

	private String[] identificadores;
	
	public Tipo(Icon icono, int tamano, String... identificadores) {

		this.original = icono;		
		this.tamano = tamano;
		
		this.identificadores = identificadores;		
	}

	public Tipo(Icon icono, String... identificadores) {

		this(icono, 0, identificadores);		
	}
	
	public Tipo(Tipo other) {
	
		this(other.getIcono(), other.getTamano(), other.getIdentificadores());
	}
	
	public Icon getIcono() {
		
		return IconUtils.escalar(original, tamano);
	}
	
	public Icon getOriginal() {
		
		return original;
	}
	
	public void setOriginal(Icon original) {
		
		this.original = original;
	}
	
	public int getTamano() {
		
		return tamano;
	}
	
	public void setTamano(int tamano) {
		
		this.tamano = tamano;
	}
	
	public String[] getIdentificadores() {
		
		return identificadores;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(identificadores);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Tipo other = (Tipo) obj;
		return Arrays.equals(identificadores, other.identificadores);
	}
}
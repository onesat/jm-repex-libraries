package com.repex.librerias.ftp.control;

import java.util.HashMap;
import java.util.Map;

public class FTPController {

	// CONSTANTES
	public static int DEFAULT_INDEX = 0;
	
	//
	private static FTPController singleton;
	
	private FTPController() {
		
		conexiones = new HashMap<Integer, FTPConection>();
	}
	
	public static FTPController getSingleton() {
	
		if(singleton == null) {
		
			singleton = new FTPController();
		}
		
		return singleton;
	}
	
	private Map<Integer, FTPConection> conexiones;

	// CONEXION POR DEFECTO
	public FTPConection getDefault() {
		
		return getConexion(DEFAULT_INDEX);
	}
	
	public void setDefault(FTPConection conexion) {
		
		addConexion(DEFAULT_INDEX, conexion);		
	}
	
	// OTRAS CONEXIONES
	public void addConexion(int id, FTPConection conexion) {
		
		this.conexiones.put(id, conexion);
	}
	
	public FTPConection getConexion(int id) {
		
		return this.conexiones.get(id);
	}
	
	public boolean removeConexion(int id) {
		
		return this.conexiones.remove(id) != null;
	}
}
package com.repex.librerias.ftp.entidades;

public interface Carpeta {
	
	public String getCarpetaLocal();	
	public String getCarpetaFtp();	
	public String getCarpetaPc();
}
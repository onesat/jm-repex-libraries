package com.repex.librerias.ftp.control;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.io.CopyStreamListener;

import com.repex.librerias.date.Fecha;
import com.repex.librerias.ftp.entidades.Adjunto;
import com.repex.librerias.ftp.entidades.Upload;

public class FTPConection {
		
	private String servidor;
	private String usuario;
	private String password;
	
	private String carpeta;
	
	private FTPClient client;
	
	public FTPConection(String servidor, String usuario, String password, String carpeta, boolean persistant) {
		
		this.servidor = servidor;
		this.usuario = usuario;
		this.password = password;
		
		this.carpeta = carpeta;
		
		this.persistant = persistant;

		// INICIALIZAR CLIENTE
		this.client = new FTPClient();
	}

	public FTPConection(String servidor, String usuario, String password, String carpeta) {
		
		this(servidor, usuario, password, carpeta, true);
	}
	
	public FTPConection(String servidor, String usuario, String password, boolean persistant) {
		
		this(servidor, usuario, password, "", persistant);
	}

	public FTPConection(String servidor, String usuario, String password) {
		
		this(servidor, usuario, password, true);
	}
	
	// VARIABLES
	private Thread hilo;
	private boolean persistant = true;
	
	public boolean isPersistant() {
		
		return persistant;
	}
	
	public void setPersistant(boolean persistant) {
		
		this.persistant = persistant;
	}
	
	private boolean conectar() throws NotFtpException, UnreachableFtpException {
		
		if(servidor.equals("") || usuario.equals("") || password.equals("")) {
			
			throw new NotFtpException("Not FTP Server selected. Use cargar from FTPConection first");
		}
		
		try {
			
			client.connect(servidor);
			
			if(!FTPReply.isPositiveCompletion(client.getReplyCode())) {

				throw new UnreachableFtpException("Error conecting to this FTP Server");
			}

			if(isPersistant()) {

				hilo = new Thread(new Control());
				hilo.start(); // HILO DE CONTROL
			}

			if(!client.login(usuario, password)) {

				throw new UnreachableFtpException("Error conecting to this FTP Server");
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return true;
	}
	
	// FUNCIONES
	// LISTAR FICHEROS. OK
	public List<Adjunto> listFiles() throws NotFtpException, UnreachableFtpException {
		
		return listFiles("");
	}
	
	public List<Adjunto> listFiles(String destino) throws NotFtpException, UnreachableFtpException {
		
		List<Adjunto> resultados = new LinkedList<Adjunto>();

		if(!client.isConnected()) {
		
			conectar();
		}

		continuar = true;
        client.enterLocalPassiveMode();
        
		try {
			     
			for (FTPFile file : client.listFiles(comprobar(destino))) {
				
				if(!file.getName().equals(".")) {
					
					Adjunto adjunto = null;
					if(file.isDirectory()) {

						adjunto = new Adjunto(destino, file.getName(), AtachmentController.getSingleton().get(AtachmentController.CARPETA), new Fecha(file.getTimestamp().getTime()), file.getSize());

					} else {

						adjunto = new Adjunto(destino, file.getName(), new Fecha(file.getTimestamp().getTime()), file.getSize());
					}

					resultados.add(adjunto);
				}
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return new LinkedList<Adjunto>();
			
		} finally {
			
			// DESCONECTAR
			if(!isPersistant()) {
				
				try {

					desconectar();
					
				} catch (Exception e2) {}
			}
		}
		
		// ORDENAR CON CARPETAS PRIMERO
		List<Adjunto> ordenado = new LinkedList<Adjunto>();
		for(int i=resultados.size()-1; i>=0; i--) {
			
			Adjunto adjunto = resultados.get(i);
			
			if(adjunto.getTipo() == AtachmentController.getSingleton().get(AtachmentController.CARPETA)) {
				
				ordenado.add(0, adjunto);
				
			} else {
			
				ordenado.add(ordenado.size(), adjunto);
			}			
		}
		
		return ordenado;
	}
	
	// SUBIR FICHERO. OK
	public boolean uploadFile(String destino, Upload fichero, CopyStreamListener listener) throws NotFtpException, UnreachableFtpException {
		
		if(!client.isConnected()) {
			
			if(!conectar()) {
				
				return false;
			}
		}

		continuar = true;
        client.enterLocalPassiveMode();
        
		try {
			
			if(listener != null) {
				
				client.setCopyStreamListener(listener);
			}
	        
	        // FICHERO RESULTADO	        
	        String resultado = comprobar(destino)+fichero.getNombre();
	        String extension = fichero.getFile().getAbsolutePath();
	        extension = extension.substring(extension.lastIndexOf(".") + 1);
	        
	        if(!resultado.endsWith(extension)) {
	        	
	        	resultado = resultado + "." + extension;
	        }
	        
	        // LEER FICHERO
			BufferedInputStream buffIn = new BufferedInputStream(new FileInputStream(fichero.getFile()));
			client.setFileType(FTP.BINARY_FILE_TYPE);
			
			// GUARDAR
			client.storeFile(resultado, buffIn);
			
			buffIn.close();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return false;
			
		} finally {
			
			// DESCONECTAR
			if(!isPersistant()) {
				
				try {

					desconectar();
					
				} catch (Exception e2) {}
			}
		}
		
		return true;
	}

	// SUBIR FICHEROS. OK
	public boolean uploadFiles(String destino, List<Upload> ficheros, CopyStreamListener listener) throws NotFtpException, UnreachableFtpException {

		// CAMBIAR PERSISTENCIA
		boolean p = isPersistant();
		setPersistant(true);
		
		// SUBIR UNO A UNO
		boolean resultado = true;		
		for(Upload fichero : ficheros) {
			
			resultado = resultado && uploadFile(destino, fichero, listener);
		}
		
		// VOLVER A PONER PERSISTENCIA
		setPersistant(p);
		
		// DESCONECTAR
		if(!isPersistant()) {
			
			try {

				desconectar();
				
			} catch (Exception e2) {}
		}
		
		return resultado;
	}

	public boolean uploadFiles(String destino, List<Upload> ficheros, List<CopyStreamListener> listener) throws NotFtpException, UnreachableFtpException {

		// CAMBIAR PERSISTENCIA
		boolean p = isPersistant();
		setPersistant(true);
		
		// SUBIR UNO A UNO
		boolean resultado = true;
		for(int i=0; i<ficheros.size(); i++) {
			
			Upload fichero = ficheros.get(i);
			CopyStreamListener l = null;
			if(i < listener.size()) {
				
				l = listener.get(i);
			}
			
			resultado = resultado && uploadFile(destino, fichero, l);
		}
		
		// VOLVER A PONER PERSISTENCIA
		setPersistant(p);
		
		// DESCONECTAR
		if(!isPersistant()) {
			
			try {

				desconectar();
				
			} catch (Exception e2) {}
		}
		
		return resultado;
	}
	
	// DESCARGAR. OK
	/**
	 * 
	 * @param ruta Donde se guarda el fichero en el ordenador
	 * @param location Ruta desde la que lo tiene que descargar
	 * @param nombre Nombre del fichero
	 * @param listener
	 * @return
	 */
	public File downloadFile(String ruta, String destino, String nombre, CopyStreamListener listener) throws NotFtpException, UnreachableFtpException {
		
		if(!client.isConnected()) {
			
			if(!conectar()) {
				
				return null;
			}
		}

		continuar = true;
        client.enterLocalPassiveMode();
        
		try {
			
	        client.setFileType(FTP.BINARY_FILE_TYPE);
	        
			if(listener != null) {
				
				client.setCopyStreamListener(listener);
			}
	        
	        // FICHERO PARA LEER	        
	        String fichero = comprobar(destino)+nombre;
	        
			// COMPROBAR SI EXISTE DIRECTORIO EN PC
			File directorio = new File(ruta);
			if(!directorio.exists()) {
				
				directorio.mkdirs();
			}
			
			// SUBIR FICHERO
	        File downloadFile = new File(directorio, nombre);
	        OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile));
	        if(client.retrieveFile(fichero, outputStream)) {
	        	
		        outputStream.close();				
		        return downloadFile;
	        }
	        
	        // ELIMINAR FICHERO
	        downloadFile.delete();
	        
		} catch (Exception e) {
			
			e.printStackTrace();
			
		} finally {
			
			// DESCONECTAR
			if(!isPersistant()) {
				
				try {

					desconectar();
					
				} catch (Exception e2) {}
			}
		}
		
		return null;
	}

	// DESCARGA MULTIPLE. OK
	public List<File> downloadFiles(String ruta, String destino, List<String> ficheros, CopyStreamListener listener) throws NotFtpException, UnreachableFtpException {

		// CAMBIAR PERSISTENCIA
		boolean p = isPersistant();
		setPersistant(true);
		
		// DESCARGAR UNO A UNO
		List<File> resultado = new LinkedList<File>();
		for(String fichero : ficheros) {
			
			File f = downloadFile(ruta, destino, fichero, listener);
			if(f != null) {
				
				resultado.add(f);
			}
		}
		
		// VOLVER A PONER PERSISTENCIA
		setPersistant(p);
		
		// DESCONECTAR
		if(!isPersistant()) {
			
			try {

				desconectar();
				
			} catch (Exception e2) {}
		}
		
		return resultado;
	}
	
	public List<File> downloadFiles(String ruta, String destino, List<String> ficheros, List<CopyStreamListener> listener) throws NotFtpException, UnreachableFtpException {

		// CAMBIAR PERSISTENCIA
		boolean p = isPersistant();
		setPersistant(true);
		
		// DESCARGAR UNO A UNO
		List<File> resultado = new LinkedList<File>();
		for(int i=0; i<ficheros.size(); i++) {
			
			String fichero = ficheros.get(i);
			CopyStreamListener l = null;
			if(i < listener.size()) {
				
				l = listener.get(i);
			}
			
			File f = downloadFile(ruta, destino, fichero, l);
			if(f != null) {
				
				resultado.add(f);
			}
		}
		
		// VOLVER A PONER PERSISTENCIA
		setPersistant(p);
		
		// DESCONECTAR
		if(!isPersistant()) {
			
			try {

				desconectar();
				
			} catch (Exception e2) {}
		}
			
		return resultado;
	}
	
	// RENOMBRAR. OK
	public boolean renameFile(String destino, String original, String modificado) throws NotFtpException, UnreachableFtpException {
		
		if(!client.isConnected()) {
			
			if(!conectar()) {
				
				return false;
			}
		}

		continuar = true;
        client.enterLocalPassiveMode();
        
		try {

			// COMPROBAR EXTENSION
			String extension = original.substring(original.lastIndexOf(".") + 1);
			if(!extension.equals("") && !modificado.endsWith(extension))
				return false;
				        	        
	        // FICHERO PARA LEER	        
	        String antiguo = comprobar(destino)+original;
	        String nuevo = comprobar(destino)+modificado;
	        
	        return client.rename(antiguo, nuevo);
	        
		} catch (Exception e) {
			
			e.printStackTrace();
			
		} finally {
			
			// DESCONECTAR
			if(!isPersistant()) {
				
				try {

					desconectar();
					
				} catch (Exception e2) {}
			}
		}
		
		return false;
	}
	
	public boolean renameDirectory(String destino, String original, String modificado) throws NotFtpException, UnreachableFtpException {
		
		if(!client.isConnected()) {
			
			if(!conectar()) {
				
				return false;
			}
		}

		continuar = true;
        client.enterLocalPassiveMode();
        
		try {
	        	        
	        // FICHERO PARA LEER	        
	        String antiguo = comprobar(destino)+original;
	        String nuevo = comprobar(destino)+modificado;
	        
	        return client.rename(antiguo, nuevo);
	        
		} catch (Exception e) {
			
			e.printStackTrace();
			
		} finally {
			
			// DESCONECTAR
			if(!isPersistant()) {
				
				try {

					desconectar();
					
				} catch (Exception e2) {}
			}
		}
		
		return false;
	}
	
	// ELIMINAR. OK
	public boolean removeFile(String destino, String nombre) throws NotFtpException, UnreachableFtpException {
		
		if(!client.isConnected()) {
			
			if(!conectar()) {
				
				return false;
			}
		}

		continuar = true;
        client.enterLocalPassiveMode();
        
		try {

	        // FICHERO PARA ELIMINAR	        
	        String fichero = comprobar(destino)+nombre;			
	        return client.deleteFile(fichero);
			
		} catch (Exception e) {
			
			e.printStackTrace();
		
		} finally {
			
			// DESCONECTAR
			if(!isPersistant()) {
				
				try {

					desconectar();
					
				} catch (Exception e2) {}
			}
		}
		
		return false;	
	}

	// ELIMINAR VARIOS. OK
	public boolean removeFiles(String destino, List<String> nombres) throws NotFtpException, UnreachableFtpException {

		// TODO: HACER PARA INDICAR ERROR EN LOS FICHEROS INDIVIDUALES
		
		// CAMBIAR PERSISTENCIA
		boolean p = isPersistant();
		setPersistant(true);
		
		// ELIMINAR UNO A UNO
		boolean resultado = true;
		for(String nombre : nombres) {
		
			resultado = resultado && removeFile(destino, nombre);
		}
		
		// VOLVER A PONER PERSISTENCIA
		setPersistant(p);
		
		// DESCONECTAR
		if(!isPersistant()) {
			
			try {

				desconectar();
				
			} catch (Exception e2) {}
		}
		
		return resultado;
	}

	// ELIMINAR DIRECTRIO. OK
	public boolean removeDirectory(String destino) throws NotFtpException, UnreachableFtpException {
		
		// CAMBIAR PERSISTENCIA
		boolean p = isPersistant();
		setPersistant(true);
		
		try {
			
			// ELIMINAR
			for(Adjunto adjunto : listFiles(destino)) {
				
				if(!adjunto.isPrincipal()) {
					
					if(adjunto.getTipo().equals(AtachmentController.getSingleton().get(AtachmentController.CARPETA))) {

						// ELIMINAR CARPETA
						String nuevo = destino + "/" + adjunto.getFichero() + "/"; 
						if(!removeDirectory(nuevo)) {

							return false;
						}

					} else {

						// ELIMINAR FICHERO
						if(!removeFile(destino, adjunto.getFichero())) {

							return false;
						}
					}
				}
			}
			
			return client.removeDirectory(comprobar(destino));
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		} finally {
			
			// VOLVER A PONER PERSISTENCIA
			setPersistant(p);
			
			// DESCONECTAR
			if(!isPersistant()) {
				
				try {

					desconectar();
					
				} catch (Exception e2) {}
			}
		}
		
		return false;
	}
	
	// DIRECTORIOS
	public boolean makeDirectory(String destino) throws NotFtpException, UnreachableFtpException {
		
		if(!client.isConnected()) {
			
			if(!conectar()) {
				
				return false;
			}
		}

		continuar = true;
        client.enterLocalPassiveMode();
        
		try {       

            String[] carpetas = comprobar(destino).split("/");
            for(int i=0; i<carpetas.length; i++) {
            
            	String carpeta = carpetas[i];
            	
            	if(!carpeta.equals("") && !client.changeWorkingDirectory(carpeta)) {
            		
        	        boolean creado = client.makeDirectory(carpeta);
        	        if(!creado) {
        	        	
        	        	return creado;
        	        }
        	        
        	        client.changeWorkingDirectory(carpeta);
            	}
            }

			
		} catch (Exception e) {
			
			return false;
			
		} finally {

			try {

				// VOLVER AL DIRECTORIO PRINCIPAL
				client.changeWorkingDirectory("/");
				client.changeWorkingDirectory(carpeta);
				
				if(!isPersistant()) {
					
					desconectar();
				}
				
			} catch (Exception e2) {}
		}
		
		return true;		
	}
	
	public boolean checkDirectory(String location) throws NotFtpException, UnreachableFtpException {
		
		if(!client.isConnected()) {
			
			if(!conectar()) {
				
				return false;
			}
		}

		continuar = true;
        client.enterLocalPassiveMode();
        
		try {
			
			// TODO: HAY QUE TENER EN CUENTA LA CARPETA BASE
			client.changeWorkingDirectory(comprobar(location));			
			return (client.getReplyCode() != 550);

		} catch (Exception e) {
			
			e.printStackTrace();
			
		} finally {

			try {

				// VOLVER AL DIRECTORIO PRINCIPAL
				client.changeWorkingDirectory("/");

				if(!isPersistant()) {
					
					desconectar();
				}
				
			} catch (Exception e2) {}
		}
		
		return false;
	}
	
	// CONTROL DE CONEXION
	private int connectionTime = 120; // SEGUNDOS QUE MANTIENE LA CONEXION
	private boolean continuar = false;
	
	public void setConnectionTime(int connectionTime) {
		
		this.connectionTime = connectionTime;
	}
	
	private class Control implements Runnable {
		
		public void run() {

			try {
				
				do {

					continuar = false;
					Thread.sleep(connectionTime*1000);

				} while (continuar);

				// DESCONECTAR
				desconectar();
				
			} catch (Exception e) {
				
				System.out.println("CIERRE DE CONEXION ANTES DE TIEMPO");
			}
		}
	}
	
	private void desconectar() throws IOException {

		if(client != null) {

			client.disconnect();
			client.logout();
		}
		
		if(hilo != null) {
			
			hilo.interrupt();
			hilo = null;
		}
	}
	
	// AUXILIAR
	private String comprobar(String url) {
		
		if(!url.startsWith("/")) 
			url = "/" + url;
		
		url = url.replaceAll("//", "/");
		
		return carpeta + url;
	}
	
	// CLASES
	public class NotFtpException extends Exception {
		
		private static final long serialVersionUID = 1L;

		public NotFtpException() {
			
			super();
		}

		public NotFtpException(String message, Throwable cause) {
			
			super(message, cause);
		}

		public NotFtpException(String message) {
			
			super(message);
		}

		public NotFtpException(Throwable cause) {
			
			super(cause);
		}
	}

	public class UnreachableFtpException extends Exception {
		
		private static final long serialVersionUID = 1L;

		public UnreachableFtpException() {
			
			super();
		}

		public UnreachableFtpException(String message, Throwable cause) {
			
			super(message, cause);
		}

		public UnreachableFtpException(String message) {
			
			super(message);
		}

		public UnreachableFtpException(Throwable cause) {
			
			super(cause);
		}
	}
}
package com.repex.librerias.ftp.componentes;

import java.util.LinkedList;
import java.util.List;

import com.repex.librerias.components.popupmenu.MyPopupMenu;
import com.repex.librerias.ftp.control.AtachmentController;
import com.repex.librerias.utilidades.ClipboardController;

public class AdjuntoPopupMenu extends MyPopupMenu {
	
	private static final long serialVersionUID = 1L;

	public AdjuntoPopupMenu(PanelAdjunto parent, List<Opcion> opciones) {

		super(parent, opciones);		
		opciones.addAll(0, crearOpciones(parent));
	}

	protected List<Opcion> crearOpciones(final PanelAdjunto parent) {
		
		List<Opcion> principales = new LinkedList<Opcion>();
				
		// COPIAR URL
		principales.add(new MyPopupMenu.Opcion("Copiar url", new Runnable() {
			
			@Override
			public void run() {
				
				ClipboardController.copiar(parent.getAdjunto().getRuta());
			}
		}));

		principales.add(new MyPopupMenu.OpcionSeparator());
		
		// CONDICIONES
		MyPopupMenu.Condicion carpeta = new MyPopupMenu.Condicion() {
			
			@Override
			public boolean comprobar() {
				
				return parent.getAdjunto().getTipo() != AtachmentController.getSingleton().get(AtachmentController.CARPETA);
			}
		};

		MyPopupMenu.Condicion principal = new MyPopupMenu.Condicion() {
			
			@Override
			public boolean comprobar() {
				
				return !parent.getAdjunto().isPrincipal();
			}
		};
		
		// ACTUALIZAR
		MyPopupMenu.Opcion actualizar = new MyPopupMenu.Opcion("Actualizar", new Runnable() {
			
			@Override
			public void run() {
				
				parent.actualizar();
			}
		});
		
		actualizar.addCondicion(carpeta);		
		principales.add(actualizar);

		// DESCARGAR
		MyPopupMenu.Opcion descargar = new MyPopupMenu.Opcion("Descargar", new Runnable() {
			
			@Override
			public void run() {
				
				parent.descargar();
			}
		});
		
		descargar.addCondicion(carpeta);
		principales.add(descargar);

		// BORRAR
		MyPopupMenu.Opcion eliminar = new MyPopupMenu.Opcion("Eliminar", new Runnable() {
			
			@Override
			public void run() {
				
				parent.eliminar();
			}
		});
		
		eliminar.addCondicion(principal);
		principales.add(eliminar);

		// BARRA SEPARADORA
		principales.add(new MyPopupMenu.OpcionSeparator());

		// RENOMBRAR
		MyPopupMenu.Opcion renombrar = new MyPopupMenu.Opcion("Cambiar nombre", new Runnable() {
			
			@Override
			public void run() {
				
				parent.renombrar();
			}
		});
		
		renombrar.addCondicion(principal);
		principales.add(renombrar);
		
		return principales;
	}
	
	public AdjuntoPopupMenu(PanelAdjunto parent) {

		this(parent, new LinkedList<Opcion>());
	}
}
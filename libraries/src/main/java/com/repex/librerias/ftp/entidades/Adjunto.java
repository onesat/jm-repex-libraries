package com.repex.librerias.ftp.entidades;

import com.repex.librerias.date.Fecha;
import com.repex.librerias.ftp.control.AtachmentController;

public class Adjunto {
	
	private String directorio;
	private String fichero;
	
	private Tipo tipo;
	
	private Fecha fecha;
	
	private long tamano;
	
	public Adjunto(String directorio, String fichero, Tipo tipo, Fecha fecha, long tamano) {

		this.fichero = fichero.substring(fichero.lastIndexOf("/")+1);
		this.directorio = directorio.replaceAll(this.fichero, "").replaceAll("//", "/");
		
		this.tipo = tipo;
	
		this.fecha = fecha;
		
		this.tamano = tamano;

		if(tipo == null) {
			
			try {

				String sufijo = fichero.substring(fichero.lastIndexOf(".") + 1);
				this.tipo = AtachmentController.getSingleton().get(sufijo);
				
			} catch (Exception e) {

				this.tipo = AtachmentController.getSingleton().get(AtachmentController.DESCONOCIDO);				
			}
		}
	}
	
	public Adjunto(String directorio, String fichero, Fecha fecha, long tamano) {
		
		this(directorio, fichero, null, fecha, tamano);
	}

	public String getDirectorio() {
		
		return directorio;
	}
	
	public String getFichero() {
		
		return fichero;
	}

	public void setFichero(String fichero) {
		
		this.fichero = fichero;
	}
	
	public String getNombre() {
	
		if(!fichero.contains("."))
			return fichero;
		
		return fichero.substring(0, fichero.lastIndexOf("."));
	}
	
	public String getExtension() {
	
		if(!fichero.contains("."))
			return "";
		
		return fichero.substring(fichero.lastIndexOf(".")+1);
	}
	
	public String getRuta() {
		
		String ruta = ("/" + directorio + "/" + fichero);
		while(ruta.contains("//"))
			ruta = ruta.replaceAll("//", "/");
		
		return ruta;	
	}
	
	public Tipo getTipo() {
	
		return tipo;
	}
	
	public Fecha getFecha() {
		
		return fecha;
	}

	public void setFecha(Fecha fecha) {
		
		this.fecha = fecha;
	}

	public double getTamano() {
		
		return getTamano(KILO_BYTES);
	}
	
	public double getTamano(SistemaDecimal sistema) {
		
		return tamano/sistema.divisor();
	}
	
	public void setTamano(long tamano) {
		
		this.tamano = tamano;
	}
	
	public boolean isPrincipal() {
		
		return fichero.equals(".") || fichero.equals("..");
	}
	
	public interface SistemaDecimal {
		
		public int divisor();
	}

	public static SistemaDecimal BYTES = new SistemaDecimal() {
		
		@Override
		public int divisor() {
			
			return 1;
		}
	};
	
	public static SistemaDecimal KILO_BYTES = new SistemaDecimal() {
		
		@Override
		public int divisor() {
			
			return 1024;
		}
	};
}
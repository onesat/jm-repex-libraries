package com.repex.librerias.ftp.componentes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import org.apache.commons.net.io.CopyStreamEvent;
import org.apache.commons.net.io.CopyStreamListener;

import com.repex.librerias.components.labels.LabelPanel;
import com.repex.librerias.components.paneles.BorderPanelOverSelected;
import com.repex.librerias.components.popupmenu.MyPopupMenu;
import com.repex.librerias.components.textfield.HintTextField;
import com.repex.librerias.date.Fecha;
import com.repex.librerias.ftp.control.AtachmentController;
import com.repex.librerias.ftp.control.FTPConection;
import com.repex.librerias.ftp.control.FTPController;
import com.repex.librerias.ftp.entidades.Adjunto;

public class PanelAdjunto extends BorderPanelOverSelected {

	private static final long serialVersionUID = 1L;
	
	// CONSTANTES
	private final static int LONGITUD_MAXIMA_NOMBRE = 20;
	
	// VARIABLES
	private FTPConection conexion;
	
	private Adjunto adjunto;

	private boolean hasPopupMenu = false;	
	private MyPopupMenu popupMenu;

	private LabelPanel.RoundedShape shape = new LabelPanel.RoundedShape(new Dimension(10, 10));
	
	// INTERFAZ
	private JLabel tamano;	
	private JLabel fecha;
	
	public PanelAdjunto(FTPConection conexion, Adjunto adjunto, boolean hasPopupMenu) {
	
		super();
		
		this.conexion = conexion;
		
		this.adjunto = adjunto;		
		this.hasPopupMenu = hasPopupMenu;

		// POPUP MENU
		popupMenu = new AdjuntoPopupMenu(this);
		
		super.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				
				if(SwingUtilities.isRightMouseButton(e) && PanelAdjunto.this.hasPopupMenu && popupMenu != null) {
					
					// MOSTAR MENU POPUP
					popupMenu.crear();
				}
				
				super.mouseReleased(e);
			}
		});
		
		inicializar();
	}

	public PanelAdjunto(Adjunto adjunto, boolean hasPopupMenu) {
		
		this(FTPController.getSingleton().getDefault(), adjunto, hasPopupMenu);
	}
	
	public PanelAdjunto(Adjunto adjunto) {
		
		this(adjunto, true);
	}
	
	public Adjunto getAdjunto() {
		
		return adjunto;
	}

	// POPUP MENU
	public boolean hasPopupMenu() {
		
		return hasPopupMenu;
	}
	
	public void setHasPopupMenu(boolean hasPopupMenu) {
		
		this.hasPopupMenu = hasPopupMenu;
	}
	
	public void setPopupMenu(MyPopupMenu popupMenu) {
		
		this.popupMenu = popupMenu;
	}
	
	private void inicializar() {

		setSelectable(true);
		setOverable(true);
		
		// QUITAR ANTERIOR
		removeAll();
		
		// TAMANO MAXIMO
		int size = AtachmentController.getSingleton().getSize();
		
		Dimension d = new Dimension(2000, size + 20);
		setMaximumSize(d);

		// CONFIGURACION
		setLayout(new BorderLayout(0, 0));
		
		// INFORMACION PRINCIPAL
		JPanel panelInformacion = new JPanel();
		panelInformacion.setOpaque(false);
		panelInformacion.setLayout(new FlowLayout(FlowLayout.LEADING, 10, 5));
		add(panelInformacion, BorderLayout.WEST);
		
		JLabel imagen = new JLabel();
		imagen.setIcon(adjunto.getTipo().getIcono());
		panelInformacion.add(imagen);
		
		String nombre = adjunto.getFichero();
		if(nombre.length() > LONGITUD_MAXIMA_NOMBRE) {
		
			nombre = nombre.substring(0, LONGITUD_MAXIMA_NOMBRE) + "...";
		}
		
		JLabel fichero = new JLabel(nombre);
		panelInformacion.add(fichero);
		
		// OPCIONES
		JPanel panelOpciones = new JPanel();
		panelOpciones.setOpaque(false);
		panelOpciones.setLayout(new FlowLayout(FlowLayout.TRAILING, 10, 10));
		add(panelOpciones, BorderLayout.EAST);
		
		int tamanoOpcion = Math.min(20, size-5);
		
		if(!adjunto.getTipo().equals(AtachmentController.getSingleton().get(AtachmentController.CARPETA))) {

			tamano = new JLabel();
			panelInformacion.add(tamano);
			
			updateTamano();

			fecha = new JLabel();
			panelInformacion.add(fecha);
			
			updateFecha();
			
			LabelPanel descargar = new LabelPanel(AtachmentController.CARPETA_RECURSOS + "adjuntos/descargar.png", tamanoOpcion);
			descargar.setOverColor(Color.decode("#ffee7c"));
			descargar.setShape(shape);
			panelOpciones.add(descargar);

			descargar.addAccion(new Runnable() {
				
				@Override
				public void run() {
					
					descargar();
				}
			});
			
			LabelPanel actualizar = new LabelPanel(AtachmentController.CARPETA_RECURSOS + "adjuntos/actualizar.png", tamanoOpcion);
			actualizar.setOverColor(Color.decode("#7cff8b"));
			actualizar.setShape(shape);
			panelOpciones.add(actualizar);

			actualizar.addAccion(new Runnable() {

				@Override
				public void run() {

					actualizar();
				}
			});
		}
		
		if(!adjunto.isPrincipal()) {
			
			LabelPanel eliminar = new LabelPanel(AtachmentController.CARPETA_RECURSOS + "adjuntos/eliminar.png", tamanoOpcion);
			eliminar.setOverColor(Color.decode("#ff7c7c"));
			eliminar.setShape(shape);
			panelOpciones.add(eliminar);
			
			eliminar.addAccion(new Runnable() {
				
				@Override
				public void run() {
					
					eliminar();
				}
			});
		}

		revalidate();
		repaint();
	}
	
	public void actualizar() {

		if(conexion != null) {
			
			try {

				String destino = adjunto.getRuta();				
				List<Adjunto> adjuntos = conexion.listFiles(destino);

				if(!adjuntos.isEmpty()) {

					adjunto = adjuntos.get(0);

					updateTamano();
					updateFecha();
				}

			} catch (Exception e) {

				e.printStackTrace();
			}
		}
	}
	
	public void descargar() {

		if(conexion != null) {
			
			JFileChooser chooser = new JFileChooser();
			chooser.setDialogTitle("Directorio descarga");
			chooser.setMultiSelectionEnabled(false);
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			chooser.setCurrentDirectory(new File(".").getAbsoluteFile());

			int sel = chooser.showOpenDialog(getParent());

			if (sel == JFileChooser.APPROVE_OPTION) {

				// DESCARGAR EN LA RUTA
				final String destino = chooser.getSelectedFile().getAbsolutePath();

				setSelectable(false);
				setSeleccionado(false);
				setOverable(false);

				// QUITAR ANTERIOR
				removeAll();

				// PONER PROGRESS BAR
				final JProgressBar cargando = new JProgressBar();
				add(cargando, BorderLayout.CENTER);
				
				new Thread(new Runnable() {

					@Override
					public void run() {

						// DESCARGAR
						CopyStreamListener listener = new CopyStreamListener() {

							@Override
							public void bytesTransferred(long total, int transferido, long tamano) {

								double transmitido = (total/adjunto.getTamano(Adjunto.BYTES))*100;
								transmitido = Math.min(transmitido, 100);

								cargando.setValue((int) transmitido);
							}

							@Override
							public void bytesTransferred(CopyStreamEvent arg0) {}
						};

						try {

							if(conexion.downloadFile(destino, adjunto.getDirectorio(), adjunto.getFichero(), listener) != null) {

								cargando.setValue(100);

								// ESPERA
								try {
									Thread.sleep(500);
								} catch (InterruptedException e) {}

								// INFORMAR
								removeAll();

								setLayout(new FlowLayout(FlowLayout.LEADING, 10, 5));

								LabelPanel imagen = new LabelPanel(AtachmentController.CARPETA_RECURSOS + "adjuntos/resultado_correcto.png", AtachmentController.getSingleton().getSize()-5);
								add(imagen);

								JLabel informacion = new JLabel("Se ha descargado con exito"); 
								add(informacion);

								revalidate();
								repaint();

							} else {

								// INFORMAR
								removeAll();

								setLayout(new FlowLayout(FlowLayout.LEADING, 10, 5));

								LabelPanel imagen = new LabelPanel(AtachmentController.CARPETA_RECURSOS + "adjuntos/resultado_error.png", AtachmentController.getSingleton().getSize()-5);
								add(imagen);

								JLabel informacion = new JLabel("No se ha podido descargar el fichero"); 
								add(informacion);

								revalidate();
								repaint();
							}

						} catch (Exception e) {

							e.printStackTrace();
						}

						// ESPERA
						try {
							Thread.sleep(1500);
						} catch (InterruptedException e) {}

						// CARGAR NORMAL
						inicializar();
					}

				}).start();

				revalidate();
				repaint();
			}
		}
	}
	
	public void eliminar() {

		setSelectable(false);
		setSeleccionado(false);
		setOverable(false);
		
		// QUITAR ANTERIOR
		removeAll();

		setLayout(new FlowLayout(FlowLayout.TRAILING, 10, 5));

		JLabel informacion = new JLabel("¿Estas seguro?"); 
		add(informacion);
		
		LabelPanel aceptar = new LabelPanel(AtachmentController.CARPETA_RECURSOS + "adjuntos/resultado_correcto.png", AtachmentController.getSingleton().getSize()-5);
		aceptar.setOverColor(Color.decode("#7cff8b"));
		aceptar.setShape(shape);
		add(aceptar);

		aceptar.addAccion(new Runnable() {
			
			@Override
			public void run() {

				boolean resultado = false;

				if(conexion != null) {
					
					try {

						if(adjunto.getTipo() == AtachmentController.getSingleton().get(AtachmentController.CARPETA)) {

							resultado = conexion.removeDirectory(adjunto.getRuta());

						} else {

							resultado = conexion.removeFile(adjunto.getDirectorio(), adjunto.getFichero());
						}

					} catch (Exception e) {

						e.printStackTrace();
					}
				}
				
				if(resultado) {
					
					Container parent = getParent();

					parent.remove(PanelAdjunto.this);

					parent.revalidate();
					parent.repaint();
					
				} else {

					// INFORMAR
					removeAll();

					setLayout(new FlowLayout(FlowLayout.LEADING, 10, 5));

					LabelPanel imagen = new LabelPanel(AtachmentController.CARPETA_RECURSOS + "adjuntos/resultado_error.png", AtachmentController.getSingleton().getSize()-5);
					add(imagen);

					JLabel informacion = new JLabel("No se ha podido eliminar"); 
					add(informacion);

					revalidate();
					repaint();

					// ESPERA
					try {
						Thread.sleep(1500);
					} catch (InterruptedException e) {}

					// CARGAR NORMAL
					inicializar();
				}
			}
		});
		
		LabelPanel cancelar = new LabelPanel(AtachmentController.CARPETA_RECURSOS + "adjuntos/resultado_error.png", AtachmentController.getSingleton().getSize()-5);
		cancelar.setOverColor(Color.decode("#ff7c7c"));
		cancelar.setShape(shape);
		add(cancelar);
		
		cancelar.addAccion(new Runnable() {
			
			@Override
			public void run() {

				// CARGAR NORMAL
				inicializar();				
			}
		});
		
		revalidate();
		repaint();
	}
	
	private HintTextField texto;
	
	public void renombrar() {
		
		setSelectable(false);
		setSeleccionado(false);
		setOverable(false);
		
		// QUITAR ANTERIOR
		removeAll();

		setLayout(new BorderLayout(0, 0));
		
		// INFORMACION PRINCIPAL
		JPanel panelInformacion = new JPanel();
		panelInformacion.setOpaque(false);
		panelInformacion.setLayout(new FlowLayout(FlowLayout.LEADING, 10, 5));
		add(panelInformacion, BorderLayout.WEST);
		
		texto = new HintTextField("");
		texto.setText(adjunto.getNombre());
		texto.setColumns(20);
		panelInformacion.add(texto);
		
		texto.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				String nuevo = texto.getText();
				aplicarRenombre(nuevo);
			}
		});
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
			
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {}
				
				texto.requestFocus();
				texto.setSelectionStart(texto.getText().length());
			}
			
		}).start();
		
		// OPCIONES
		JPanel panelOpciones = new JPanel();
		panelOpciones.setOpaque(false);
		panelOpciones.setLayout(new FlowLayout(FlowLayout.TRAILING, 10, 5));
		add(panelOpciones, BorderLayout.EAST);
		
		LabelPanel aceptar = new LabelPanel(AtachmentController.CARPETA_RECURSOS + "adjuntos/resultado_correcto.png", AtachmentController.getSingleton().getSize()-5);
		aceptar.setOverColor(Color.decode("#7cff8b"));
		aceptar.setShape(shape);
		panelOpciones.add(aceptar);

		aceptar.addAccion(new Runnable() {
			
			@Override
			public void run() {
				
				String nuevo = texto.getText();
				aplicarRenombre(nuevo);
			}
		});
		
		LabelPanel cancelar = new LabelPanel(AtachmentController.CARPETA_RECURSOS + "adjuntos/resultado_error.png", AtachmentController.getSingleton().getSize()-5);
		cancelar.setOverColor(Color.decode("#ff7c7c"));
		cancelar.setShape(shape);
		panelOpciones.add(cancelar);
		
		cancelar.addAccion(new Runnable() {
			
			@Override
			public void run() {

				// CARGAR NORMAL
				inicializar();
			}
		});
		
		revalidate();
		repaint();
	}
	
	private void aplicarRenombre(String nuevo) {
		
		if(!nuevo.equals("") && !nuevo.equals(adjunto.getNombre())) {
			
			boolean resultado = false;

			if(conexion != null) {
				
				try {

					if(adjunto.getTipo() == AtachmentController.getSingleton().get(AtachmentController.CARPETA)) {

						resultado = conexion.renameDirectory(adjunto.getDirectorio(), adjunto.getFichero(), nuevo);

					} else {

						nuevo += "." + adjunto.getExtension();
						resultado = conexion.renameFile(adjunto.getDirectorio(), adjunto.getFichero(), nuevo);
					}

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
			
			if(resultado) {
				
				adjunto.setFichero(nuevo);
				inicializar();
				
			} else {

				// INFORMAR
				removeAll();

				setLayout(new FlowLayout(FlowLayout.LEADING, 10, 5));

				LabelPanel imagen = new LabelPanel(AtachmentController.CARPETA_RECURSOS + "adjuntos/resultado_error.png", AtachmentController.getSingleton().getSize()-5);
				add(imagen);

				JLabel informacion = new JLabel("No se ha podido cambiar el nombre"); 
				add(informacion);

				revalidate();
				repaint();

				// ESPERA
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e) {}

				// CARGAR NORMAL
				inicializar();
			}
		}
	}
	
	// ACTUALIZAR DATOS
	private void updateTamano() {

		tamano.setText(Integer.toString((int)adjunto.getTamano()) + " KB");
	}
	
	private void updateFecha() {
		
		fecha.setText(adjunto.getFecha().parse(new Fecha.Formato() {
			
			@Override
			public SimpleDateFormat getFormato() {

				return new SimpleDateFormat("dd/MM/yyy H:mm:ss");
			}
		}));
	}
}
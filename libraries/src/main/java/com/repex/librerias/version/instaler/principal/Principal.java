package com.repex.librerias.version.instaler.principal;

import com.repex.librerias.version.instaler.screen.InstalerScreen;

public class Principal {

	public static void main(String[] args) {
				
		if(args.length > 0) {

			InstalerScreen interfaz = new InstalerScreen(args[0]);
			interfaz.setVisible(true);
		}
	}
}

package com.repex.librerias.version.control.version;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.repex.librerias.date.Fecha;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;
import com.repex.librerias.version.control.version.impl.VersionImpl;
import com.repex.librerias.version.instaler.utilidades.LinuxParser;

public class VersionControl {

	private String url; // URL DONDE SE ENCUENTRA EL FICHERO DE VERSION Y EL NUEVO FICHERO	
		
	private String nombre;
	private String icono; // URL DEL ICONO
	
	public Version version; // VERSION ACTUAL
	
	private Fecha fecha;
	private int tamano;
	
	private String ruta; // RUTA DEL PC DONDE SE GUARDA EL JAR
	
	private boolean requerido; // ACTUALIZACION OBLIGATORIA
	
	public VersionControl(String url, String nombre, String icono, Version version, Fecha fecha, int tamano, String ruta, boolean requerido) {
		
		this.url = url;
		
		this.nombre = nombre;
		this.icono = icono;
		
		this.version = version;
		
		this.fecha = fecha;
		this.tamano = tamano;
		
		this.ruta = ruta;
		
		this.requerido = requerido;
	}
	
	public VersionControl(String url) throws MalformedURLException, IOException, ParserConfigurationException, SAXException, RequiredAuthenticationException {

		URLConnection conn = new URL(url).openConnection();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new InputSource(conn.getInputStream()));
		
		this.url = doc.getElementsByTagName("url").item(0).getTextContent();
				
		this.nombre = doc.getElementsByTagName("name").item(0).getTextContent();		
		this.version = new VersionImpl(doc.getElementsByTagName("number").item(0).getTextContent());
		
		this.fecha = new Fecha(doc.getElementsByTagName("date").item(0).getTextContent());
		this.tamano = Integer.parseInt(doc.getElementsByTagName("size").item(0).getTextContent());
		
		this.ruta = doc.getElementsByTagName("ruta").item(0).getTextContent();
		
		this.requerido = Boolean.parseBoolean(doc.getElementsByTagName("required").item(0).getTextContent());

		if(doc.getElementsByTagName("icon") != null && doc.getElementsByTagName("icon").getLength() > 0) {
			this.icono = doc.getElementsByTagName("icon").item(0).getTextContent();
		}
	}
	
	public VersionControl(InputStream inputStream) throws IOException, ParserConfigurationException, SAXException {
			
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(inputStream);

		this.url = doc.getElementsByTagName("url").item(0).getTextContent();
		
		this.nombre = doc.getElementsByTagName("name").item(0).getTextContent();
		this.version = new VersionImpl(doc.getElementsByTagName("number").item(0).getTextContent());
		
		this.fecha = new Fecha(doc.getElementsByTagName("date").item(0).getTextContent());
		this.tamano = Integer.parseInt(doc.getElementsByTagName("size").item(0).getTextContent());

		this.ruta = doc.getElementsByTagName("ruta").item(0).getTextContent();
		
		this.requerido = Boolean.parseBoolean(doc.getElementsByTagName("required").item(0).getTextContent());
	}
	
	public String getNombre() {
		
		return nombre;
	}
	
	public String getIcono() {
		return icono;
	}
	
	public String getUrl() {
		
		return url;
	}
	
	public Version getVersion() {
		
		return version;
	}
	
	public Fecha getFecha() {
		
		return fecha;
	}
	
	public int getTamano() {
		
		return tamano;
	}
	
	public String getRuta() {

		if (System.getProperty("os.name").startsWith("Windows")) {
			return ruta;
		}
		
		return LinuxParser.parse(ruta);
	}
	
	public boolean isRequerido() {
		return requerido;
	}
	
	public String getUrlVersion() {
		
		return getUrl() + "version.xml";
	}
	
	public String getUrlFichero() {
		
		return getUrl() + getNombre() + "-" + getVersion().toString() + ".jar";
	}
	
	public String getRutaFichero() {
		
		return getRuta() + getNombre() + ".jar";
	}
	
	public String getRutaFicheroCompleta() {
		
		return getRuta() + getNombre() + "-" + getVersion().toString() + ".jar";
	}
}
package com.repex.librerias.version.instaler.control;

import java.io.IOException;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class InstalerControl {
	
	private String url; // URL DONDE SE ENCUENTRA EL FICHERO INSTALER JAR
	private int tamano;
	
	private static InstalerControl singleton;
	
	public static InstalerControl getSingleton() {
		
		if(singleton == null) {
		
			try {

				singleton = new InstalerControl("http://repexgroup.eu/apps/instaler/instaler.xml");
				
			} catch (Exception e) {
				
				e.printStackTrace();
			}			
		}
		
		return singleton;
	}
	
	private InstalerControl(String url) throws MalformedURLException, IOException, ParserConfigurationException, SAXException {

		URLConnection conn = new URL(url).openConnection();					
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new InputSource(conn.getInputStream()));
		
		this.url = doc.getElementsByTagName("url").item(0).getTextContent();
		this.tamano = Integer.parseInt(doc.getElementsByTagName("size").item(0).getTextContent());
	}
	
	public String getUrl() {
		
		return url;
	}
	
	public int getTamano() {
		
		return tamano;
	}
}
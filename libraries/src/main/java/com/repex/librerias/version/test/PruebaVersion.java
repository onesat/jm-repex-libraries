package com.repex.librerias.version.test;

public class PruebaVersion {

	public static void main(String[] args) {
		
		String tempdir = System.getProperty("java.io.tmpdir");

		if ( !(tempdir.endsWith("/") || tempdir.endsWith("\\")) )
		   tempdir = tempdir + System.getProperty("file.separator");
		
		System.out.println(tempdir);
	}
}
package com.repex.librerias.version.instaler.control;

public interface Instaler {

	public String getRuta();
	public int getTamano();
}

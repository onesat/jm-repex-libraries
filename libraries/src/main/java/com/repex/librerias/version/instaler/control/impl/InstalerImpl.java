package com.repex.librerias.version.instaler.control.impl;

import com.repex.librerias.version.instaler.control.Instaler;

public class InstalerImpl implements Instaler {

	private String ruta;
	private int tamano;
	
	public InstalerImpl(String ruta, int tamano) {
		
		this.ruta = ruta;
		this.tamano = tamano;
	}

	@Override
	public String getRuta() {
		
		return ruta;
	}

	@Override
	public int getTamano() {
		
		return tamano;
	}
}
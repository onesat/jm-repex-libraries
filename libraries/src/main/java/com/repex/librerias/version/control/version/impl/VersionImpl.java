package com.repex.librerias.version.control.version.impl;

import com.repex.librerias.version.control.version.Version;

public class VersionImpl implements Version {
	
	public int version;
	public int subversion;
	public int release;
	
	public VersionImpl(int version, int subversion, int release) {
		
		this.version = version;
		this.subversion = subversion;
		this.release = release;
	}

	public VersionImpl(String version) {
				
		String v[] = version.split("\\.");
		
		try {

			this.version = Integer.parseInt(v[0]);
			this.subversion = Integer.parseInt(v[1]);
			this.release = Integer.parseInt(v[2]);
			
		} catch (Exception e) {}
	}
	
	@Override
	public int getVersion() {
		
		return version;
	}

	@Override
	public int getSubversion() {
		
		return subversion;
	}

	public int getRelease() {
		
		return release;
	}

	@Override
	public boolean compare(Version other) {
		
		if ((this.version < other.getVersion()) || (this.version == other.getVersion() && this.subversion < other.getSubversion()))
			return true;
		
		if(other instanceof VersionImpl)
			return (this.version == other.getVersion() && this.subversion == other.getSubversion() && this.release < ((VersionImpl) other).getRelease());
	
		return false;
	}
	
	@Override
	public String toString() {
		
		return this.version + "." + this.subversion + "." + this.release;
	}
}
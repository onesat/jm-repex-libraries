package com.repex.librerias.version.instaler.utilidades;

public class Constantes {
	
	public static String PC_INSTALLER() {
		
		if (System.getProperty("os.name").startsWith("Windows")) {
			
			return "C:\\tmp\\instaler.jar";
	    }

		return "usr/tmp/instaler.jar";
	}
	
	public static String CARPETA_LOGO = "logo";
}
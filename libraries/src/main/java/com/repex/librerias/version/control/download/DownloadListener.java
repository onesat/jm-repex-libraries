package com.repex.librerias.version.control.download;

public interface DownloadListener {

	public void update(double downloaded);
	public void complete();
}
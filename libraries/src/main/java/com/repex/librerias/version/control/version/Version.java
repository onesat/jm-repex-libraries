package com.repex.librerias.version.control.version;

public interface Version {

	public int getVersion();	
	public int getSubversion();
	
	public boolean compare(Version other);
}
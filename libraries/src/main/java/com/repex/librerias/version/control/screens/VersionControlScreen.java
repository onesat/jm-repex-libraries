package com.repex.librerias.version.control.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.xml.parsers.ParserConfigurationException;

import com.repex.librerias.components.botones.LoadingButton;
import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.components.screen.Screen;
import com.repex.librerias.components.utilidades.UrlControler;
import com.repex.librerias.utilidades.IconUtils;
import com.repex.librerias.version.control.download.DownloadListener;
import com.repex.librerias.version.control.download.impl.DownloadImpl;
import com.repex.librerias.version.control.exception.NoVersionException;
import com.repex.librerias.version.control.utilidades.Constantes;
import com.repex.librerias.version.control.version.VersionControl;
import com.repex.librerias.version.instaler.control.InstalerControl;

public class VersionControlScreen extends Screen {

private static final long serialVersionUID = 1L;

	// COMPRUEBA SI HAY NUEVA VERSION, RECUPERA EL INSTALADOR E INSTALA LA NUEVA VERSION
	private static final Logger LOGGER = Logger.getLogger(VersionControlScreen.class.getName() );

	// DATOS
	private VersionControl versionControl;	
	private VersionControl version;

	private String icono;
	
	// HILOS	
	private Thread comprobar;
	private Thread descargar;
	
	// CONFIGURACION
	private boolean cierreAutomatico = true;
	private int segundosCierre = 2;

	public VersionControlScreen(final VersionControl versionControl) {
		this(versionControl, null);
	}
	
	public VersionControlScreen(final VersionControl versionControl, String icono) {

		super("Version");
		
		// LOGGER
		LOGGER.setLevel(Level.ALL);
		
		ConsoleHandler handler = new ConsoleHandler();
		handler.setLevel(Level.ALL);
		
		LOGGER.addHandler(handler);
		
		// VERSION ACTUAL
		this.versionControl = versionControl;		
		this.icono = icono;
		
		// PANEL CENTRAL
		BorderPanel contentPanel = new BorderPanel(1, Color.BLACK);
		contentPanel.setBackground(Color.WHITE);
		contentPanel.setLayout(new BorderLayout());
		setContentPane(contentPanel);

		// PROGRESS BAR
		createProgressBar(contentPanel);
		
		// INFORMACION
		createInformacion(contentPanel);
		updateInformacion();
		
		// BOTONES
		createButtons(contentPanel);
		
		// COMPROBAR
		comprobar = new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				// ELIMINAR EL ANTERIOR INSTALER
				File instaler = new File(Constantes.PC_INSTALLER());
				if(instaler.exists()) {
					
					instaler.delete();
				}
				
				// BUSCAR FICHERO XML
				try {

					new Thread(new Runnable() {
						
						@Override
						public void run() {
							
							try {
								
								Thread.sleep(3000);

								if(comprobar.isAlive() && versionControl == null) {
									
									comprobar.interrupt();
									errorInformacion("No se puede conectar con el servidor");
								}
								
							} catch (InterruptedException e) {}
						}
						
					}).start();
					
					// VERSION
					version = new VersionControl(versionControl.getUrlVersion());
					
					// INSTALER
					InstalerControl.getSingleton();
					
					updateInformacion();
					updateButtons();
					
					// SI NO HAY QUE ACTUALIZAR CONTINUAR
					if(cierreAutomatico && !versionControl.getVersion().compare(version.getVersion())) {
						
						Thread.sleep(segundosCierre * 1000);
						continuar();
					}
					
				} catch (InterruptedException e) {
					
					System.out.println("interrupted");
					
				} catch (ParserConfigurationException e) {
					
					LOGGER.log(Level.ALL, "Error de parsing al recuperar XML", e);
					
				} catch (Exception e) {

					LOGGER.log(Level.ALL, "Error en la URL", e);
				}
			}
		});
		
		comprobar.start();
		
		// ICONO
		setIconImage(((ImageIcon) IconUtils.escalar(getClass().getClassLoader().getResource(com.repex.librerias.utilidades.Constantes.CARPETA_RECURSOS + "login/logo.png"))).getImage());
		
		if(this.icono != null) {
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					
					try {
						
						setIconImage(load());
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
			}).start();
		}
	}

	private Image load() throws Exception {
		return ((ImageIcon) UrlControler.getSingleton().cargar(icono, 100)).getImage();
	}
	
	public void setCierreAutomatico(boolean cierreAutomatico) {
		
		this.cierreAutomatico = cierreAutomatico;
	}
	
	public void setSegundosCierre(int segundos) {
		
		this.segundosCierre = segundos;
	}
	
	@Override
	protected void configuracion() {

		setSize(550, 300);
		setResizable(false);
		setLocationRelativeTo(null);
	}

	private JProgressBar progressBar;
	
	private void createProgressBar(JComponent parent) {

		JPanel loadingPanel = new BorderPanel(0, Color.BLACK, 15, false, true);
		loadingPanel.setOpaque(false);
		loadingPanel.setLayout(new BorderLayout(0, 0));
		parent.add(loadingPanel, BorderLayout.NORTH);
		
		progressBar = new JProgressBar(0, 100);
		loadingPanel.add(progressBar, BorderLayout.CENTER);
		
		progressBar.setVisible(false);
	}
	
	protected void updateProgressBar(int nivel) {
		
		if(!progressBar.isVisible()) {
			
			progressBar.setVisible(true);
			progressBar.setStringPainted(true);
		}
		
		if(nivel < 0) {
			
			progressBar.setVisible(false);
			
		} else {
			
			progressBar.setValue(nivel);
			progressBar.setString(nivel + "%");
		}
	}
	
	private JLabel informacion;
	private JLabel actual;
	private JLabel nueva;

	private void updateInformacion(String informacion) {

		this.informacion.setText(informacion);
	}
	
	private void updateInformacion() {

		this.informacion.setText("Comprobando nueva version...");
		this.actual.setText("Actual: "+versionControl.getVersion().toString() + " (" + (versionControl.getTamano()/1024) + " Kb)");
		
		if(version != null) {
			
			this.nueva.setText("Nueva: "+version.getVersion().toString() + " (" + (versionControl.getTamano()/1024) + " Kb)");
			
			if(versionControl.getVersion().compare(version.getVersion())) {

				this.informacion.setText(version.getNombre() + "  --  Hay una nueva version");				
				this.nueva.setForeground(Color.decode("#448844"));
				
			} else {

				this.informacion.setText("Nada que actualizar");
				this.nueva.setForeground(Color.decode("#444488"));			
			}
			
		} else {

			this.nueva.setText("--");
		}
	}
	
	private void errorInformacion(String error) {

		this.informacion.setText(error);
		this.informacion.setForeground(Color.decode("#884444"));
	}
	
	protected void createInformacion(JComponent parent) {

		GridBagConstraints constraints = new GridBagConstraints();
		
		constraints.insets = new Insets(0, 0, 15, 0);
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		
		JPanel panelInformacion = new BorderPanel(0, Color.BLACK, 5, false, true);
		panelInformacion.setOpaque(false);
		panelInformacion.setLayout(new GridBagLayout());
		parent.add(panelInformacion);
		
		informacion = new JLabel();
		informacion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		informacion.setHorizontalAlignment(SwingConstants.CENTER);
		panelInformacion.add(informacion, constraints);

		constraints.insets = new Insets(0, 0, 5, 0);
		constraints.gridy = 1;
		
		actual = new JLabel();
		actual.setFont(new Font("Tahoma", Font.BOLD, 16));
		actual.setHorizontalAlignment(SwingConstants.CENTER);
		panelInformacion.add(actual, constraints);

		constraints.gridy = 2;
		
		nueva = new JLabel();
		nueva.setFont(new Font("Tahoma", Font.BOLD, 16));
		nueva.setHorizontalAlignment(SwingConstants.CENTER);
		panelInformacion.add(nueva, constraints);
	}
	
	private LoadingButton actualizar;
	private LoadingButton continuar;
	
	private void updateButtons() {
	
		if(version != null) {

			if(versionControl.getVersion().compare(version.getVersion())) {

				this.actualizar.setTexto("Actualizar");

				actualizar.addMouseListener(new MouseAdapter() {
					
					@Override
					public void mouseClicked(MouseEvent e) {
						actualizar();
					}
				});
				
				if(version.isRequerido()) {
					
					this.continuar.setVisible(false);
					
					new Thread(new Runnable() {
						
						@Override
						public void run() {
							
							try {
								Thread.sleep(2000); // ESPERAR 2 SEGUNDOS
							} catch (InterruptedException e) {}
							
							actualizar();
						}
						
					}).start();
				}
				
			} else {

				this.continuar.setTexto("Continuar");
				this.actualizar.setVisible(false);
			}
		}
	}
	
	private void actualizar() {
		
		if(descargar == null) {
			
			// PROGRESS BAR
			updateProgressBar(0);

			descargar = new Thread(new Runnable() {

				@Override
				public void run() {

					// DESCARGAR
					updateInformacion("Descargando el instalador...");

					String url = InstalerControl.getSingleton().getUrl();

					try {

						final File destino = File.createTempFile("instaler", ".jar");
						destino.deleteOnExit();
						
						// DESCARGAR INSTALER Y ABRIRLO
						new DownloadImpl().download(url, destino, new DownloadListener() {

							@Override
							public void update(double downloaded) {

								double percentage = downloaded/InstalerControl.getSingleton().getTamano() * 100;
								updateProgressBar((int) percentage);									
							}

							public void complete() {

								try {

									updateInformacion("La aplicacion se cerrara y abrira el instalador");

									Thread.sleep(1000);

									ProcessBuilder pb = new ProcessBuilder("java", "-jar", destino.getAbsolutePath(), version.getUrlVersion());
									pb.start();

									salir();

								} catch (Exception e2) {}

							};										
						});

					} catch (NoVersionException e2) {

						errorInformacion("Fallo al descargar instalador");

					} catch (MalformedURLException e2) {

						LOGGER.log(Level.ALL, "URL mal formada al descargar", e2);
						
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});

			descargar.start();
		}
	}
	
	protected void createButtons(JComponent parent) {
	
		JPanel buttonsPanel = new BorderPanel(0, Color.BLACK, 5, false, true);
		buttonsPanel.setOpaque(false);
		buttonsPanel.setLayout(new GridBagLayout());
		parent.add(buttonsPanel, BorderLayout.SOUTH);
		
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridx = 0;
		c.gridy = 0;
		
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;

		c.insets = new Insets(0, 0, 0, 5);
		
		// SALTAR ACTUALIZACION
		continuar = new LoadingButton("Continuar sin actualizar");
		buttonsPanel.add(continuar, c);
		
		c.gridx++;
		c.insets = new Insets(0, 0, 0, 0);

		// ACTUALIZAR
		actualizar = new LoadingButton("...");
		buttonsPanel.add(actualizar, c);
		
		continuar.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
				// SALTAR CONTROL
				if(comprobar.isAlive()) {
					
					comprobar.interrupt();
				}
				
				continuar();
			}
		});
	}
	
	private void continuar() {

		// CONTINUAR
		if(listener != null) {
			
			listener.continuar();
		}

		setVisible(false);
		dispose(); // CERRAR
	}
	
	// LISTENER
	public OnContinueListener listener;
	
	public void setListener(OnContinueListener listener) {
		
		this.listener = listener;
	}
	
	public interface OnContinueListener {
		
		public void continuar();
	}
}
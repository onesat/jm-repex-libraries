package com.repex.librerias.version.instaler.screen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;

import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.components.screen.Screen;
import com.repex.librerias.components.textfield.HintTextArea;
import com.repex.librerias.io.IOController;
import com.repex.librerias.version.control.download.DownloadListener;
import com.repex.librerias.version.control.download.impl.DownloadImpl;
import com.repex.librerias.version.control.exception.NoVersionException;
import com.repex.librerias.version.control.version.VersionControl;
import com.repex.librerias.version.instaler.utilidades.Constantes;
import com.repex.librerias.version.instaler.utilidades.LinuxParser;

import net.jimmc.jshortcut.JShellLink;

public class InstalerScreen extends Screen {

	private static final long serialVersionUID = 1L;
	
	private VersionControl versionControl;

	// HILOS
	private Thread descargar;
	
	public InstalerScreen(final String url) {
		
		super("Instalador 1.0.0");
		
		// PANEL CENTRAL
		BorderPanel contentPanel = new BorderPanel(1, Color.BLACK);
		contentPanel.setBackground(Color.WHITE);
		contentPanel.setLayout(new BorderLayout());
		setContentPane(contentPanel);

		// PROGRESS BAR
		createProgressBar(contentPanel);

		// INFORMACION
		createInformacion(contentPanel);
		updateInformacion();
		
		descargar = new Thread(new Runnable() {
			
			@Override
			public void run() {

				new Thread(new Runnable() {
					
					@Override
					public void run() {
						
						try {
							
							// TODO: ES DEMASIADO POCO
							Thread.sleep(10000);

							if(descargar.isAlive() && versionControl == null) {
								
								descargar.interrupt();
								errorInformacion("No se pueden conectar con el servidor");
							}
							
						} catch (InterruptedException e) {}
					}
					
				}).start();
				
				try {

					updateInformacion("Recuperando fichero de versiones...");
					
					// VERSION
					versionControl = new VersionControl(url);

					Thread.sleep(1000); // ESPERAR UN SEGUNDO					
					updateInformacion();
					
					// DESCARGAR
					File directorio = new File(versionControl.getRuta());
					if(!directorio.exists()) {
						
						directorio.mkdirs();
					}
					
					String url = versionControl.getUrlFichero();
					final File destino = new File(versionControl.getRutaFicheroCompleta());

					// ELIMINAR ANTERIOR
					if(destino.exists()) {
						destino.delete();
					}
					
					log(url);
					log(destino.getAbsolutePath());
					
					try {
						
						// DESCARGAR INSTALER Y ABRIRLO
						new DownloadImpl().download(url, destino, new DownloadListener() {
							
							@Override
							public void update(double downloaded) {
								
								double percentage = downloaded/versionControl.getTamano() * 100;
								updateProgressBar((int) percentage);
							}
							
							public void complete() {

								// Actualizar a 100%
								updateProgressBar(100);
								
								try {
									
									// DESCARGAR ICONO
									String icono = null;
									try {
										icono = getIcono();
									} catch (Exception e) {
										e.printStackTrace();
									}
									
									if(icono != null && !icono.equals("")) {
										
										// CREAR LINK
										JShellLink link = new JShellLink();
										link.setFolder(LinuxParser.desktop().getAbsolutePath());
										link.setName(versionControl.getNombre());  //Choose a name for your shortcut.In my case its JMM.
										link.setPath(destino.getAbsolutePath()); //link for our executable jar file
										link.setIconLocation(icono); //set icon image(before that choose your on manual icon file inside our project folder.[jmm.ico in my case])
										link.save();
										
									} else {
										
	                                    // CREAR LINK                                    
	                                    Path newLink = Paths.get(new File(LinuxParser.desktop(), versionControl.getNombre() + ".jar").getAbsolutePath());
	                                    Path target = Paths.get(destino.getAbsolutePath());

	                                    // ELIMINAR ANTERIOR
	                                    File anterior = new File(newLink.toFile().getAbsolutePath());
	                                    if(anterior.exists()) {
	                                        anterior.delete();
	                                    }
	                                    
	                                    Files.createLink(newLink, target);
									}
							        									
									// ABRIR LA APP
									updateInformacion("La aplicacion se abrira en breve");
									
									Thread.sleep(1000);
									
							        ProcessBuilder pb = new ProcessBuilder("java", "-jar", destino.getAbsolutePath());
							        pb.start();
							        
							        salir();
							        
								} catch (IOException e) {

									errorInformacion("IO Exception");
									log(e);
									
								} catch (UnsupportedOperationException e) {

									errorInformacion("Unsupported Operation Exception");
									log(e);
									
								} catch (InterruptedException e) {
									
									e.printStackTrace();
									log(e);
								}
							};										
						});
						
					} catch (NoVersionException e) {
						
						errorInformacion("Fallo al descargar instalador");
						log(e);
						
					} catch (MalformedURLException e) {

						errorInformacion("Malformed URL Exception");
						log(e);
					}
					
				} catch (ParserConfigurationException e) {

					errorInformacion("Parser Config Exception");
					log(e);
					
				} catch (Exception e) {

					errorInformacion("Otras excepciones");
					log(e);
				}
			}
		});
		
		descargar.start();
	}

	private String getIcono() throws Exception {

		if(versionControl.getIcono() == null)
			return null;
		
		String nombre = "logo.ico";
		String directorio = versionControl.getRuta() + Constantes.CARPETA_LOGO;

		File file = new File(directorio, nombre);
		if(file.exists()) {
			file.delete();
		}
		
		FileUtils.copyURLToFile(new URL(versionControl.getIcono()), file);
			
		return file.getAbsolutePath();
	}
	
	@Override
	protected void configuracion() {

		setSize(550, 300);
		setResizable(false);
		setLocationRelativeTo(null);
	}

	private JProgressBar progressBar;
	
	private void createProgressBar(JComponent parent) {

		JPanel loadingPanel = new BorderPanel(0, Color.BLACK, 15, false, true);
		loadingPanel.setOpaque(false);
		loadingPanel.setLayout(new BorderLayout(0, 0));
		parent.add(loadingPanel, BorderLayout.NORTH);
		
		progressBar = new JProgressBar(0, 100);
		loadingPanel.add(progressBar, BorderLayout.CENTER);
		
		progressBar.setVisible(false);
	}
	
	protected void updateProgressBar(int nivel) {
		
		if(!progressBar.isVisible()) {
			
			progressBar.setVisible(true);
			progressBar.setStringPainted(true);
		}
		
		if(nivel < 0) {
			
			progressBar.setVisible(false);
			
		} else {
			
			progressBar.setValue(nivel);
			progressBar.setString(nivel + "%");
		}
	}
	
	private JLabel titulo;
	private JLabel informacion;

	private HintTextArea error;
	
	private void errorInformacion(String error) {

		this.informacion.setText(error);
		this.informacion.setForeground(Color.decode("#884444"));
	}
	
	private void updateInformacion(String informacion) {

		this.informacion.setText(informacion);
	}
	
	private void updateInformacion() {

		this.titulo.setText("---");
		this.informacion.setText("Descargando fichero control de version...");
		
		if(versionControl != null) {

			this.titulo.setText(versionControl.getNombre().toUpperCase());
			this.informacion.setText("Descargando version " + versionControl.getVersion());
		}
	}
	
	protected void createInformacion(JComponent parent) {

		GridBagConstraints constraints = new GridBagConstraints();
		
		constraints.insets = new Insets(0, 0, 15, 0);
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		
		JPanel panelInformacion = new BorderPanel(0, Color.BLACK, 5, false, true);
		panelInformacion.setOpaque(false);
		panelInformacion.setLayout(new GridBagLayout());
		parent.add(panelInformacion);
		
		titulo = new JLabel();
		titulo.setFont(new Font("Tahoma", Font.BOLD, 18));
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		panelInformacion.add(titulo, constraints);

		constraints.gridy++;

		informacion = new JLabel();
		informacion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		informacion.setHorizontalAlignment(SwingConstants.CENTER);
		panelInformacion.add(informacion, constraints);

		constraints.gridy++;
		constraints.gridwidth = 5;
		
		error = new HintTextArea("ERROR");
		error.setColumns(150);
		error.setHasPopupMenu(true);
		error.setRows(3);
		panelInformacion.add(error, constraints);
		
		error.setVisible(false);
	}
	
	private void log(String log) {

		new IOController().anadir("log.txt", Arrays.asList(new String[] {log}));
	}
	
	private void log(Exception e) {
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		
		new IOController().anadir("log.txt", Arrays.asList(new String[] {sw.toString()}));

		this.error.setVisible(true);		
		this.error.setText(sw.toString());
	}
}
package com.repex.librerias.version.instaler.utilidades;

import java.io.File;

import javax.swing.filechooser.FileSystemView;

import org.apache.commons.io.FilenameUtils;

public class LinuxParser {

	public static String parse(String directorio) {

		if(directorio.startsWith("C:")) {
			directorio = directorio.substring("C:".length());
		}
		
		directorio = FilenameUtils.separatorsToUnix(directorio);
		
		String home = System.getProperty("user.home");		
		directorio = home + directorio;
		
		return directorio;
	}
	
	public static File desktop() {

		if (System.getProperty("os.name").startsWith("Windows")) {
			return FileSystemView.getFileSystemView().getHomeDirectory();
		}
		
		return new File(System.getProperty("user.home"), "Desktop");
	}
}
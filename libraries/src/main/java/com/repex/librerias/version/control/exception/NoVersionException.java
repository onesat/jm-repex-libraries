package com.repex.librerias.version.control.exception;

public class NoVersionException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public NoVersionException() {
		
		super();
	}
	
	public NoVersionException(String message) {
		
		super(message);
	}
	
	public NoVersionException(String message, Throwable cause) {
		
		super(message, cause);
	}
	
	public NoVersionException(Throwable cause) {
		
		super(cause);
	}
}
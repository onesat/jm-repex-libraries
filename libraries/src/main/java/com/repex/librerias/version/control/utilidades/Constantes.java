package com.repex.librerias.version.control.utilidades;

public class Constantes {
	
	public static String PC_INSTALLER() {

		String tempdir = System.getProperty("java.io.tmpdir");

		if ( !(tempdir.endsWith("/") || tempdir.endsWith("\\")) )
		   tempdir = tempdir + System.getProperty("file.separator");
		
		return tempdir + "instaler.jar";
	}
}
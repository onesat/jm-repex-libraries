package com.repex.librerias.version.control.download.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.repex.librerias.version.control.download.Download;
import com.repex.librerias.version.control.download.DownloadListener;
import com.repex.librerias.version.control.exception.NoVersionException;

public class DownloadImpl implements Download {

	@Override
	public void download(String url, File destino, DownloadListener listener) throws MalformedURLException, NoVersionException {
		
		FileOutputStream out = null;
		
		try {

			BufferedInputStream in = new BufferedInputStream(new URL(url).openStream());
			out = new FileOutputStream(destino.getAbsolutePath());

			byte data[] = new byte[1024];
			int count = 0;
			int total = 0;
			while((count = in.read(data, 0, 1024)) != -1) {
				
				out.write(data, 0, count);
				listener.update(total += count);
			}
			
			listener.complete(); // COMPLETADO
			
		} catch (IOException e) {

			e.printStackTrace();
			
			throw new NoVersionException(url + " version doesnt exist");
			
		} finally {
		
			try {
				
				if(out != null) {
					
					out.close();
				}
				
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
	}	
}
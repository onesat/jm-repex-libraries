package com.repex.librerias.version.control.download;

import java.io.File;
import java.net.MalformedURLException;

import com.repex.librerias.version.control.exception.NoVersionException;

public interface Download {

	public void download(String url, File destino, DownloadListener listener) throws MalformedURLException, NoVersionException;
}
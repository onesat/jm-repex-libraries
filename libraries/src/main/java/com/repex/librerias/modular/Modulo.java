package com.repex.librerias.modular;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import com.repex.librerias.components.labels.LabelPanel;
import com.repex.librerias.components.labels.LabelPanelUrl;
import com.repex.librerias.utilidades.IconUtils;

public abstract class Modulo {
	
	public static int TAMANO_ICONOS = 20;
	
	// DATOS
	private String url;
	
	private Icon icono;
	private String titulo;
	
	private boolean onlyExecute; // SI SOLO HAY QUE EJECUTAR O TAMBIEN CREAR
	private Runnable accion;
	
	private int orden;
	
	public Modulo(Icon icono, String titulo, boolean onlyExecute, Runnable accion) {
				
		this.titulo = titulo;
		
		this.onlyExecute = onlyExecute;
		this.accion = accion;

		if(icono != null) {
			
			this.icono = icono;
		}
		
		if(accion == null) {
			
			this.onlyExecute = false;
		}
	}

	public Modulo(String icono, String titulo, boolean url, boolean onlyExecute, Runnable accion) {
				
		this.titulo = titulo;
		
		this.onlyExecute = onlyExecute;
		this.accion = accion;

		if(icono != null && !icono.equals("")) {
			if(url) {
				this.url = icono;
			} else {
				this.icono = IconUtils.escalar(getClass().getClassLoader().getResource(icono));
			}
		}
		
		if(accion == null) {
			
			this.onlyExecute = false;
		}
	}
	
	public Modulo(Icon icono, String titulo) {
		
		this(icono, titulo, false, null);
	}

	public Modulo(String icono, String titulo, boolean url) {
		
		this(icono, titulo, url, false, null);
	}

	public Modulo(String icono, String titulo) {
		
		this(icono, titulo, false, false, null);
	}
	
	public Modulo(String titulo) {
		
		this("", titulo, false, false, null);
	}

	public Modulo() {

		this("", "", false, false, null);
	}
	
	public LabelPanel construirIcono(int size) {
		
		if(url != null) {

			LabelPanelUrl imagen = new LabelPanelUrl(getUrl(), size);
			imagen.setOpaque(false);
			imagen.setSelectableOverable(false);
			return imagen;
			
		} else if(icono != null) {

			LabelPanel imagen = new LabelPanel(IconUtils.escalar(getIcono(), size));
			imagen.setOpaque(false);
			imagen.setSelectableOverable(false);
			return imagen;
		}
		
		return null;
	}
	
	public String getUrl() {
		return url;
	}
	
	public Icon getIcono() {		
		return icono;
	}
	
	public String getTitulo() {		
		return titulo;
	}
	
	public String getTitulo(int limite) {

		String texto = titulo;
		if(texto.length() > limite) {
			
			texto = texto.substring(0, limite) + "...";
		}
		
		return texto;
	}
	
	public boolean isOnlyExecute() {
		
		return onlyExecute;
	}
	
	public Runnable getAccion() {
		
		return accion;
	}
	
	public int getOrden() {
		return orden;
	}
	
	public void setOrden(int orden) {
		this.orden = orden;
	}
	
	// METODOS HEREDADOS
	private PanelModular modulo;
	public PanelModular construir() {
		
		if(modulo == null) {			
			modulo = crear();
		}
		
		return modulo;
	}

	public void abrir() {
		
		Ventana ventana = new Ventana(this);
		ventana.setVisible(true);
	}
	
	protected abstract PanelModular crear();
	
	public abstract void actualizar(int tipo, Object... data);	
	public abstract String getTag();
	
	public abstract Object clone();
	
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Modulo other = (Modulo) obj;
		if (modulo == null && other.modulo != null)
			return false;
		
		return modulo.equals(other.modulo);
	}
	
	private class Ventana extends JFrame {
		
		private static final long serialVersionUID = 1L;

		public Ventana(Modulo modulo) {

			// CONFIGURACION
			setExtendedState(JFrame.MAXIMIZED_BOTH);
			setBounds(50, 50, 450, 300);
			setMinimumSize(new Dimension(1350, 600));
			setTitle(modulo.getTitulo());

			setContentPane(modulo.construir().contenido());
			
			if(modulo.getIcono() != null) {
				setIconImage(((ImageIcon) modulo.getIcono()).getImage());
			}

			addWindowListener(new WindowAdapter() {
				
				@Override
				public void windowClosing(WindowEvent e) {

					super.windowClosing(e);
				}
			});
		}
	}
}
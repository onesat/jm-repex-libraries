package com.repex.librerias.modular;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.repex.librerias.components.labels.LabelPanel;
import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.components.paneles.BorderPanelOverSelected;
import com.repex.librerias.utilidades.Constantes;

public abstract class PanelModular {
		
	// STATIC
	public static int PREFERED_WIDTH = 230;
	public static int PREFERED_HEIGHT = 45;
	
	public static Color OVER_COLOR = Color.decode("#DCA95A");
	public static Color BACKGROUND_COLOR = Color.decode("#DDDDEE");
	
	private int indice;
	
	private ModularTabbedPane parent;
	
	// CONFIGURACION	
	private Modulo modulo;
	
	private int emptySpace;
	
	private boolean reconstruir;
	private boolean cerrar;
	private boolean ventana;
	
	private boolean nombre;
	
	public PanelModular(Modulo modulo, int emptySpace, boolean reconstruir, boolean cerrar, boolean ventana, boolean nombre) {
		
		this.indice = 0;
		
		this.modulo = modulo;
		
		this.emptySpace = emptySpace;
		
		this.reconstruir = reconstruir;
		this.cerrar = cerrar;
		this.ventana = ventana;
		
		this.nombre = nombre;
	}

	public PanelModular(Modulo modulo, int emptySpace, boolean reconstruir, boolean cerrar, boolean ventana) {

		this(modulo, emptySpace, reconstruir, cerrar, ventana, false);
	}
	
	public PanelModular(Modulo modulo, int emptySpace) {
		
		this(modulo, emptySpace, false, true, false, false);
	}
	
	public ModularTabbedPane getParent() {
		return parent;
	}
	
	public void setParent(ModularTabbedPane parent) {
		this.parent = parent;
	}
	
	public Modulo getModulo() {
		return modulo;
	}
	
	public int getIndice() {
		return indice;
	}
	
	public void setIndice(int indice) {
		this.indice = indice;
	}
	
	public boolean isCerrar() {
		return cerrar;
	}
	
	private BorderPanelOverSelected cabecera;
	
	public BorderPanelOverSelected cabecera() {
			
		if(cabecera == null) {
			
			cabecera = new BorderPanelOverSelected(0, Color.WHITE, 2, false, true);
			cabecera.setLayout(new BorderLayout(8, 0));
			cabecera.setBackgroundColor(BACKGROUND_COLOR);
			cabecera.setOverColor(OVER_COLOR);
			
			Dimension d = new Dimension(PanelModular.PREFERED_WIDTH, PanelModular.PREFERED_HEIGHT);
			cabecera.setMaximumSize(d);
			cabecera.setMinimumSize(d);
			cabecera.setPreferredSize(d);
			
			if(modulo.getIcono() != null || modulo.getUrl() != null) {

				LabelPanel imagen = modulo.construirIcono(d.height - 15);
				cabecera.add(imagen, (nombre && !modulo.getTitulo().equals("")) ? BorderLayout.WEST : BorderLayout.CENTER);
			}

			if(nombre && !modulo.getTitulo().equals("")) {

				// TODO: BOLD
				JLabel titulo = new JLabel(modulo.getTitulo(20).toUpperCase());
				titulo.setFont(new Font("Tahoma", Font.PLAIN, 14));
				titulo.setForeground(Color.BLACK);
				cabecera.add(titulo, BorderLayout.CENTER);
			}

			if(ventana || cerrar) {

				JPanel opciones = new BorderPanel(0, Color.BLACK, 1, false, true);
				opciones.setOpaque(false);
				opciones.setLayout(new GridBagLayout());
				cabecera.add(opciones, BorderLayout.EAST);

				GridBagConstraints c_1 = new GridBagConstraints();

				c_1.gridx = 0;
				c_1.gridy = 0;

				c_1.fill = GridBagConstraints.BOTH;

				c_1.insets = new Insets(0, 0, 0, emptySpace);

				if(ventana) {

					final LabelPanel ventana = new LabelPanel(Constantes.CARPETA_RECURSOS + "modular/modulo/ventana.png", d.height - 15);
					ventana.setOpaque(false);
					opciones.add(ventana, c_1);

					ventana.addAccion(new Runnable() {

						@Override
						public void run() {

							if(parent != null) {
								parent.removeModulo(PanelModular.this);
							}
							
							modulo.abrir();
						}
					});
					
					c_1.gridx++;
				}
				
				if(cerrar) {

					final LabelPanel cerrar = new LabelPanel(Constantes.CARPETA_RECURSOS + "modular/modulo/cerrar.png", d.height - 15);
					cerrar.setOpaque(false);
					opciones.add(cerrar, c_1);

					cerrar.addAccion(new Runnable() {

						@Override
						public void run() {

							if(parent != null) {
								parent.removeModulo(PanelModular.this);
								removeAction();
							}
						}
					});				
				}
			}
		}
		
		return cabecera;
	}
	
	private JPanel contenido;
	
	public JPanel contenido() {
		
		if(this.contenido == null || reconstruir) {			
			this.contenido = construir();
		}
		
		return this.contenido;
	}
	
	protected abstract JPanel construir();
	
	protected void removeAction() {
		// Se completa en los hijos
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		PanelModular other = (PanelModular) obj;
		
		return indice == other.indice;
	}
}
package com.repex.librerias.modular;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.repex.librerias.components.screen.Screen;

public class Prueba {
	
	private final static Integer MODULAR_ID = 1;
	
	public static void main(String[] args) {
		
		Screen frame = new Screen("Pruebas") {
			
			private static final long serialVersionUID = 1L;

			@Override
			protected void configuracion() {}
		};
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(0, 0));

		ModuloPrueba modulo = new ModuloPrueba();
		ModuloController.getSingleton().add(MODULAR_ID, modulo.getTag(), modulo);
		
		ModuloPrueba2 modulo2 = new ModuloPrueba2();
		ModuloController.getSingleton().add(MODULAR_ID, modulo2.getTag(), modulo2);
		
		// CONTENIDO. CENTRO
		JPanel contenido = new ModularTabbedPane(MODULAR_ID, frame, true);
		panel.add(contenido, BorderLayout.CENTER);
		
		frame.setContentPane(panel);
		
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setMinimumSize(new Dimension(1350, 900));
		frame.setVisible(true);
	}
	
	private static class ModuloPrueba extends Modulo {
		
		public ModuloPrueba() {		
			super("", "ASD1", false);
		}
		
		@Override
		protected PanelModular crear() {
			return new PanelModuloPrueba(this);
		}

		@Override
		public void actualizar(int tipo, Object... data) {			
			System.out.println("Actualizando 1...");
		}

		@Override
		public String getTag() {			
			return "PRUEBA 1";
		}
		
		@Override
		public Object clone() {			
			return new ModuloPrueba();
		}
		
		private class PanelModuloPrueba extends PanelModular {

			public PanelModuloPrueba(ModuloPrueba modulo) {
				super(modulo, 2, false, true, false);
			}

			@Override
			protected JPanel construir() {
				
				JPanel panel = new JPanel();
				
				JButton boton = new JButton("Actualizari");
				panel.add(boton);
				
				boton.addMouseListener(new MouseAdapter() {
				
					@Override
					public void mouseClicked(MouseEvent e) {
						
						ModuloController.getSingleton().actualizar(MODULAR_ID, "PRUEBA 1");
					}
				});
				
				return panel;
			}
		}
	}
	
	private static class ModuloPrueba2 extends Modulo {
		
		public ModuloPrueba2() {		
			super("", "ASD2", false);
		}
		
		@Override
		protected PanelModular crear() {
			return new PanelModuloPrueba2(this);
		}

		@Override
		public void actualizar(int tipo, Object... data) {			
			System.out.println("Actualizando 2...");
		}

		@Override
		public String getTag() {			
			return "PRUEBA 2";
		}
		
		@Override
		public Object clone() {			
			return new ModuloPrueba2();
		}
		
		private class PanelModuloPrueba2 extends PanelModular {

			public PanelModuloPrueba2(ModuloPrueba2 modulo) {
				super(modulo, 2, false, true, false);
			}

			@Override
			protected JPanel construir() {
				
				JPanel panel = new JPanel();
				
				JButton boton = new JButton("Actualizaro");
				panel.add(boton);
				
				boton.addMouseListener(new MouseAdapter() {
				
					@Override
					public void mouseClicked(MouseEvent e) {
						
						ModuloController.getSingleton().actualizar(MODULAR_ID, "PRUEBA 2");
					}
				});
				
				return panel;
			}
		}
	}
}
package com.repex.librerias.modular;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ModuloController {
	
	private static ModuloController singleton;
	
	private ModuloController() {
		
		modulos = new HashMap<Integer, Map<String, Modulo>>();
		instancias = new HashMap<Integer, Map<String, List<Modulo>>>();
	}
	
	public static ModuloController getSingleton() {
	
		if(singleton == null) {
		
			singleton = new ModuloController();
		}
		
		return singleton;
	}
	
	// MODULOS
	private Map<Integer, Map<String, Modulo>> modulos;

	public Modulo get(Integer id, String tag) {

		if(modulos.get(id) != null) {
			return modulos.get(id).get(tag);
		}
		
		return null;
	}
	
	public List<Modulo> get(Integer id) {
		
		List<Modulo> lista = new LinkedList<Modulo>();
		if(modulos.get(id) != null) {
			for(Map.Entry<String, Modulo> entrada : modulos.get(id).entrySet()) {			
				lista.add(entrada.getValue());
			}

			// ORDENAR
			Collections.sort(lista, new Comparador());
		}
		
		return lista;
	}
	
	public void add(Integer id, String tag, Modulo modulo) {
		
		if(this.modulos.get(id) == null) {
			this.modulos.put(id, new HashMap<String, Modulo>());
		}
		
		if(this.modulos.get(id).get(tag) == null) {
			this.modulos.get(id).put(tag, modulo);	
		}
	}

	public void override(Integer id, String tag, Modulo modulo) {

		if(this.modulos.get(id) != null && this.modulos.get(id).get(tag) != null) {			
			this.modulos.get(id).put(tag, modulo);
		}
	}
	
	public Modulo remove(Integer id, String tag) {

		if(this.modulos.get(id) != null) {			
			return this.modulos.get(id).remove(tag);
		}
		
		return null;
	}
	
	// GUARDAR LAS INSTANCIAS DE CADA MODULO PARA ACTUALIZAR EN CASO DE SER NECESARIO
	private Map<Integer, Map<String, List<Modulo>>> instancias;
	
	public List<Modulo> getInstancias(Integer id) {
		
		List<Modulo> resultado = new ArrayList<Modulo>();
		if(instancias.get(id) != null) {
			
			for(List<Modulo> lista : instancias.get(id).values()) {
				resultado.addAll(lista);
			}
		}
		
		return resultado;
	}
	
	public void addInstancia(Integer id, Modulo modulo) {
		
		if(instancias.get(id) == null) {
			instancias.put(id, new HashMap<String, List<Modulo>>());
		}
		
		List<Modulo> modulos = instancias.get(id).getOrDefault(modulo.getTag(), new LinkedList<Modulo>());
		modulos.add(modulo);
		
		instancias.get(id).put(modulo.getTag(), modulos);
	}
	
	public void removeInstancia(Integer id, Modulo modulo) {

		if(instancias.get(id) != null) {
			List<Modulo> modulos = instancias.get(id).getOrDefault(modulo.getTag(), new LinkedList<Modulo>());
			modulos.remove(modulo);
		}
	}

	public void actualizar(Integer id, String tag) {
		
		actualizar(id, tag, 0);
	}
	
	public void actualizar(Integer id, String tag, int tipo, Object... data) {
		
		// ACTUALIZAR TODAS LAS DEL TAG
		if(instancias.get(id) != null) {
			List<Modulo> modulos = new LinkedList<Modulo>(instancias.get(id).getOrDefault(tag, new LinkedList<Modulo>()));
			for(Modulo modulo : modulos) {
				modulo.actualizar(tipo, data);
			}
		}
	}
	
	// COMPARADOR
	private class Comparador implements Comparator<Modulo> {
		
	    @Override
	    public int compare(Modulo m1, Modulo m2) {
	        return m1.getOrden() - m2.getOrden();
	    }
	}
}
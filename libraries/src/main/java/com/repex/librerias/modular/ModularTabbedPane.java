package com.repex.librerias.modular;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.OverlayLayout;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import com.repex.librerias.components.dialog.LostFocusDialog;
import com.repex.librerias.components.labels.LabelPanel;
import com.repex.librerias.components.paneles.BorderPanel;
import com.repex.librerias.components.paneles.BorderPanelOverSelected;
import com.repex.librerias.components.screen.Screen;
import com.repex.librerias.utilidades.Constantes;
import com.repex.librerias.utilidades.IconUtils;

public class ModularTabbedPane extends BorderPanel {

	private static final long serialVersionUID = 1L;
	
	private Integer modularId;
	
	private Screen parent;	
	private boolean crear;
	
	private List<PanelModular> modulos;
	private PanelModular seleccionado;
	
	public ModularTabbedPane(Integer modularId, Screen parent, boolean crear, List<PanelModular> modulos) {
		
		super(0);
		
		this.modularId = modularId;
		
		this.parent = parent;		
		this.crear = crear;
		
		this.modulos = new LinkedList<PanelModular>();
		
		inicializar();
				
		// ANADIR MODULOS
		for(PanelModular modulo : modulos) {
			addModulo(modulo, false);
		}
	}

	public ModularTabbedPane(Integer modularId, Screen parent, List<PanelModular> modulos) {

		this(modularId, parent, false, modulos);
	}
	
	public ModularTabbedPane(Integer modularId, Screen parent, boolean crear) {
		
		this(modularId, parent, crear, new LinkedList<PanelModular>());
	}

	public ModularTabbedPane(Integer modularId, Screen parent) {

		this(modularId, parent, false, new LinkedList<PanelModular>());
	}
	
	public PanelModular getSeleccionado() {
		return seleccionado;
	}
	
	public void addModulo(PanelModular modulo, boolean change) {
				
		// ANADIR INSTANCIA
		ModuloController.getSingleton().addInstancia(modularId, modulo.getModulo());
		
		modulo.setParent(this);
		modulo.setIndice(this.modulos.size());
		
		this.modulos.add(modulo);		
		addCabecera(modulo, change);

		comprobar();
	}
	
	public void removeModulo(PanelModular modulo) {

		// QUITAR INSTANCIA
		ModuloController.getSingleton().removeInstancia(modularId, modulo.getModulo());
		
		// QUITAR CABECERA
		pestanas.remove(modulo.cabecera());
		
		// QUITAR MODULO
		modulos.remove(modulo);
		
		// ACTUALIZAR CABECERA
		actualizarCabecera();
		
		// REASIGNAR INDICES
		for(int i=0; i<modulos.size(); i++) {			
			modulos.get(i).setIndice(i);
		}
		
		// CAMBIAR CONTENIDO
		if(seleccionado != null && seleccionado.equals(modulo) && modulos.size() > 0) {
			
			PanelModular nuevo = modulos.get(Math.min(seleccionado.getIndice(), modulos.size() - 1));

			nuevo.cabecera().setSelectable(false);
			nuevo.cabecera().setSeleccionado(true);
			
			cambiar(nuevo);
		}
		
		if(modulos.isEmpty()) {
			
			// REMOVER CONTENIDO
			this.contenido.removeAll();
		}
	}
	
	private BorderPanel cabecera;
	private BorderPanel pestanas;
	
	private BorderPanel contenido;

	private JPanel cambio; // BOTONES PARA CAMBIAR CONTENIDO
	
	// INTERNOS
	private void inicializar() {
		
		if(parent != null) {
			
			parent.addComponentListener(new ComponentAdapter() {

				@Override
				public void componentResized(ComponentEvent e) {

					super.componentResized(e);
					comprobar();
				}
			});
		}
		
		// CONGIFURACION
		setLayout(new BorderLayout(0, 1));
		setBackground(Color.WHITE);
		
		// CABECERA
		cabecera = new BorderPanel(0);
		cabecera.setBackground(Color.WHITE);
		cabecera.setLayout(new OverlayLayout(cabecera));
		add(cabecera, BorderLayout.NORTH);
		
		pestanas = new BorderPanel(0);
		pestanas.setOpaque(false);
		pestanas.setLayout(new FlowLayout(FlowLayout.LEADING, 2, 0));
		cabecera.add(pestanas);
		
		cambio = new BorderPanel(0);
		cambio.setOpaque(false);
		cambio.setLayout(new FlowLayout(FlowLayout.TRAILING, 5, 0));
		cabecera.add(cambio);
		
		int size = 25;
		
		Color over = Color.decode("#C6C6C6");
		
		LabelPanel izquierda = new LabelPanel(Constantes.CARPETA_RECURSOS + "modular/izquierda.png", size);
		izquierda.setShape(new LabelPanel.RoundedShape(new Dimension(5, 5)));
		izquierda.setOverColor(over);
		cambio.add(izquierda);

		izquierda.addAccion(new Runnable() {
			
			@Override
			public void run() {
				
				indice = Math.max(indice-1, 0);
				actualizarCabecera();
			}
		});
		
		LabelPanel derecha = new LabelPanel(Constantes.CARPETA_RECURSOS + "modular/derecha.png", size);
		derecha.setShape(new LabelPanel.RoundedShape(new Dimension(5, 5)));
		derecha.setOverColor(over);
		cambio.add(derecha);

		derecha.addAccion(new Runnable() {
			
			@Override
			public void run() {
				
				indice = Math.min(indice+1, modulos.size() - maximo);
				actualizarCabecera();
			}
		});
		
		cambio.setVisible(false);
		
		// CONTENIDO
		contenido = new BorderPanel(1);
		contenido.setOpaque(false);
		contenido.setLayout(new BorderLayout(0, 0));
		add(contenido, BorderLayout.CENTER);
	}

	// INTERFAZ
	public void cabeceraOpaque(boolean opaque) {
		cabecera.setOpaque(opaque);
	}
	
	public void contenidoBorder(int size) {
		contenido.setBorderSize(size);
	}
	
	// FUNCIONES
	private void comprobar() {

		// MAXIMO
		maximo = getWidth()/PanelModular.PREFERED_WIDTH;
		
		int resto = getWidth()%PanelModular.PREFERED_WIDTH;
		if(resto < (PanelModular.PREFERED_WIDTH/2 + 50)) {
			maximo = maximo-1;
		}
		
		// INDICE
		if(seleccionado != null) {
			indice = Math.min(seleccionado.getIndice(), modulos.size() - maximo);
			indice = Math.max(indice, 0);
		}
		
		actualizarCabecera();				
		cambio.setVisible(maximo < modulos.size());
	}
	
	private void cabeceraCrear() {

		BorderPanelOverSelected anadir = new BorderPanelOverSelected(0, Color.WHITE, 2, false, true);
		anadir.setLayout(new GridLayout(1, 1));
		anadir.setBackgroundColor(PanelModular.BACKGROUND_COLOR);
		anadir.setOverColor(PanelModular.OVER_COLOR);
		pestanas.add(anadir);

		Dimension d = new Dimension(PanelModular.PREFERED_HEIGHT + 20, PanelModular.PREFERED_HEIGHT);
		anadir.setMaximumSize(d);
		anadir.setMinimumSize(d);
		anadir.setPreferredSize(d);

		JLabel crear = new JLabel();
		crear.setIcon(IconUtils.escalar(getClass().getClassLoader().getResource(Constantes.CARPETA_RECURSOS + "modular/tabbed/nueva.png"), 25));
		crear.setHorizontalAlignment(SwingConstants.CENTER);
		anadir.add(crear);

		anadir.setSelectable(false);
		anadir.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {

	    		abrirDialogo();
			}
		});
	}
	
	private void addCabecera(PanelModular modulo) {
		
		addCabecera(modulo, false);
	}
	
	private void addCabecera(final PanelModular modulo, boolean change) {
		
		final BorderPanelOverSelected panel = modulo.cabecera();
		panel.addOnSelectedListener(new BorderPanelOverSelected.OnSelectedListener() {
			
			@Override
			public void onSelected(boolean selected) {
				
				panel.setSelectable(!selected);
				
				for(PanelModular p : modulos) {

					if(!p.cabecera().equals(panel)) {

						p.cabecera().setSelectable(true);
						p.cabecera().setSeleccionado(false);
					}
				}

				if(selected) {
					
					panel.setSeleccionado(true);
					cambiar(modulo);
				}
			}
		});
		
		panel.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
				if(modulo.isCerrar() && SwingUtilities.isMiddleMouseButton(e)) {
					
					removeModulo(modulo);
				}
			}
		});
		
		pestanas.add(panel);
		
		if(seleccionado != null && seleccionado.equals(modulo)) {

			panel.setSeleccionado(true);
		}
		
		if(change) {
			
			panel.setSelectable(false);
			
			for(PanelModular p : modulos) {

				if(!p.cabecera().equals(panel)) {

					p.cabecera().setSelectable(true);
					p.cabecera().setSeleccionado(false);
				}
			}

			panel.setSeleccionado(true);
			cambiar(modulo);
		}
	}
	
	private int indice = 0; // NUMERO DE INICIO PARA COLOCAR CABECERAS
	private int maximo = 0;
	
	public void setActual(int actual) {
		
		if(actual < modulos.size()) {
			
			for(PanelModular p : modulos) {

				p.cabecera().setSelectable(true);
				p.cabecera().setSeleccionado(false);
			}
			
			PanelModular panel = modulos.get(actual);
			BorderPanelOverSelected cabecera = panel.cabecera();

			cabecera.setSelectable(false);
			cabecera.setSeleccionado(true);
			
			cambiar(panel);
			
			actualizarCabecera();
		}
	}
	
	public void setActual(PanelModular panel) {
		
		if(panel != null) {
			
			for(int i=0; i<modulos.size(); i++) {
				
				if(modulos.get(i).equals(panel)) {
					
					setActual(i);
				}
			}
		}
	}
	
	private void actualizarCabecera() {

		pestanas.removeAll();
		
		pestanas.revalidate();
		pestanas.repaint();
		
		if(crear) {
			cabeceraCrear();
		}
		
		for(int i = indice; i < modulos.size() && i < (maximo + indice); i++) {
			
			addCabecera(modulos.get(i));
		}
		
		pestanas.revalidate();
		pestanas.repaint();
	}
	
	private void cambiar(PanelModular modulo) {
		
		// SELECCIONADO
		this.seleccionado = modulo;
		
		// QUITAR
		this.contenido.removeAll();
		
		// NUEVO
		this.contenido.add(modulo.contenido(), BorderLayout.CENTER);
		
		this.revalidate();
		this.repaint();
	}

	private void abrirDialogo() {

		if(!ModuloController.getSingleton().get(modularId).isEmpty()) {

			Dialogo dialogo = new Dialogo(parent, this);
			dialogo.setVisible(true);
		}
	}

	// DIALOGO PARA ANADIR UNA PESTANA
	private class Dialogo extends LostFocusDialog {

		private static final long serialVersionUID = 1L;

		// CONSTANTES
		private static final int PANEL_WIDTH = 200;
		private static final int PANEL_HEIGHT = 35;
		
		// VARIABLES
		private ModularTabbedPane tabbed;
		
		public Dialogo(Screen parent, ModularTabbedPane tabbed) {
			
			super(parent, tabbed);
			
			this.tabbed = tabbed;
		}
		
		@Override
		public JPanel construir() {
			
			JPanel contentPane = new JPanel();
			contentPane.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
			contentPane.setLayout(new BorderLayout(0, 0));
			setContentPane(contentPane);

			// CABECERA
			contentPane.add(getPanelCabecera(), BorderLayout.NORTH);

			// CENTRAL
			contentPane.add(getPanelCentral(), BorderLayout.CENTER);

			return contentPane;
		}
		
		private JPanel getPanelCabecera() {

			JPanel panelCabecera = new JPanel();
			panelCabecera.setLayout(new BorderLayout());
			panelCabecera.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
			panelCabecera.setBackground(Color.decode("#88acff"));

			panelCabecera.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
			panelCabecera.setMinimumSize(panelCabecera.getPreferredSize());
			panelCabecera.setMaximumSize(panelCabecera.getPreferredSize());

			return panelCabecera;
		}

		private JPanel getPanelCentral() {

			JPanel panelCentral = new JPanel();
			panelCentral.setLayout(new BorderLayout(0, 0));

			// MODULOS
			JPanel panelModulos = new JPanel();
			panelModulos.setLayout(new BoxLayout(panelModulos, BoxLayout.Y_AXIS));
			panelCentral.add(panelModulos, BorderLayout.CENTER);

			for(final Modulo modulo : ModuloController.getSingleton().get(modularId)) {

				// INTERFAZ
				BorderPanelOverSelected panelModulo = new BorderPanelOverSelected(0);			
				panelModulo.setLayout(new BorderLayout(15, 0));
				panelModulos.add(panelModulo);

				Dimension d = new Dimension(PANEL_WIDTH, PANEL_HEIGHT);
				panelModulo.setPreferredSize(d);
				panelModulo.setMinimumSize(d);
				panelModulo.setMaximumSize(d);
				
				panelModulo.addMouseListener(new MouseAdapter() {

					@Override
					public void mouseClicked(MouseEvent e) {

						if(modulo.getAccion() != null) {

							new Thread(modulo.getAccion()).start();
						}

						if(!modulo.isOnlyExecute()) {						

							Modulo nuevo = (Modulo) modulo.clone();
							tabbed.addModulo(nuevo.construir(), true);
						}
						
						dispose();
					}
				});

				if(modulo.getIcono() != null || modulo.getUrl() != null) {

					LabelPanel icono = modulo.construirIcono(PANEL_HEIGHT - 15);
					
					icono.setEmptyBorderSize(2);
					icono.setInnerEmptyBorder(true);

					panelModulo.add(icono, BorderLayout.WEST);
				}

				JLabel titulo = new JLabel(modulo.getTitulo(20));
				titulo.setFont(new Font("Tahoma", Font.PLAIN, 15));
				titulo.setVerticalAlignment(SwingConstants.CENTER);
				titulo.setHorizontalAlignment(SwingConstants.CENTER);
				panelModulo.add(titulo, BorderLayout.CENTER);
			}

			return panelCentral;
		}

		@Override
		public void posicion() {

			Point punto = MouseInfo.getPointerInfo().getLocation();
			this.setLocation((int)punto.getX(), (int)punto.getY());
		}
	}
}
package com.repex.librerias.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class IOController {

	private Charset codificacion;
	
	public IOController(Charset codificacion) {
		
		this.codificacion = codificacion;
	}
	
	public IOController() {
		
		this(StandardCharsets.UTF_8);
	}
	
	public List<String> leer(String fichero) {
		
		List<String> resultado = new LinkedList<String>();
		
		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fichero), codificacion));

			// Lectura del fichero
			String linea;
			while((linea=br.readLine()) != null) {

				resultado.add(linea);
			}

			br.close();
		}
		catch(Exception e) {

			return new LinkedList<String>();
		}
		
		return resultado;
	}

	public void escribir(String nombre, String fichero) {

		List<String> f = Arrays.asList(new String[] {fichero});
		escribir(nombre, f);
	}
	
	public void escribir(String nombre, List<String> fichero) {
		
		StringBuilder sb = new StringBuilder();
		for (String object : fichero) {
			
		    sb.append(object.toString()+"\r\n");
		}
		
		// Crear fichero
		File config = new File(nombre);
		
		try {

			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(config), codificacion));
			bw.write(sb.toString());
			bw.close();

		} catch (Exception e) {}	
	}
	
	public void anadir(String nombre, List<String> fichero) {

		StringBuilder sb = new StringBuilder();
		for (String object : fichero) {
			
		    sb.append(object.toString()+"\r\n");
		}
		
		// Crear fichero
		File config = new File(nombre);
		
		try {

			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(config, true), codificacion));
			bw.write(sb.toString());
			bw.close();

		} catch (Exception e) {}	
	}
}
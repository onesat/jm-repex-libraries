package com.repex.librerias.utilidades;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public class IconUtils {

	/**
	 * Crea un icono a partir de una URL local
	 * @param url URL local de resources
	 * @param tamano Tamano del icono resultante
	 * @return
	 */
	public static Icon escalar(URL url, int tamano) {
		
		if(tamano == 0) {
			
			return new ImageIcon(new ImageIcon(url).getImage());
		}

		return new ImageIcon(new ImageIcon(url).getImage().getScaledInstance(tamano, tamano, url.toString().endsWith(".gif") ? Image.SCALE_DEFAULT : Image.SCALE_SMOOTH));
	}
	
	public static Icon escalar(URL url) {
		
		return escalar(url, 0);
	}
	
	public static Icon escalar(Icon icono, int tamano) {
		
		if(tamano == 0) {
			
			return new ImageIcon(iconToImage(icono));
		}

		return new ImageIcon(new ImageIcon(iconToImage(icono)).getImage().getScaledInstance(tamano, tamano, Image.SCALE_SMOOTH));
	}
	
	public static Icon escalar(Icon icono) {
		
		return escalar(icono, 0);
	}
	
	/**
	 * Convierte el icono en una imagen
	 * @param icon
	 * @return imagen generada
	 */
	public static Image iconToImage(Icon icon) {
		
	    if(icon instanceof ImageIcon) {
	    	
	        return ((ImageIcon) icon).getImage();
	        
	    } else {
	    	
	        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
	        icon.paintIcon(null, image.getGraphics(), 0, 0);
	        return image;
	    }
	}
}
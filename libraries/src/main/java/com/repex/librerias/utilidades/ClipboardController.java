package com.repex.librerias.utilidades;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

public class ClipboardController {

	// TODO: HACER PARA COPIA INTERNA DE DATOS ??
	public static Clipboard getSystemClipboard() {
		
		Toolkit defaultToolkit = Toolkit.getDefaultToolkit();
        return defaultToolkit.getSystemClipboard();
	}
	
	public static void copiar(String contenido) {
		
		StringSelection s = new StringSelection(contenido);
		getSystemClipboard().setContents(s, null);
	}
}
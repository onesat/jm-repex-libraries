package com.repex.librerias.utilidades;

public class Constantes {

	public static final String CARPETA_RECURSOS = "com/repex/librerias/";
	
	public static final String PRE_END_POINT = "http://repexgroup.eu/apps/onesat-app/axpre/server/wsdl/wsdl_usuarios.php?wsdl";
	public static final String PRO_END_POINT = "http://repexgroup.eu/apps/onesat-app/axinpro/server/wsdl/wsdl_usuarios.php?wsdl";
}
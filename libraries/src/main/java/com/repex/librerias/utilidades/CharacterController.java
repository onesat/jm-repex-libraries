package com.repex.librerias.utilidades;

import java.util.Random;

public class CharacterController {

	public static String generarCatacter() {
		
		Random random = new Random();
		
		char[] caracteres = new char[] {
				'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'W', 'X', 'Y', 'Z', 
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'w', 'x', 'y', 'z',
				'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
		
		return Character.toString(caracteres[random.nextInt(caracteres.length)]);
	}
}